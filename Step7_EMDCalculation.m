% Perform EMD analysis based on surface distance
% Calculate original Reubner EMD
% Generate new Surface distance/Euclidian distance matrices
clear;

addpath('~/Dropbox/LAB_Folder_Macbook/MATLAB/Code/FastEMD_PeleWerman/');
addpath('~/Dropbox/LAB_Folder_Macbook/MATLAB/Utilities/');

% Calculation type: 'actmaps' or 'tmaps'
CalcType = 'actmaps';
disp(CalcType);

% Calculate fast EMD (histograms don't have to be normalized)
extra_mass_penalty = -1;
% For Rubner's EMD, extra_mass_penalty = 0

ncond = 8;
all_conds_pairs = nchoosek(1:ncond,2);

for sub = 1
    disp(['Analyzing subject' num2str(sub)]);
    subj_dir = ['~/Dropbox/LAB_Folder_Macbook/MATLAB/Data/haxby2001/subj' num2str(sub)];
    cd(subj_dir);
    
    if strcmp(CalcType,'actmaps')
        EMD_PrefileName = ['EMD_pre_subj' num2str(sub) '_mc_ss_VT.mat'];
        % EMD_PrefileName = ['EMD_pre_subj' num2str(sub) '_mc_ss_VT2.mat'];
    elseif strcmp(CalcType,'tmaps')
        EMD_PrefileName = ['EMD_pre_subj' num2str(sub) '_mc_ss_VT_tmaps.mat'];
        % EMD_PrefileName = ['EMD_pre_subj' num2str(sub) '_mc_ss_VT_tmaps2.mat'];
    end
    load(EMD_PrefileName);
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Correlation calculation
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    CorrMtrx = zeros(ncond * (ncond - 1)/2,1);
    CorrMtrx_scaled = CorrMtrx;
    
    for condpairs = 1:size(all_conds_pairs,1)
        condA = Afni_coors_val(:,5 + all_conds_pairs(condpairs,1));
        condB = Afni_coors_val(:,5 + all_conds_pairs(condpairs,2));
        % Convert original activity maps range to [0,255]
        condA_scaled = 255 * mat2gray(condA);
        condB_scaled = 255 * mat2gray(condB);
        
        CorrMtrx(condpairs,1) = corr(condA,condB);
        CorrMtrx_scaled(condpairs,1) = corr(condA_scaled,condB_scaled);
    end
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % EMD calculation
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % For EMD_pre_subj*_mc_ss_VT.mat
    GroundDistFiles = who('GD_Surf_com*');
    GDlength = size(GroundDistFiles,1);
    GroundDistFiles{GDlength + 1,1} = 'GD_Euc_com';
    
    % For EMD_pre_subj*_mc_ss_VT2.mat
    % GroundDistFiles = who('GD_*_com_fac*_thr70','GD_*_com_fac*_thr90');    
    % GDlength = size(GroundDistFiles,1);
    % GroundDistFiles{GDlength + 1,1} = 'GD_Surf_com_fac10';
    % GroundDistFiles{GDlength + 2,1} = 'GD_Surf_com_fac2';
    % GroundDistFiles{GDlength + 3,1} = 'GD_Surf_com_fac5';
    % GroundDistFiles{GDlength + 4,1} = 'GD_Surf_com_fac7';
    % GroundDistFiles{GDlength + 5,1} = 'GD_Surf_com_fac8';
    % GroundDistFiles{GDlength + 6,1} = 'GD_Surf_com_fac9';

    for ctr = 1:length(GroundDistFiles)
        % Load different ground distance matrices
        GDMtrx = GroundDistFiles{ctr};
        % Matrix initialization
        EMDMtrx = zeros(ncond * (ncond - 1)/2,1);
        EMDMtrx_Rub = EMDMtrx;
        EMDMtrx_scaled = EMDMtrx;
        EMDMtrx_scaled_Rub = EMDMtrx;
        for condpairs = 1:size(all_conds_pairs,1)
            condA = Afni_coors_val(:,5 + all_conds_pairs(condpairs,1));
            condB = Afni_coors_val(:,5 + all_conds_pairs(condpairs,2));
            % Convert original activity maps range to [0,255]
            condA_scaled = 255 * mat2gray(condA);
            condB_scaled = 255 * mat2gray(condB);
            
            % Calculate EMD
            EMDMtrx(condpairs,1) = emd_hat_gd_metric_mex(condA,condB,eval(GDMtrx),-1);
            EMDMtrx_Rub(condpairs,1) = emd_hat_gd_metric_mex(condA,condB,eval(GDMtrx),0);
            
            EMDMtrx_scaled(condpairs,1) = emd_hat_gd_metric_mex(condA_scaled,condB_scaled,eval(GDMtrx),-1);
            EMDMtrx_scaled_Rub(condpairs,1) = emd_hat_gd_metric_mex(condA_scaled,condB_scaled,eval(GDMtrx),0);
        end
        eval([strcat('EMD_fin',GDMtrx(3:end)) ' = EMDMtrx;'])
        eval([strcat('EMD_fin',GDMtrx(3:end),'_Rub') ' = EMDMtrx_Rub;'])
        eval([strcat('EMD_fin',GDMtrx(3:end),'_scaled') ' = EMDMtrx_scaled;'])
        eval([strcat('EMD_fin',GDMtrx(3:end),'_scaled_Rub') ' = EMDMtrx_scaled_Rub;'])
    end
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Save data
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    if strcmp(CalcType,'actmaps')
        % ---------------------------------------------------------
        FileName = ['EMD_subj' num2str(sub) '_mc_ss_VT_new3.mat'];
        % ---------------------------------------------------------
        EMDResultsFile = who('EMD_fin*');
        save(fullfile(subj_dir,FileName),'CorrMtrx','CorrMtrx_scaled');
        for ctr = 1:length(EMDResultsFile)
            % save(fullfile(subj_dir,FileName),EMDResultsFile{ctr},'-append');
        end
    elseif strcmp(CalcType,'tmaps')
        % ---------------------------------------------------------
        FileName = ['EMD_subj' num2str(sub) '_mc_ss_VT_tmaps_new3.mat'];
        % ---------------------------------------------------------
        EMDResultsFile = who('EMD_fin*');
        save(fullfile(subj_dir,FileName),'CorrMtrx','CorrMtrx_scaled');
        for ctr = 1:length(EMDResultsFile)
            % save(fullfile(subj_dir,FileName),EMDResultsFile{ctr},'-append');
        end
    end
    
    disp(['EMD calculation for subject ' num2str(sub) ' is done']);
    unix('df -h | /usr/bin/mail -s "EMD calculation for a single subject is done" xixi.wang577@gmail.com')
end
