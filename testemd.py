import cv
import scipy
import scipy.io
import numpy as np

mat_dict = scipy.io.loadmat('/axelUsers/xwang91/Documents/MATLAB/Data/haxby2001/subj1/EMD_pre_subj1_mc_ss_VT.mat')
GD_Euc_com = np.array(mat_dict['GD_Euc_com'])
Afni_coors_val = np.array(mat_dict['Afni_coors_val'])

ctri = 0
ctrj = 1
voxel_num = 526
condA = Afni_coors_val[:, 5 + ctri]
condB = Afni_coors_val[:, 5 + ctrj]
sigA1 = cv.CreateMat(voxel_num, 1, cv.CV_32FC1)
sigB1 = cv.CreateMat(voxel_num, 1, cv.CV_32FC1)
sigA1 = np.array(sigA1)
sigB1 = np.array(sigB1)
sigA1[:,0] = condA
sigB1[:,0] = condB
                
GD_Euc_com_cont = GD_Euc_com.copy()
print GD_Euc_com_cont.flags.contiguous
Dist_float = cv.CreateMat(voxel_num, voxel_num, cv.CV_32FC1)
Dist_float = np.array(Dist_float)
Dist_float = GD_Euc_com_cont
print Dist_float
# Generate different signatures

test = cv.CalcEMD2(cv.fromarray(sigA1), cv.fromarray(sigB1), cv.CV_DIST_USER,
        distance_func=None, cost_matrix=cv.fromarray(Dist_float))
print "test1"
