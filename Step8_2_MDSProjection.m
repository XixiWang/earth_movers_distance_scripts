% MDS projection
% Here non-classical MDS is used
% Several criterions have been applied
clear
close all

addpath('~/Dropbox/LAB_Folder_Macbook/MATLAB/Utilities/');
addpath('~/Dropbox/LAB_Folder_Macbook/MATLAB/Utilities/export_fig/');

labels = {'bottle' 'cat' 'chair' 'face' 'house' 'scissors' 'scrambledpix' 'shoe'};
criterions = {'stress','sstress','metricstress','metricsstress','sammon','strain'};

Sub_dir = '~/Dropbox/LAB_Folder_Macbook/MATLAB/Data/haxby2001/';
MatName = 'mc_ss_VT_new3';
tempFile = fullfile(Sub_dir,'subj1',['EMD_subj1_' MatName '.mat']);
% tempfile = '~/Dropbox/LAB_Folder_Macbook/MATLAB/Data/haxby2001/subj1/EMD_subj1_mc_ss_VT_new.mat';
AllVarNames = who('-file',tempFile);

% CalcType: 'tmaps' or 'actmaps'
CalcType = 'actmaps';
% EMDType: 'EMDVec' or 'EMDVec_norm'
EMDType = 'EMDVec';

EMD_MDS_criterion = 'metricstress';
Corr_MDS_criterion = 'sstress';

disp(CalcType);
disp(EMDType);

% Generate correlation matrix
CorrMtrx = GenerateGroupMtrx(MatName,CalcType,AllVarNames{1});

% Select a possible EMD matrix
% -----------------------------
varname = 36;
% -----------------------------
EMDMtrx = GenerateGroupMtrx(MatName,CalcType,AllVarNames{varname});
EMDMtrx_mat = mat2gray(EMDMtrx);
disp(AllVarNames{varname})

%%% Possible EMD matrices: AllVarNames{6}, AllVarNames{9}, {10},{11}

%%% Updated: if using EMD_subj*_mc_ss_VT_new2.mat
%%% AllVarNames{49}:scaled; 50; 
%%% Could potential work:45 
%%% May not working: Rub

for ctr = 1:5
    subj_str = ['Subject ' num2str(ctr)];
    % Similarity/Dissimilarity vectors
    % MDS could use dissimilarity vectors directly
    CorrVec = CorrMtrx(ctr,:);
    % Convert correlation to dissimilarity
    DisCorrVec = 1 - CorrVec;
    EMDVec = EMDMtrx(ctr,:);
    EMDVec_norm = EMDMtrx_mat(ctr,:);
    
    if strcmp(EMDType,'EMDVec')
        EMDResultVec = EMDVec;
    elseif strcmp(EMDType, 'EMDVec_norm')
        EMDResultVec = EMDVec_norm;
    end
    opts = statset('MaxIter',100000,'Display','off');
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Project EMD results
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Non-metric scaling
    [temp1,~,disparities] = mdscale(EMDResultVec,2,'criterion','stress','Options',opts);
    dist1 = pdist(temp1);
    temp2 = mdscale(EMDResultVec,2,'criterion','sstress','Options',opts);
    dist2 = pdist(temp2);
    % Metric scaling
    temp3 = mdscale(EMDResultVec,2,'criterion','metricstress','Options',opts);
    dist3 = pdist(temp3);
    temp4 = mdscale(EMDResultVec,2,'criterion','metricsstress','Options',opts);
    dist4 = pdist(temp4);
    temp5 = mdscale(EMDResultVec,2,'criterion','sammon','Options',opts);
    dist5 = pdist(temp5);
    temp6 = mdscale(EMDResultVec,2,'criterion','strain','Options',opts);
    dist6 = pdist(temp6);
    
    scrsz = get(0,'ScreenSize');
    figure('Position',[1 scrsz(4) scrsz(3)*0.8 scrsz(4)*0.8])
    % figure('Position',[1 scrsz(4) scrsz(3) scrsz(4)])
    set(gcf,'Color',[1 1 1])

    subplot(2,2,1);
    plot(EMDResultVec,dist1,'bo',EMDResultVec,dist2,'r+',EMDResultVec,dist3,'go',...
        EMDResultVec,dist4,'c*',EMDResultVec,dist5,'y+',EMDResultVec,dist6,'mx',...
        [0 max(EMDResultVec)],[0 max(EMDResultVec)],'k--');
    legend({'Non-metric stress','Non-metirc ss','Metric stress','Metric ss',...
        'Sammon Mapping','strain','1:1 Line'},'Location','NorthWest');
    xlabel('Dissimilarities: EMD','FontSize',12);
    ylabel('Distance','FontSize',12);
    % title(['Shepard plot - ' EMDType]);
    title('Shepard plot - based on EMD','FontSize',12,'FontWeight','bold');
    set(gca,'FontSize',12);
    
    %     figure;
    %     [dum,ord] = sortrows([disparities(:) EMDResultVec(:)]);
    %     plot(EMDResultVec,disparities,'bo',...
    %         EMDResultVec(ord),disparities(ord),'r.-');
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Project correlation results
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Non-metric scaling
    if ctr == 5 % For subject 5
        corrdist1 = [];
    else
        corrtemp1 = mdscale(DisCorrVec,2,'criterion','stress','Options',opts);
        corrdist1 = pdist(corrtemp1);
    end
    corrtemp2 = mdscale(DisCorrVec,2,'criterion','sstress','Options',opts);
    corrdist2 = pdist(corrtemp2);
    % Metric scaling
    corrtemp3 = mdscale(DisCorrVec,2,'criterion','metricstress','Options',opts);
    corrdist3 = pdist(corrtemp3);
    corrtemp4 = mdscale(DisCorrVec,2,'criterion','metricsstress','Options',opts);
    corrdist4 = pdist(corrtemp4);
    corrtemp5 = mdscale(DisCorrVec,2,'criterion','sammon','Options',opts);
    corrdist5 = pdist(corrtemp5);
    corrtemp6 = mdscale(DisCorrVec,2,'criterion','strain','Options',opts);
    corrdist6 = pdist(corrtemp6);
    
    subplot(2,2,2);
    if ctr == 5
        plot(DisCorrVec,corrdist2,'r+',DisCorrVec,corrdist3,'go',...
            DisCorrVec,corrdist4,'c*',DisCorrVec,corrdist5,'y+',DisCorrVec,corrdist6,'mx',...
            [0 max(DisCorrVec)],[0 max(DisCorrVec)],'k--');
        legend({'Non-metirc ss','Metric stress','Metric ss',...
        'Sammon Mapping','strain','1:1 Line'},'Location','NorthWest');
    else
        plot(DisCorrVec,corrdist1,'bo',DisCorrVec,corrdist2,'r+',DisCorrVec,corrdist3,'go',...
            DisCorrVec,corrdist4,'c*',DisCorrVec,corrdist5,'y+',DisCorrVec,corrdist6,'mx',...
            [0 max(DisCorrVec)],[0 max(DisCorrVec)],'k--');
        legend({'Non-metric stress','Non-metirc ss','Metric stress','Metric ss',...
        'Sammon Mapping','strain','1:1 Line'},'Location','NorthWest');
    end
    xlabel('Dissimilarities: 1-Correlation','FontSize',12);
    ylabel('Distance','FontSize',12);
    set(gca,'FontSize',12);
    title('Shepard plot - based on correlation','FontSize',12,'FontWeight','bold');
    
    % Save figure
    % Fig1Name = ['sub' num2str(ctr) '_ShepardPlot'];
    % eval(['export_fig ' Fig1Name ' -m2 -nocrop']);
    
    % figure('Position',[1 scrsz(4) scrsz(3)*0.5 scrsz(4)*0.5])
    % Project EMD results
    EMD_MDS_cri_idx = find(strcmp(EMD_MDS_criterion,criterions));
    EMD_MDS_cri_coords = eval(['temp' num2str(EMD_MDS_cri_idx)]);
    subplot(2,2,3);
    plot(EMD_MDS_cri_coords(:,1),EMD_MDS_cri_coords(:,2),'.','MarkerSize',2);
    text(EMD_MDS_cri_coords(:,1),EMD_MDS_cri_coords(:,2),labels,'Color','r', ...
        'FontSize',12,'FontWeight','bold');
    set(gca,'xticklabel',{[]});set(gca,'yticklabel',{[]});
    % title(strcat('EMD-',EMD_MDS_criterion));
    title('Neural similarity space in 2D MDS - based on EMD', ...
        'FontSize',12,'FontWeight','bold');
    
    % Project corr results
    Corr_MDS_cri_idx = find(strcmp(Corr_MDS_criterion,criterions));
    Corr_MDS_cri_coords = eval(['corrtemp' num2str(Corr_MDS_cri_idx)]);
    subplot(2,2,4);
    plot(Corr_MDS_cri_coords(:,1),Corr_MDS_cri_coords(:,2),'.','MarkerSize',2);
    text(Corr_MDS_cri_coords(:,1),Corr_MDS_cri_coords(:,2),labels,'Color','r', ...
        'FontSize',12,'FontWeight','bold');
    set(gca,'xticklabel',{[]});set(gca,'yticklabel',{[]});
    % title(strcat('Correlation-',Corr_MDS_criterion));
    title('Neural similarity space in 2D MDS - based on correlation', ...
        'FontSize',12,'FontWeight','bold');
    
    annotation(gcf,'textbox',[0.02 0.75 0.2 0.2],'String',subj_str, ...
        'FitBoxToText','on','FontSize',14,'FontWeight','bold',...
        'LineStyle','none','EdgeColor','none')
    
    % Save figure
    Fig1Name = ['sub' num2str(ctr) '_MDSPlot'];
    % eval(['export_fig ' Fig1Name ' -m2 -nocrop']);
    close all;
end
