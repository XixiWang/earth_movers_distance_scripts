from mvpa2.suite import *
import os
import nibabel
from mvpa2.misc.io.base import SampleAttributes
from mvpa2.datasets.mri import fmri_dataset
import scipy
import scipy.stats
import scipy.spatial  

# Change the location of the root directory, for your local machine:
pymvpa_datadbroot = '/Users/XixiWang/Dropbox/LAB_Folder_Macbook/MATLAB/Data/haxby2001'

# Exclude subject 6
num_subjs = 5
num_categories = 8
num_squareform_vals = num_categories * (num_categories-1) / 2
all_subjs_squareform_sims = scipy.zeros((num_subjs,num_squareform_vals),float)

for subj_num in range(num_subjs):
    subj_string = 'subj' + str(subj_num + 1)
    # set path for each subject
    subjpath = os.path.join(pymvpa_datadbroot, subj_string)
    # Read PyMVPA sample attribute definitions from text files
    attrs = SampleAttributes(os.path.join(subjpath, 'labels.txt'),header=True)

    print " ----------------- About to import the dataset for " + subj_string
    # Create a dataset from an fMRI timeseries image (samples,targets,chunks,mask)
    my_dataset = mvpa2.datasets.mri.fmri_dataset( samples=os.path.join(subjpath,'bold_raw_mc.nii.gz'),
    targets=attrs.labels,chunks=attrs.chunks,mask=os.path.join(subjpath, 'mask4_vt.nii.gz'))

    # Extract coordinates of VOIs
    print " ----------------- Extract coordinates of VOI"
    Coords = my_dataset.fa.voxel_indices
    print Coords

    # Perform detrending
    detrender = PolyDetrendMapper(polyord=1, chunks_attr='chunks')
    # Detrended_dataset:results
    detrended_dataset = my_dataset.get_mapped(detrender)
    # Data normalization: scales all voxels into the same range and removes the mean
    zscore(detrended_dataset,param_est=('targets', ['rest']))

    print " ----------------- Export data without rest state"
    detrended_dataset_without_rest_periods = detrended_dataset[detrended_dataset.sa.targets != 'rest']
    # print detrended_dataset_without_rest_periods.sa.chunks
    # print detrended_dataset_without_rest_periods.sa.targets
    detrended_dataset_without_rest_periods_values = detrended_dataset_without_rest_periods.samples
    Coords_without_rest = detrended_dataset_without_rest_periods.fa.voxel_indices

    # Calculate the mean per target/condition
    averager = mean_group_sample(['targets'])
    averaged_dataset = detrended_dataset_without_rest_periods.get_mapped(averager)
    print averaged_dataset.sa.chunks
    print averaged_dataset.sa.targets
    averaged_fMRI_values = averaged_dataset.samples
    Coords_mean1 = averaged_dataset.fa.voxel_indices

    averager2 = mean_group_sample(['targets'],order='occurrence')
    averaged_dataset2 = detrended_dataset_without_rest_periods.get_mapped(averager2)
    print averaged_dataset2.sa.chunks
    print averaged_dataset2.sa.targets
    averaged_fMRI_values2 = averaged_dataset2.samples
    Coords_mean2 = averaged_dataset2.fa.voxel_indices

    print " ----------------- Export data for each condition"
    detrended_dataset_bottle = detrended_dataset[detrended_dataset.sa.targets == 'bottle']
    detrended_dataset_bottle_values = detrended_dataset_bottle.samples

    detrended_dataset_cat = detrended_dataset[detrended_dataset.sa.targets == 'cat']
    detrended_dataset_cat_values = detrended_dataset_cat.samples

    detrended_dataset_chair = detrended_dataset[detrended_dataset.sa.targets == 'chair']
    detrended_dataset_chair_values = detrended_dataset_chair.samples

    detrended_dataset_face = detrended_dataset[detrended_dataset.sa.targets == 'face']
    detrended_dataset_face_values = detrended_dataset_face.samples

    detrended_dataset_house = detrended_dataset[detrended_dataset.sa.targets == 'house']
    detrended_dataset_house_values = detrended_dataset_house.samples

    detrended_dataset_scissors = detrended_dataset[detrended_dataset.sa.targets == 'scissors']
    detrended_dataset_scissors_values = detrended_dataset_scissors.samples

    detrended_dataset_scrambledpix = detrended_dataset[detrended_dataset.sa.targets == 'scrambledpix']
    detrended_dataset_scrambledpix_values = detrended_dataset_scrambledpix.samples

    detrended_dataset_shoe = detrended_dataset[detrended_dataset.sa.targets == 'shoe']
    detrended_dataset_shoe_values = detrended_dataset_shoe.samples

    print " ----------------- Export rest state"
    detrended_dataset_rest_periods = detrended_dataset[detrended_dataset.sa.targets == 'rest']
    detrended_dataset_rest_periods_values = detrended_dataset_rest_periods.samples
    Coords_rest = detrended_dataset_rest_periods.fa.voxel_indices

    print " ----------------- Save the data and coordinates"
    FileName = subj_string + '_actmaps_VT_mc_ss.mat'
    FileName = os.path.join(subjpath, FileName)
    data = {}

    data[subj_string + '_data'] = averaged_fMRI_values
    data[subj_string + '_data_occurrence'] = averaged_fMRI_values2
    data[subj_string + '_coords'] = Coords
    data[subj_string + '_taskdata_all'] = detrended_dataset_without_rest_periods_values
    data[subj_string + '_restdata_all'] = detrended_dataset_rest_periods_values
    data[subj_string + '_bottle'] = detrended_dataset_bottle_values
    data[subj_string + '_cat'] = detrended_dataset_cat_values
    data[subj_string + '_chair'] = detrended_dataset_chair_values
    data[subj_string + '_face'] = detrended_dataset_face_values
    data[subj_string + '_house'] = detrended_dataset_house_values
    data[subj_string + '_scissors'] = detrended_dataset_scissors_values
    data[subj_string + '_scrambledpix'] = detrended_dataset_scrambledpix_values
    data[subj_string + '_shoe'] = detrended_dataset_shoe_values


    # data[subj_string + '_coords_without_rest'] = Coords_without_rest
    # data[subj_string + '_coords_rest'] = Coords_rest
    # data[subj_string + '_coordsmean1'] = Coords_mean1
    # data[subj_string + '_coordsmean2'] = Coords_mean2

    # Coords, Coords_without_rest, Coords_mean1, Coords_mean2, and Coords_rest are the same (checked in MATLAB)

    scipy.io.savemat(FileName, data)

    # ----------------------------------------
    # Now make the similarity matrix, which consists of the spatial correlations
    # between the condition-average activation patterns.
    print " ----------------- Generate similarity matrix"
    sim_matrix = scipy.corrcoef(averaged_fMRI_values)
    this_squareform_vec = scipy.spatial.distance.squareform(sim_matrix,checks=False)
    all_subjs_squareform_sims[subj_num,:] = this_squareform_vec

    # scipy.io.savemat('haxby2001_sim_matrices_VT_test2_Xixi.mat',{'all_subjs_squareform_sims':all_subjs_squareform_sims})




