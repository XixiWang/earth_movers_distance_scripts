function ave_dist = CalcAveDist(DistData,idxlist1,idxlist2)
% ***************************************************************
% function ave_dist: Calculate averaged bin-to-bin distance
%
% DistData: n-by-4 matrix (idx1, idx2, Euc_dist, Surf_dist)
%
% Idxlist1, Idxlist2: indices for original each bins, used to find
% original distance data
%
% Update: when using findsubmat function, only search the first
% two columns of the DistData matrix
% ***************************************************************
addpath('~/Dropbox/LAB_Folder_Macbook/MATLAB/Utilities/');

comp_Euc_vals = zeros(length(idxlist1) * length(idxlist2),1);
[idxlist1_mesh,idxlist2_mesh] = meshgrid(idxlist1(:),idxlist2(:));
idxlist1_col = idxlist1_mesh(:);
idxlist2_col = idxlist2_mesh(:);
for ctrj = 1:length(idxlist1_col)
    if idxlist1_col(ctrj) < idxlist2_col(ctrj)
        [r,~] = findsubmat(DistData(:,1:2),[idxlist1_col(ctrj) idxlist2_col(ctrj)]);
        comp_Euc_vals(ctrj) = DistData(r,3);
    elseif idxlist1_col(ctrj) > idxlist2_col(ctrj)
        [r,~] = findsubmat(DistData(:,1:2),[idxlist2_col(ctrj) idxlist1_col(ctrj)]);
        comp_Euc_vals(ctrj) = DistData(r,3);
    elseif idxlist1_col(ctrj) == idxlist2_col(ctrj)
        comp_Euc_vals(ctrj) = 0;
    end
end
ave_dist = mean(comp_Euc_vals);

end

% ---------------------------------------------------------
% num_bin = 100, t = ~8h
% num_bin = 526, t = ~10h

% ---------------------------------------------------------
%     % Method1
%     tic;
%     idx1_num = length(idxlist1);
%     idx2_num = length(idxlist2);
%     comp_Euc_vals = zeros(idx1_num * idx2_num,1);
%     % comp_Surf_vals = comp_Euc_vals;
%     a = 1;
%     for ctri = 1:idx1_num
%         for ctrj = 1:idx2_num
%             tempidx1 = idxlist1(ctri);
%             tempidx2 = idxlist2(ctrj);
%             if tempidx1 < tempidx2
%                 tempidxlist = [tempidx1 tempidx2];
%                 [r,~] = findsubmat(DistData,tempidxlist);
%                 comp_Euc_vals(a) = DistData(r,3);
%             elseif tempidx1 > tempidx2
%                 tempidxlist = [tempidx2 tempidx1];
%                 [r,~] = findsubmat(DistData,tempidxlist);
%                 comp_Euc_vals(a) = DistData(r,3);
%             elseif tempidx1 == tempidx2
%                 comp_Euc_vals(a) = 0;
%             end
%             a = a + 1;
%         end
%     end
%     ave_dist = mean(comp_Euc_vals);
%     t1 = toc;
%
