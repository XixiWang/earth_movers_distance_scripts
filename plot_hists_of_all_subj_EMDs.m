%%% Let's make a stacked plot of the histograms of the different subjects
%%% EMD values

%%% This script assumes that the EMD values GroupMtrx are already in
%%% workspace
clear 
% close all

load GroupMtrx.mat;

num_subjs = 5;
EMD_min_max_range = [min(GroupMtrx(:)) max(GroupMtrx(:))];

stimulus_list_order = { 'bottle' 'cat' 'chair' 'face' 'house' 'scissors' 'scrambled' 'shoe'};

figure(1);
clf;
for subj_num = 1:num_subjs,
    subplot(num_subjs,1,subj_num);
    
    this_subj_EMD_values = GroupMtrx(subj_num,:);
    
    hist(this_subj_EMD_values,50);
    set(gca,'XLim',EMD_min_max_range);
end;

%%% When we run this, it looks like subj 1 has some really high EMD values
subj1_EMD_values_vec = GroupMtrx(1,:);
subj1_EMD_matrix = squareform(subj1_EMD_values_vec);
figure(2);
clf;
imagesc(subj1_EMD_matrix);
colormap hot;
caxis(EMD_min_max_range);
set(gca,'XTickLabel',stimulus_list_order);
set(gca,'YTickLabel',stimulus_list_order);
title('Subject 1');
colorbar;

%%% Let's also look at subject 3
subj3_EMD_values_vec = GroupMtrx(3,:);
subj3_EMD_matrix = squareform(subj3_EMD_values_vec);
figure(3);
clf;
imagesc(subj3_EMD_matrix);
colormap hot;
% caxis(EMD_min_max_range);
set(gca,'XTickLabel',stimulus_list_order);
set(gca,'YTickLabel',stimulus_list_order);
title('Subject 3');
colorbar;

