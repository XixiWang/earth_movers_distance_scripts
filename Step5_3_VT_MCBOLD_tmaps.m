% For each subject, the code generates t-maps under different conditions
% and then the activity maps can be fed into EMD calculation
clear;
close all;

% restdata:588*577
% taskdata:864*577
% Original code: cond_activations:864*69757
% Original code: rest_activations:588*69757
addpath('~/Dropbox/LAB_Folder_Macbook/MATLAB/Toolboxes/spm8/')

num_subjs = 5;
num_conds = 8;

for sub = 1:5
    subj_dir = ['~/Documents/MATLAB/Data/haxby2001/subj' num2str(sub)];
    cd(subj_dir)
    
    % Generate t-maps under different conditions for each subject
    disp(['Processing subject ' num2str(sub) ]);
    load(['subj' num2str(sub) '_actmaps_VT_mc_ss.mat']);
    
    % Pay attention to subject 5
    if sub == 5,
        num_TRs_per_cond = 9*11;
        num_TRs_for_rest = 660;
    else
        num_TRs_per_cond = 9*12;
        num_TRs_for_rest = 588;
    end;
    
    eval(['taskdata_mtrx = subj' num2str(sub) '_taskdata_all;']);
    eval(['restdata_mtrx = subj' num2str(sub) '_restdata_all;']);
    eval(['subj_coords = subj' num2str(sub) '_coords;']);
    
    num_voxels = size(taskdata_mtrx,2);
    
    cond1 = eval(['subj' num2str(sub) '_bottle']);
    cond2 = eval(['subj' num2str(sub) '_cat']);
    cond3 = eval(['subj' num2str(sub) '_chair']);
    cond4 = eval(['subj' num2str(sub) '_face']);
    cond5 = eval(['subj' num2str(sub) '_house']);
    cond6 = eval(['subj' num2str(sub) '_scissors']);
    cond7 = eval(['subj' num2str(sub) '_scrambledpix']);
    cond8 = eval(['subj' num2str(sub) '_shoe']);
    
    subj_Ts_indConds_mat = zeros(num_conds,num_voxels);
    % Use MATLAB function to perform two-sample t-test
    for cond_num = 1:num_conds
        [h,p,ci,t] = ttest2(eval(['cond' num2str(cond_num)]),restdata_mtrx);
        subj_Ts_indConds_mat(cond_num,:) = t.tstat;
    end
        
    FileName = ['subj' num2str(sub) '_tmaps_VT_mc_ss.mat'];
    eval(['subj' num2str(sub) '_tmaps = subj_Ts_indConds_mat;'])
    
    save(fullfile(subj_dir,FileName),['subj' num2str(sub) '_tmaps'],['subj' num2str(sub) '_coords']);
    
    clear cond1 cond2 cond3 cond4 cond5 cond6 cond7 cond8 subj_Ts_indConds_mat
end