% Generate new Surface distance/Euclidian distance matrices

clear;
addpath('~/Dropbox/LAB_Folder_Macbook/MATLAB/Code/FastEMD_PeleWerman/');
addpath('~/Dropbox/LAB_Folder_Macbook/MATLAB/Utilities/');

CalcType = 'actmaps';
disp(CalcType);

for sub = 1:5
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Generate ground distance matrices
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    disp(['Analyzing subject' num2str(sub)]);
    subj_dir = ['~/Dropbox/LAB_Folder_Macbook/MATLAB/Data/haxby2001/subj' num2str(sub)];
    subj_surfdir = ['~/Dropbox/LAB_Folder_Macbook/MATLAB/Data/haxby2001/subj' num2str(sub) '_SurfCoords'];
    
    addpath(subj_dir);
    addpath(subj_surfdir);
    cd(subj_dir);
    
    disp('Load ground distance matrices');
    GDFile = ['Subj' num2str(sub) '_GDMatrices.mat'];
    load(GDFile);
    
    if strcmp(CalcType,'actmaps')
        disp('Load activity maps (generated based on individual VT masks)')
        ActFile = ['subj' num2str(sub) '_actmaps_VT_mc_ss.mat'];
        load(ActFile);
        Subj_data = double(eval(['subj' num2str(sub) '_data']))';
    elseif strcmp(CalcType,'tmaps')
        disp('Load t maps (generated based on individual VT masks)')
        tmapsFile = ['subj' num2str(sub) '_tmaps_VT_mc_ss.mat'];
        load(tmapsFile);
        Subj_data = double(eval(['subj' num2str(sub) '_tmaps']))';
    end
    
    % Generate activity map's vector
    Subj_coords = double(eval(['subj' num2str(sub) '_coords']));
    disp([num2str(size(Subj_coords,1)) ' voxels are selected from the VT mask (PyMVPA)'])
    disp([num2str(size(SingleNodeInfo,1)) ' voxels are used for cortical surface distance calculation (AFNI)'])
    
    % Subscripts to indices
    imgsize = [40,64,64];
    Subj_coords = [Subj_coords zeros(size(Subj_coords,1),1)];
    for ctr = 1:size(Subj_coords,1)
        Subj_coords(ctr,4) = sub2ind(imgsize,Subj_coords(ctr,1), ...
            Subj_coords(ctr,2) + 1, ...
            Subj_coords(ctr,3) + 1);
    end
    Subj_coords = [Subj_coords(:,4) Subj_coords(:,1:3)];
    
    % Sort according to each voxel's index and see whether it's same to
    % what afni generated
    % [~,IX] = sort(Subj_coords(:,1));
    % Subj_coords_sorted = Subj_coords(IX,:);
    PyMVPA_coords_val = [Subj_coords Subj_data];
    
    % Find corresponding values based on coordinates
    Vals = zeros(size(SingleNodeInfo,1),8);
    for ctr = 1:size(SingleNodeInfo,1)
        Vals(ctr,:) = PyMVPA_coords_val(PyMVPA_coords_val(:,1) == SingleNodeInfo(ctr,2),5:12);
    end
    Afni_coors_val = [SingleNodeInfo Vals];
        
    if strcmp(CalcType,'actmaps')
        Afni_coors_val_HDR = {'Local idx','index','Coords','Act maps'};
    elseif strcmp(CalcType,'tmaps')
        Afni_coors_val_HDR = {'Local idx','index','Coords','tmaps'};
    end
    
    % Generate symmetric matirces
    GD_Euc_com = GD_Euc + GD_Euc';
    GD_Surf_com = GD_Surf + GD_Surf';
    
    % Test 2: assign different values based on Euclidian distance matrix
    % It has been noticed that two clusters could be generated after
    % replacing -1 with different values
    
    factors = [2 5 7 8 9 10];
    per = 0.1:0.2:0.9;
    
    for ctri = 1:length(factors)
        temp1 = GD_Surf;
        idx = find(temp1 == -1);
        updated_val = GD_Euc(idx) * factors(ctri);
        temp1(idx) = updated_val;
        GD_Surf_com_newval = temp1 + temp1';
        eval(['GD_Surf_com_fac' num2str(factors(ctri)) ' = GD_Surf_com_newval;'])
        
        % Threshold the new ground distance matrices
        for ctrj = 1:length(per)
            thr = quantile(GD_Surf_com_newval(:),per(ctrj));
            GD_Surf_com_newval_thr = min(GD_Surf_com_newval,thr);
            eval(['GD_Surf_com_fac' num2str(factors(ctri)) '_thr' num2str(per(ctrj) * 100) ' = GD_Surf_com_newval_thr;'])
            clear GD_Surf_com_newval_thr
        end
    end
    
    % ------------ Updated 06/30/2015: Use values to replace -1 directly
    temp2 = GD_Surf;
    temp_fac = 100;
    temp2(idx) = GD_Euc_com(idx) * temp_fac;
    GD_Surf_com_fac100 = temp2 + temp2';
    
    % Save all GD matrices
    GroundDistFiles = who('GD_*');
    
    % Save ground truth distance matrix and corresponding data
    if strcmp(CalcType,'actmaps')
        EMD_PrefileName = ['EMD_pre_subj' num2str(sub) '_mc_ss_VT2.mat'];
    elseif strcmp(CalcType,'tmaps')
        EMD_PrefileName = ['EMD_pre_subj' num2str(sub) '_mc_ss_VT_tmaps2.mat'];
    end
    
    save(fullfile(subj_dir,EMD_PrefileName),'Afni_coors_val','Afni_coors_val_HDR');
    for ctr = 1:length(GroundDistFiles)
       save(fullfile(subj_dir,EMD_PrefileName),GroundDistFiles{ctr},'-append');
    end
    
    disp(['Ground distance matrices for subject ' num2str(sub) ' is done']);

end
