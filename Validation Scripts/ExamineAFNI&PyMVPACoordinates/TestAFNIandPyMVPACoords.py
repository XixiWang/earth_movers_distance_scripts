from mvpa2.suite import * 
import os
import scipy

num_subjs = 1

pymvpa_datadbroot = '/Users/XixiWang/Documents/MATLAB/Data/haxby2001'

for subj_num in range(num_subjs):
    subj_string = 'subj' + str(subj_num + 1)
    subjpath = os.path.join(pymvpa_datadbroot, subj_string)

    ds = mvpa2.datasets.mri.fmri_dataset(samples=os.path.join(subjpath,'bold_raw_mc_mean.nii'), mask=os.path.join(subjpath, 'mask4_vt.nii.gz'))
    
    print ds
    Coords = ds.fa.voxel_indices
    Data = ds.samples
    print Coords
    print Data

    data = {}
    data['PyMVPA_Coords'] = Coords
    data['PyMVPA_Data'] = Data

    FileName = subj_string + '_PyMVPA_DS.mat'
    scipy.io.savemat(FileName,data)
