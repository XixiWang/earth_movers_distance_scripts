% Examine AFNI extracted coordinates and PyMVPA extracted coordinates

% Use PyMVPA to generate masked bold_raw_mc_mean.nii file
% Adopted from Python code (.py)
% ------------------------------------------------------------------------
from mvpa2.suite import * 
import os
import scipy


pymvpa_datadbroot = '/axelUsers/xwang91/Documents/MATLAB/Data/haxby2001'

subj_string = 'subj4'
subjpath = os.path.join(pymvpa_datadbroot, subj_string)

ds = mvpa2.datasets.mri.fmri_dataset(samples=os.path.join(subjpath,'bold_raw_mc_mean.nii'), mask=os.path.join(subjpath, 'mask4_vt.nii.gz'))

print ds
Coords = ds.fa.voxel_indices
Data = ds.samples
print Coords
print Data

data = {}
data['PyMVPA_Coords'] = Coords
data['PyMVPA_Data'] = Data

FileName = subj_string + '_PyMVPA_DS.mat'
scipy.io.savemat(os.path.join(subjpath,FileName),data)
% ------------------------------------------------------------------------

% Use AFNI to generate masked bold_raw_mc_mean.nii file
% Adopted from bash commands (.sh)
% ------------------------------------------------------------------------
% cd ..
% 3dmaskdump -cmask '-a mask4_vt+orig[0] -expr ispositive(a-0)' -index -xyz -o maskedfile2 bold_raw_mc_mean.nii
% ------------------------------------------------------------------------

% I then checked the matrices and found out even though the voxels were
% extracted in different orders, their corresponding values were same. ijk
% coordinates were also same. I noticed that the xyz coordinates differed
% (they depend on different coordinates system? )
load(['subj' num2str(nsub) '_PyMVPA_DS.mat']);
tempa = [double(PyMVPA_Coords) PyMVPA_Data'];
% check with maskedfile.txt