clear;

addpath('~/Dropbox/LAB_Folder_Macbook/MATLAB/Code/FastEMD_PeleWerman/');
addpath('~/Dropbox/LAB_Folder_Macbook/MATLAB/Utilities/');
addpath(pwd);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Check dataset directory
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
[~,hostname] = system('hostname');

sendgmail_init;

% Calculation type: 'actmaps' or 'tmaps'
CalcType = 'actmaps';
disp(CalcType);

% Calculate fast EMD (histograms don't have to be normalized)
extra_mass_penalty = -1;
% For Rubner's EMD, extra_mass_penalty = 0

parpool;
for sub = 2
    if sub ~= 5
        ncomp = 12 * 8;
    elseif sub == 5
        ncomp = 11 * 8;
    end
    all_comp_pairs = nchoosek(1:ncomp,2);
    
    disp(['Analyzing subject' num2str(sub)]);
    
    if length(hostname) == 18 % Lab-Mac
        subj_dir = ['~/Documents/Data/haxby2001/subj' num2str(sub)];
    elseif length(hostname) == 39 % Macbook
        subj_dir = ['~/Dropbox/LAB_Folder_Macbook/MATLAB/Data/haxby2001/subj' num2str(sub)];
    end
    cd(subj_dir);
    
    if strcmp(CalcType,'actmaps')
        EMD_PrefileName = ['EMD_pre_subj' num2str(sub) '_allchunks_VT.mat'];
    end
    load(EMD_PrefileName);
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Correlation calculation
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    CorrMtrx = zeros(ncomp * (ncomp - 1)/2,1);
    for comppairs = 1:size(all_comp_pairs,1)
        condA = Afni_coors_val(:,5 + all_comp_pairs(comppairs,1));
        condB = Afni_coors_val(:,5 + all_comp_pairs(comppairs,2));
        
        CorrMtrx(comppairs,1) = corr(condA,condB);
    end
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % EMD calculation
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Load ground distance files
    GroundDistFiles = whos('GD_*','-file',['EMD_pre_subj' num2str(sub) '_mc_ss_VT2.mat']);
    for ctr = 1:length(GroundDistFiles)
        load(['EMD_pre_subj' num2str(sub) '_mc_ss_VT2.mat'],GroundDistFiles(ctr).name);
    end
    clear GD_Euc GD_Surf;
    
    GroundDistFiles = who('GD_*');
    % Examine all ground distance files
    % for ctr = 1:length(GroundDistFiles)
    for ctr = [1,7]
        % Load different ground distance matrix
        GDMtrx = GroundDistFiles{ctr};
        disp(GDMtrx);
        GDMtrxVal = eval(GDMtrx);
        EMDMtrx = zeros(ncomp * (ncomp - 1)/2,1);
        EMDMtrx_Rub = zeros(ncomp * (ncomp - 1)/2,1);
        
        tic;
        parfor comppairs = 1:size(all_comp_pairs,1)
            condA = Afni_coors_val(:,5 + all_comp_pairs(comppairs,1));
            condB = Afni_coors_val(:,5 + all_comp_pairs(comppairs,2));
            
            EMDMtrx(comppairs,1) = emd_hat_gd_metric_mex(condA,condB,GDMtrxVal,-1);
            EMDMtrx_Rub(comppairs,1) = emd_hat_gd_metric_mex(condA,condB,GDMtrxVal,0);
        end
        tEla = toc;
        
        eval([strcat('EMD_fin',GDMtrx(3:end)) ' = EMDMtrx;'])
        eval([strcat('EMD_fin',GDMtrx(3:end),'_Rub') ' = EMDMtrx_Rub;'])
    end
    sendmail('xixi.wang577@gmail.com','Haxby data - Sub1 is done!');
    
    FileName = ['EMD_subj' num2str(sub) '_mc_ss_VT_allchunks.mat'];
    EMDResultsFile = who('EMD_fin*');
    save(fullfile(subj_dir,FileName),'CorrMtrx');
    for ctr = 1:length(EMDResultsFile)
        save(fullfile(subj_dir,FileName),EMDResultsFile{ctr},'-append');
    end
    
    disp(['EMD calculation for subject ' num2str(sub) ' is done']);
end

delete(gcp);