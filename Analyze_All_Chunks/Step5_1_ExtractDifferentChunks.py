__author__ = 'xixiwang'

from mvpa2.suite import *
from mvpa2.misc.io.base import SampleAttributes
from mvpa2.datasets.mri import fmri_dataset
import scipy
import scipy.stats
import os
import numpy as np

# Change the location of the root directory, for your local machine:
pymvpa_datadbroot = '/Users/XixiWang/Dropbox/LAB_Folder_Macbook/MATLAB/Data/haxby2001'
# pymvpa_datadbroot = '/raizadaUsers/xwang91/Dropbox/LAB_Folder_Macbook/MATLAB/Data/haxby2001'

# Exclude subject 6
num_subjs = 5
num_categories = 8
num_squareform_vals = num_categories * (num_categories-1) / 2
all_subjs_squareform_sims = scipy.zeros((num_subjs,num_squareform_vals),float)

for subj_num in [4]:
    subj_string = 'subj' + str(subj_num + 1)
    # set path for each subject
    subjpath = os.path.join(pymvpa_datadbroot, subj_string)
    # Read PyMVPA sample attribute definitions from text files
    attrs = SampleAttributes(os.path.join(subjpath, 'labels.txt'),header=True)

    # For subject 5, chunk 8 was corrupted
    if subj_num == 4:
        all_chunk = [0, 1, 2, 3, 4, 5, 6, 7, 9, 10, 11]
    else:
        all_chunk = np.unique(attrs.chunks)
    all_labels = np.unique(attrs.labels)
    # Delete rest chunk
    all_labels_norest = np.delete(all_labels, 5)

    print " ----------------- About to import the dataset for " + subj_string
    # Create a dataset from an fMRI timeseries image (samples,targets,chunks,mask)
    my_dataset = fmri_dataset(samples=os.path.join(subjpath, 'bold_raw_mc.nii'), targets=attrs.labels,
                              chunks=attrs.chunks, mask=os.path.join(subjpath, 'mask4_vt.nii.gz'))

    # Extract coordinates of VOIs
    print " ----------------- Extract coordinates of VOI"
    Coords = my_dataset.fa.voxel_indices

    # Perform detrending
    detrender = PolyDetrendMapper(polyord=1, chunks_attr='chunks')
    detrended_dataset = my_dataset.get_mapped(detrender)

    # Data normalization: scales all voxels into the same range and removes the mean
    zscore(detrended_dataset, param_est=('targets', ['rest']))

    averager = mean_group_sample(['targets'])
    mat_dict = {}
    
    print "----------------- Export data for a specific chunk"
    for labelnum in range(len(all_labels_norest)):
        for chunknum in range(len(all_chunk)):
            dataval = detrended_dataset[(detrended_dataset.sa.targets == all_labels_norest[labelnum]) & (detrended_dataset.sa.chunks == all_chunk[chunknum])]
            # Calculate mean avalues for each block/chunk
            dataval_mean = dataval.get_mapped(averager)

            # Save to dict
            # mat_dict['subj' + str(subj_num + 1) + '_' + all_labels_norest[labelnum] + '_chunk' + str(all_chunk[chunknum]).zfill(2)] = dataval.samples
            mat_dict['subj' + str(subj_num + 1) + '_' + all_labels_norest[labelnum] + '_chunk' + str(all_chunk[chunknum]).zfill(2) + '_mean'] = dataval_mean.samples

    mat_dict['coords'] = Coords
    print " ----------------- Export data without rest state"
    detrended_dataset_without_rest_periods = detrended_dataset[detrended_dataset.sa.targets != 'rest']
    detrended_dataset_without_rest_periods_values = detrended_dataset_without_rest_periods.samples

    print " ----------------- File I/O"
    FileName = os.path.join(subjpath, subj_string + '_SeparateBlocks')
    scipy.io.savemat(FileName, mat_dict)

    print subj_string + " is done!"
