%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Extract activation maps for each chunk for all five subjects
% 
% Afni_coords_val contains activity maps extracted from AFNI and the
% corresponding coordinates information
% 
% PyMVPA_coords_val contains activity maps extracted from PyMVPA using VT
% cortex masks and the corresponding coordinates information
% 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

clear;
clc;
addpath('~/Dropbox/LAB_Folder_Macbook/MATLAB/Utilities/');

CalcType = 'actmaps';
disp(CalcType);

for sub = 5
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Generate ground distance matrices
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    disp(['Analyzing subject' num2str(sub)]);
    subj_dir = ['~/Dropbox/LAB_Folder_Macbook/MATLAB/Data/haxby2001/subj' num2str(sub)];
    subj_surfdir = ['~/Dropbox/LAB_Folder_Macbook/MATLAB/Data/haxby2001/subj' num2str(sub) '_SurfCoords'];
    
    addpath(subj_dir);
    addpath(subj_surfdir);
    cd(subj_dir);
    
    % Load activation file for each chunk
    ActFile = ['subj' num2str(sub) '_SeparateBlocks.mat'];
    load(ActFile);
    Subj_coords = double(coords);
    
    disp('Load ground distance matrices');
    GDFile = ['Subj' num2str(sub) '_GDMatrices.mat'];
    load(GDFile);
    
    disp([num2str(size(Subj_coords,1)) ' voxels are selected from the VT mask (PyMVPA)'])
    disp([num2str(size(SingleNodeInfo,1)) ' voxels are used for cortical surface distance calculation (AFNI)'])
    
    % Subscripts to indices
    imgsize = [40,64,64];
    Subj_coords = [Subj_coords zeros(size(Subj_coords,1),1)];
    for ctr = 1:size(Subj_coords,1)
        Subj_coords(ctr,4) = sub2ind(imgsize,Subj_coords(ctr,1), ...
            Subj_coords(ctr,2) + 1, ...
            Subj_coords(ctr,3) + 1);
    end
    Subj_coords = [Subj_coords(:,4) Subj_coords(:,1:3)];
    
    % Sort according to each voxel's index and see whether it's same to
    % what afni generated
    % [~,IX] = sort(Subj_coords(:,1));
    % Subj_coords_sorted = Subj_coords(IX,:);
    PyMVPA_coords_val = Subj_coords;
    mean_val_names = whos('*_mean');
    if (sub ~= 5 && length(mean_val_names) == 96) || (sub == 5 && length(mean_val_names) == 88) % 8 conditions, 12 blocks in total
        for ctrj = 1:length(mean_val_names)
            PyMVPA_coords_val = [PyMVPA_coords_val eval(mean_val_names(ctrj).name)'];
        end
    else
        disp('Something is wrong with the *_mean variables!')
    end
    PyMVPA_coords_val_HDR = {'Index','Coords','Act Maps for each chunk'};   
    
    % Find corresponding values based on coordinates
    % Vals = zeros(size(SingleNodeInfo,1),5 + 96);
    for ctr = 1:size(SingleNodeInfo,1)
        Vals(ctr,:) = PyMVPA_coords_val(PyMVPA_coords_val(:,1) == SingleNodeInfo(ctr,2),5:end);
    end
    Afni_coors_val = [SingleNodeInfo Vals];
    Afni_coors_val_HDR = {'Local idx','Index','Coords','Act Maps for each chunk'};
    
    % --------------------------------------------------------------------
    EMD_pre_filename = ['EMD_pre_subj' num2str(sub) '_allchunks_VT.mat'];
    % --------------------------------------------------------------------
    save(fullfile(subj_dir,EMD_pre_filename),'PyMVPA_coords_val','PyMVPA_coords_val_HDR',...
        'Afni_coors_val','Afni_coors_val_HDR');
    
    disp(['Subject ' num2str(sub) ' is done']);
    
    clear;
end