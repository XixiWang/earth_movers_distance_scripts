% Calculate distance between different pairs and pick out most different
% condition pairs

% Update: for MDS plots, I'm using metricsstress criterion to project both
% correlation and EMD results right now. It could be seen from the MDS
% plots that for subj 1-5, EMD is able to distinguish between animate vs.
% inanimate categories while only for subj 1,2,3 and 5, correlation is able
% to distinguish betwee animate vs. inanimate categories.

clear
close all;

addpath('~/Dropbox/LAB_Folder_Macbook/MATLAB/Utilities/');

labels = {'bottle' 'cat' 'chair' 'face' 'house' 'scissors' 'scrambledpix' 'shoe'};
criterions = {'stress','sstress','metricstress','metricsstress','sammon','strain'};

paired_conds = zeros(28,2);
a = 1;
for ctri = 1:8
    for ctrj = 1:8
        if  ctri < ctrj
            paired_conds(a,1) = ctri;
            paired_conds(a,2) = ctrj;
            a = a + 1;
        end
    end
end

Sub_dir = '~/Dropbox/LAB_Folder_Macbook/MATLAB/Data/haxby2001/';
% Mat file name
MatName = 'mc_ss_VT_new3';
tempFile = fullfile(Sub_dir,'subj1',['EMD_subj1_' MatName '.mat']);
AllVarNames = who('-file',tempFile);

CalcType = 'actmaps';
EMDType = 'EMDVec';

EMD_MDS_criterion = 'metricsstress';
Corr_MDS_criterion = 'metricsstress';

disp(CalcType);
disp(EMDType);

% Generate group matrix
disp(AllVarNames{1});
CorrMtrx = GenerateGroupMtrx(MatName,CalcType,AllVarNames{1});

% Select a possible EMD matrix
% -------------------------------
varname = 36;
% -------------------------------
disp(AllVarNames{varname});
EMDMtrx = GenerateGroupMtrx(MatName,CalcType,AllVarNames{varname});
EMDMtrx_mat = mat2gray(EMDMtrx);

for ctr = 1:5
    subj_str = ['Subject ' num2str(ctr)];
    subj_dir = fullfile(Sub_dir,['subj' num2str(ctr)]);
    cd(subj_dir);
    
    % Convert similarity values to dissimilarities
    CorrVec = CorrMtrx(ctr,:);
    DisCorrVec = 1 - CorrVec;
    
    % Use dissimilarity values directly
    EMDVec = EMDMtrx(ctr,:);
    EMDVec_norm = EMDMtrx_mat(ctr,:);
    
    if strcmp(EMDType,'EMDVec')
        EMDResultVec = EMDVec;
    elseif strcmp(EMDType, 'EMDVec_norm')
        EMDResultVec = EMDVec_norm;
    end
    opts = statset('MaxIter',100000,'Display','off');
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Project EMD results
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % For EMD results, try non-metric scaling and metric scaling
    % Non-metric scaling
    [temp1,~,disparities] = mdscale(EMDResultVec,2,'criterion','stress','Options',opts);
    dist1 = pdist(temp1);
    temp2 = mdscale(EMDResultVec,2,'criterion','sstress','Options',opts);
    dist2 = pdist(temp2);
    % Metric scaling
    temp3 = mdscale(EMDResultVec,2,'criterion','metricstress','Options',opts);
    dist3 = pdist(temp3);
    temp4 = mdscale(EMDResultVec,2,'criterion','metricsstress','Options',opts);
    dist4 = pdist(temp4);
    temp5 = mdscale(EMDResultVec,2,'criterion','sammon','Options',opts);
    dist5 = pdist(temp5);
    temp6 = mdscale(EMDResultVec,2,'criterion','strain','Options',opts);
    dist6 = pdist(temp6);
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Project correlation results
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Non-metric scaling
    if ctr == 5 % For subject 5
        corrdist1 = [];
    else
        corrtemp1 = mdscale(DisCorrVec,2,'criterion','stress','Options',opts);
        corrdist1 = pdist(corrtemp1);
    end
    corrtemp2 = mdscale(DisCorrVec,2,'criterion','sstress','Options',opts);
    corrdist2 = pdist(corrtemp2);
    % Metric scaling
    corrtemp3 = mdscale(DisCorrVec,2,'criterion','metricstress','Options',opts);
    corrdist3 = pdist(corrtemp3);
    corrtemp4 = mdscale(DisCorrVec,2,'criterion','metricsstress','Options',opts);
    corrdist4 = pdist(corrtemp4);
    corrtemp5 = mdscale(DisCorrVec,2,'criterion','sammon','Options',opts);
    corrdist5 = pdist(corrtemp5);
    corrtemp6 = mdscale(DisCorrVec,2,'criterion','strain','Options',opts);
    corrdist6 = pdist(corrtemp6);
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Figure initialization
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    scrsz = get(0,'ScreenSize');
    figure('Position',[1 scrsz(4) scrsz(3)*0.5 scrsz(4)*0.5])
    % figure('Position',[1 scrsz(4) scrsz(3) scrsz(4)])
    set(gcf,'Color',[1 1 1]);
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Plot EMD - MDS results
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    subplot(1,2,1);
    EMD_MDS_cri_idx = find(strcmp(EMD_MDS_criterion,criterions));
    EMD_MDS_cri_coords = eval(['temp' num2str(EMD_MDS_cri_idx)]);
    
    plot(EMD_MDS_cri_coords(:,1),EMD_MDS_cri_coords(:,2),'.','MarkerSize',2);
    text(EMD_MDS_cri_coords(:,1),EMD_MDS_cri_coords(:,2),labels,'Color','r', ...
        'FontSize',12,'FontWeight','bold');
    set(gca,'xticklabel',{[]});set(gca,'yticklabel',{[]});
    % title(strcat('EMD-',EMD_MDS_criterion));
    title('Neural similarity space in 2D MDS - based on EMD', ...
        'FontSize',12,'FontWeight','bold');
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Plot correlation - MDS results
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    subplot(1,2,2);
    Corr_MDS_cri_idx = find(strcmp(Corr_MDS_criterion,criterions));
    Corr_MDS_cri_coords = eval(['corrtemp' num2str(Corr_MDS_cri_idx)]);
    plot(Corr_MDS_cri_coords(:,1),Corr_MDS_cri_coords(:,2),'.','MarkerSize',2);
    text(Corr_MDS_cri_coords(:,1),Corr_MDS_cri_coords(:,2),labels,'Color','r', ...
        'FontSize',12,'FontWeight','bold');
    set(gca,'xticklabel',{[]});set(gca,'yticklabel',{[]});
    % title(strcat('Correlation-',Corr_MDS_criterion));
    title('Neural similarity space in 2D MDS - based on correlation', ...
        'FontSize',12,'FontWeight','bold');
    
    annotation(gcf,'textbox',[0.02 0.75 0.2 0.2],'String',subj_str, ...
        'FitBoxToText','on','FontSize',14,'FontWeight','bold',...
        'LineStyle','none','EdgeColor','none')
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Calculate between pairs distance
    % 1. Sort indices
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    disp('Calculate between pairs distance')
    DistData_ori = zeros(28,4);
    DistData_ori(:,1:2) = paired_conds;
    DistData_ori(:,3) = pdist(EMD_MDS_cri_coords,'euclidean')';
    DistData_ori(:,4) = pdist(Corr_MDS_cri_coords,'euclidean')';
    DistData_ori_HDR = {'paired idx','EMD dist','Corr dist'};

    EMD_MDS_dist = pdist(EMD_MDS_cri_coords,'euclidean')';
    Corr_MDS_dist = pdist(Corr_MDS_cri_coords,'euclidean')';
    
    [EMD_MDS_dist_sorted, Idx1] = sort(EMD_MDS_dist,'descend');
    [Corr_MDS_dist_sorted, Idx2] = sort(Corr_MDS_dist,'descend');
    
    % For each paired conditions, calculate the distance between using EMD
    % and correlation and find out the biggest difference
    Ori_Idx = (1:28)';
    Ori_Idx1 = zeros(28,1);
    Ori_Idx2 = zeros(28,1);
    
    for ctrk = 1:28
        Ori_Idx1(ctrk,1) = Ori_Idx(Idx1 == ctrk);
        Ori_Idx2(ctrk,1) = Ori_Idx(Idx2 == ctrk);
    end
    
    Idx_diff = abs(Ori_Idx1 - Ori_Idx2);
    DistData = [DistData_ori Ori_Idx1 Ori_Idx2 Idx_diff];
    [temp_Idx_diff,tempIdx] = sort(Idx_diff,'descend');
    
    % Select top 5 different condition pairs
    MostDiffPairs = zeros(5,7);
    for ctrr = 1:5
        MostDiffPairs(ctrr,:) = DistData(tempIdx(ctrr),:);
    end
   
    % Generate condition labels 
    MostDiffLabels = cell(5,2);
    for ctry = 1:5
        MostDiffLabels{ctry,1} = labels{MostDiffPairs(ctry,1)};
        MostDiffLabels{ctry,2} = labels{MostDiffPairs(ctry,2)};
    end
    
    % Here the idea is to calculate the difference between the sorted
    % indices
    T1 = table(MostDiffLabels,MostDiffPairs(:,1:2),MostDiffPairs(:,3),MostDiffPairs(:,4),...
               MostDiffPairs(:,5),MostDiffPairs(:,6),MostDiffPairs(:,7),...
               'VariableNames',{'Conditions','ConditionIdx','EMD_MDS_dist','Corr_MDS_dist',...
               'EMD_MDS_Idx','Corr_MDS_Idx','IdxDiff'});
    FileName = ['subj' num2str(ctr)  '_Diff_Cond_Pairs_' AllVarNames{varname}];
    % writetable(T1,FileName);
   
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Calculate between pairs distance
    % 2. Normalization method
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    EMD_MDS_dist_norm = EMD_MDS_dist ./ max(EMD_MDS_dist(:));
    Corr_MDS_dist_norm = Corr_MDS_dist ./ max(Corr_MDS_dist(:));
    
    CondPairs_ratio = EMD_MDS_dist_norm ./ Corr_MDS_dist_norm;
    CondPairs_norm = [paired_conds EMD_MDS_dist_norm Corr_MDS_dist_norm CondPairs_ratio];

    [CondPairs_ratio_sorted,tempIdx2] = sort(CondPairs_ratio,'descend');
    MostDiffLabels2 = cell(5,2);
    MostDiffPairs2 = zeros(5,2);
    EMD_MDS_dist2 = zeros(5,1);
    Corr_MDS_dist2 = zeros(5,1);
    Ratio2 = zeros(5,1);
    for ctra = 1:5
        MostDiffPairs2(ctra,:) = paired_conds(tempIdx2(ctra),:);
        MostDiffLabels2{ctra,1} = labels{MostDiffPairs2(ctra,1)};
        MostDiffLabels2{ctra,2} = labels{MostDiffPairs2(ctra,2)};
        EMD_MDS_dist2(ctra,1) = EMD_MDS_dist_norm(tempIdx2(ctra));
        Corr_MDS_dist2(ctra,1) = Corr_MDS_dist_norm(tempIdx2(ctra));
        Ratio2(ctra,1) = CondPairs_ratio(tempIdx2(ctra));
    end
    
    T2 = table(MostDiffLabels2,MostDiffPairs2,EMD_MDS_dist2,Corr_MDS_dist2,Ratio2,...
        'VariableNames',{'Conditions','ConditionIdx','EMD_MDS_normdist','Corr_MDS_normdist',...
         'Ratio'});
    FileName2 = ['subj' num2str(ctr)  '_Diff_Cond_Pairs_norm_' AllVarNames{varname}];
    writetable(T2,FileName2);
    
    disp(T1)
    disp(T2)
    
    close all;
end

