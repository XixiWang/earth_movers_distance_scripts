% Peform correlation/EMD comparison
clear;
close all;

addpath('~/Dropbox/');
addpath('~/Dropbox/LAB_Folder_Macbook/MATLAB/Utilities/');

labels = {'bottle','cat','chair','face','house','scissors','scrambledpix','shoe'};
criterions = {'stress','sstress','metricstress','metricsstress','sammon','strain'};
paired_conds = zeros(28,2);
paired_conds_labels = cell(28,2);
a = 1;
for ctri = 1:8
    for ctrj = 1:8
        if ctri < ctrj
           paired_conds(a,1) = ctri;
           paired_conds_labels(a,1) = labels(ctri);
           paired_conds(a,2) = ctrj;
           paired_conds_labels(a,2) = labels(ctrj);
           a = a + 1;
        end
    end
end

Sub_dir = '~/Dropbox/LAB_Folder_Macbook/MATLAB/Data/haxby2001';
MatName = 'mc_ss_VT_new3';
tempFile = fullfile(Sub_dir,'subj1',['EMD_subj1_' MatName '.mat']);
AllVarNames = who('-file',tempFile);

% Select dataset type: actmaps/tmaps
% EMD vectors or normalized EMD vectors
CalcType = 'actmaps';
EMDType = 'EMDVec';

disp(CalcType);
disp(EMDType);

% Generate group matrix 
% Correlation matrix
disp(AllVarNames{1});
CorrMtrx = GenerateGroupMtrx(MatName,CalcType,AllVarNames{1});
% EMD matrix
varname = 36;
disp(AllVarNames{varname});
EMDMtrx = GenerateGroupMtrx(MatName,CalcType,AllVarNames{varname});
EMDMtrx_mat = mat2gray(EMDMtrx);

% List all EMD values
for nsub = 1:5
    subj_str = ['Subject ' num2str(nsub)];
    subj_dir = fullfile(Sub_dir,['subj' num2str(nsub)]);
    cd(subj_dir);
    
    EMDval_ind = EMDMtrx(nsub,:);
    Corrval_ind = CorrMtrx(nsub,:);
    
    % Sort all EMD vals
    [EMDval_ind_sorted,Idx1] = sort(EMDval_ind,'ascend');
    EMDval_ind_sorted_conds = paired_conds(Idx1,:);
    % Sort all corr vals 
    [Corrval_ind_sorted,Idx2] = sort(Corrval_ind,'descend');
    Corrval_ind_sorted_conds = paired_conds(Idx2,:);
    
    % Combine all sorting results and save them in a table
    T1 = table(EMDval_ind_sorted',EMDval_ind_sorted_conds,...
        Corrval_ind_sorted',Corrval_ind_sorted_conds,...
        'VariableNames',{'EMD_sorted','ConditionIdx1',...
        'Corr_sorted','ConditionIdx2'});
    
    T2 = table(EMDval_ind_sorted',Idx1',EMDval_ind_sorted_conds,...
        Corrval_ind_sorted',Idx2',Corrval_ind_sorted_conds,...
        'VariableNames',{'EMD_sorted','OriIdx1','ConditionIdx1',...
        'Corr_sorted','OriIdx2','ConditionIdx2'});
    
    % Find original indices
    Idx1_ori = zeros(28,1);
    Idx2_ori = zeros(28,1);
    for ctr = 1:28
        Idx1_ori(ctr) = find(Idx1 == ctr);
        Idx2_ori(ctr) = find(Idx2 == ctr);
    end
   
    T3 = table(paired_conds_labels,EMDval_ind',Corrval_ind',...
        Idx1_ori,Idx2_ori,abs(Idx1_ori - Idx2_ori),...
        'VariableNames',{'PairedConds','EMD','Corr'...
        'EMD_idx','Corr_idx','IdxDiff'});
    
    % Select most different condition pairs
    [Idx_Diff_sorted,Idx_Diff_idx] = sort(abs(Idx1_ori - Idx2_ori),'descend');
    Idx_Diff_idx_sel = Idx_Diff_idx(1:5);
    Idx_Diff_idx_sel_cond_pairs = paired_conds(Idx_Diff_idx_sel,:);
        
    table_name = ['subj' num2str(nsub) '_Diff_Cond_Pairs_ori_' AllVarNames{varname}];
    % writetable(T3,table_name);
    disp(T3);
    
end