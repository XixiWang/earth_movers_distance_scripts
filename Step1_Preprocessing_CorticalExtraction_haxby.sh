#! /bin/bash

export PATH=${PATH}:/usr/local/bin

echo "Bash script to analyze haxby dataset"

##################################################
############ DATA PREPROCESSING ##################
##################################################
# Loop through different subjects
for nsub in subj1 subj2 subj3 subj4 subj5
do
    # export SUBPATH=~/Documents/MATLAB/Data/haxby2001/$nsub
    export SUBPATH=~/Dropbox/LAB_Folder_Macbook/MATLAB/Data/haxby2001/$nsub
    echo $SUBPATH
    cd $SUBPATH
    ls -l

    # Modify the anatomical data's header information
    if [ -f "anat_new.nii" ];then
            echo "New anatomical data exists"
            mri_info anat_new.nii
    
        else 
            echo "Display origianl anatomical data header file"
            fslhd anat.nii
            echo "Modify the header information of the original anatomical data"
            nifti_tool -mod_hdr -infiles anat.nii -prefix anat_new.nii -mod_filed qform_code '1'
            fslhd anat_new.nii
    fi

    # Preprocessing the BOLD data
    # Adapted from PyMVPA website
    ls -l
    if [ -f "bold_raw_mc_mean.nii.gz" -a "bold_raw_mc.nii" ];then
            echo "Preprocessed functional data exists"
    
        elif [ -f "bold.nii" ];then
            echo "Preprocess functional data"
            # BET extraction
            bet bold bold_raw_brain -F -f 0.5 -g 0
            # Motion correction
            mcflirt -in bold_raw_brain -out bold_raw_mc.nii.gz -plots
            # Calculate the mean BOLD data
            3dTstat -prefix bold_raw_mc_mean bold_raw_mc.nii.gz
            # Generate corresponding NIFTI file
            3dAFNItoNIFTI -prefix bold_raw_mc_mean.nii.gz bold_raw_mc_mean+tlrc
            fslhd bold_raw_mc_mean.nii.gz
 
        else
            echo "no raw bold data exists!"    
    fi
    ls -l

    ##################################################
    ######## CORTICAL SURFACE EXTRACTION #############
    ##################################################

    # Cortical surface extraction using Freesurfer
    cd ..
    echo "Current directory is ${PWD}"
    ls -l  
    if [ -d "${nsub}_fs_new_anat_all" ];then
            echo "Cortical surface extraction has been finished"
            echo "The output folder is ${nsub}_fs_new_anat_all"

        else
            echo "Perform cortical surface extraction using freesurfer" 
            recon-all -s ${nsub}_fs_new_anat_all -i $SUBPATH/anat_new.nii -all -cw256
    fi

    # Examine the surface extraction results
    export SURFACEPATH=${nsub}_fs_new_anat_all
    echo "Surface directory is ${SURFACEPATH}"
    # Visualize cortical surface extraction results
    cd $SURFACEPATH
    ls -l
    freeview -v mri/T1.mgz mri/wm.mgz mri/brainmask.mgz mri/aseg.mgz:colormap=lut:opacity=0.2 -f surf/lh.white:edgecolor=blue surf/lh.pial:edgecolor=yellow surf/rh.white:edgecolor=blue surf/rh.pial:edgecolor=yellow 

done
