addpath('~/Dropbox/LAB_Folder_Macbook/MATLAB/Toolboxes/spm8/');

clear;
labels = {'bottle','cat','chair','face','house','scissors','scrambledpix','shoe'};
ncond = 8;

for sub = 1
    subj_dir = ['~/Dropbox/LAB_Folder_Macbook/MATLAB/Data/haxby2001/subj' num2str(sub)];
    cd(subj_dir);
    
    load(['subj' num2str(sub) '_actmaps_VT_mc_ss.mat']);
    disp(['Analyzing subject ' num2str(sub)]);
    
    if exist('bold_raw_mc_mean.nii','file')
        disp('bold_raw_mc_mean.nii exists');
        hdrinfo = spm_vol('bold_raw_mc_mean.nii');    
    else
        disp('unzip .nii.gz file')
        gunzip('bold_raw_mc_mean.nii.gz')
        hdrinfo = spm_vol('bold_raw_mc_mean.nii');
    end
    
    sel_coords = eval(['subj' num2str(sub) '_coords']) + 1;
    
    for ncond = 1:8
        disp(['Analyzing condition ' num2str(ncond)]);
        
        selcond = eval(['subj' num2str(sub) '_' labels{1,ncond}]);
        selcond_mean = mean(selcond);
        sel_voxel = size(selcond,2);
        
        tempvol = zeros(40,64,64);
        for ctri = 1:sel_voxel
            tempvol(sel_coords(ctri,1),sel_coords(ctri,2),sel_coords(ctri,3)) ...
                = selcond_mean(ctri);
        end
        
        hdrinfo.fname = ['subj' num2str(sub) '_actmaps_cond' num2str(ncond) '_VT_mc_ss.nii'];
        % spm_write_vol(hdrinfo,tempvol);
    end
    
end

% Activity maps can be checked using afni & suma