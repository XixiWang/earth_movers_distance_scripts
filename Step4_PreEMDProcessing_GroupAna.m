% Generate ground distance matrix and extract corresponding activity data
% under different conditions

% Step3: all five subjects are done

clear ;
% clc;

resolution = '64';
surf = 'intermediate_al';

for sub = 1
    % subj_dir = ['~/Documents/MATLAB/Data/haxby2001/subj' num2str(sub) '_SurfCoords'];
    subj_dir = ['~/Dropbox/LAB_Folder_Macbook/MATLAB/Data/haxby2001/subj' num2str(sub) '_SurfCoords'];
    cd(subj_dir);
    FileName = ['Subj' num2str(sub) '_GDMatrices.mat'];
    
    if exist(FileName,'file') == 2
        disp('Ground distance matrix already exists')
    else
        NodesInfoFile = ['subj' num2str(sub) '_Nodelist_complete_res' resolution '_sorted.mat'];
        load(NodesInfoFile);
        
        SingleNodeCoords = load(['subj' num2str(sub) '_bold_raw_mc_mean_res' resolution '_mh_ave.1D']);
        Voxs_idx = SingleNodeCoords(:,2);
        [~,I] = sort(Voxs_idx);
        SingleNodeCoords_sort = SingleNodeCoords(I,:);
        [tempa,tempb] = unique(SingleNodeCoords_sort(:,3:5),'rows','stable');
        tempc = SingleNodeCoords_sort(tempb,2);
        num_voxels = size(tempa,1);
        disp([num2str(num_voxels) ' voxels are selected']);
        % Generate single node information
        SingleNodeInfo = [(1:num_voxels)',tempc,tempa];
        SingleNodeInfo_HDR = {'Local index','Global voxel index','Voxel Coords'};
        
        % Load Euclidian distance (all nodes)
        EucDistFile = importdata(['subj' num2str(sub) '_NodesDist_com_Euc_' resolution '_' surf '_sorted.asc.1D']);
        EucDist = EucDistFile.data;
        
        % Load surface distance (all nodes)
        SurfDistFile = importdata(['subj' num2str(sub) '_NodesDist_com_surf_' resolution '_' surf '_sorted.asc.1D']);
        SurfDist = SurfDistFile.data;
        
        DistData = [Nodeslist_com EucDist(:,3) SurfDist(:,3)]; % Add surface dist....
        DistData_HDR = {'Node1Idx','VoxCoords','Node2Idx','VoxCoords','EucDist','SurfDist'};
        
        temp = DistData;
        % Extract unique voxel pairs
        % Each voxel can be projected into multiple nodes
        % First find intra voxel comparisions
        intra_vox_idx = bsxfun(@eq,DistData(:,2:4),DistData(:,6:8));
        intra_vox_idx = all(intra_vox_idx,2)';
        intra_vox_idx = find(intra_vox_idx == 1);
        disp([num2str(length(intra_vox_idx)) ' intra voxel comparisons found']);
        
        % Delete intra voxel comparisons
        DistData(intra_vox_idx,:) = [];
        
        % Delete nodes indices
        DistData(:,1) = [];
        DistData(:,4) = [];
        
        % Combine same voxel pairs
        % From voxel A
        [a1,b1,c1] = unique(DistData(:,1:3),'rows','stable');
        % To voxel B
        [a2,b2,c2] = unique(DistData(:,4:6),'rows','stable');
        % Correspond to single node information (SingleNodeInfo)
        c2 = c2 + 1;
        
        % Generate ground distance vectors
        Euc_Surf_Col = [c1 c2 DistData(:,7:8)];
        [a3,b3,c3] = unique(Euc_Surf_Col(:,1:2),'rows','stable');
        
        % Speed up calculation using parfor
        Euc_Surf_Col_ave2 = cell(size(a3,1),1);
        tic;
        parpool;
        parfor ctr = 1:size(a3,1)
            % Calculate average value along each column
            Euc_Surf_Col_ave2{ctr,1} = mean(Euc_Surf_Col(c3 == ctr,3:4),1);
        end
        delete(gcp);
        Euc_Surf_Col_ave2 = [a3 cell2mat(Euc_Surf_Col_ave2)];
        t1 = toc;
        
        %     % Calculate averaged ground distance vector
        %     tic;
        %     Euc_Surf_Col_ave = zeros(size(a3,1),4);
        %     for ctr = 1:size(a3,1)
        %         % Calculate average value along each column
        %         Euc_Surf_Col_ave(ctr,3:4) = mean(Euc_Surf_Col(c3 == ctr,3:4),1);
        %     end
        %     Euc_Surf_Col_ave(:,1:2) = a3;
        %     t2 = toc;
        
        % Construct ground distance matrix
        GD_Euc = zeros(num_voxels,num_voxels);
        GD_Surf = GD_Euc;
        for ctr = 1:size(a3,1)
            GD_Euc(Euc_Surf_Col_ave2(ctr,1),Euc_Surf_Col_ave2(ctr,2)) = Euc_Surf_Col_ave2(ctr,3);
            GD_Surf(Euc_Surf_Col_ave2(ctr,1),Euc_Surf_Col_ave2(ctr,2)) = Euc_Surf_Col_ave2(ctr,4);
        end
        
        unix('df -h | /usr/bin/mail -s "Ground distance matrix is done" xixi.wang577@gmail.com')
        % save(fullfile(subj_dir,FileName),'GD_Euc','GD_Surf','Euc_Surf_Col_ave2', ...
        %    'DistData','DistData_HDR','SingleNodeInfo','SingleNodeInfo_HDR');
    end
    
    
end
