#! bin/bash
echo "Bash script to visualize extraction results"

# export DBRoot=~/Documents/MATLAB/Data/haxby2001
export DBRoot=~/Dropbox/LAB_Folder_Macbook/MATLAB/Data/haxby2001
cd $DBRoot
ls -l

# for nsub in subj1 subj2 sub3 subj4 subj5
for nsub in subj1
do
    export SUBPATH=$DBRoot/$nsub
    export SURFACEPATH=${DBRoot}/${nsub}_fs_new_anat_all
    export SUMASURFACE=${DBRoot}/${nsub}_suma_surfaces_al_mean_bold_mc

    echo $SURFACEPATH
    echo $SUMASURFACE
    if [ ! -d $SURFACEPATH ];then
            echo "Cortical surface extraction needs to be done first!!"
        else
            ##################################################
            ############# GENERATE SUMA SURFACES #############
            ##################################################
            if [ ! -d $SUMASURFACE ];then
                    echo "Generate SUMA surfaces"
                    # Generate SUMA surfaces and align the anatomical surfaces to the functional data (mean BOLD file)
                    pymvpa2-prep-afni-surf -e $SUBPATH/bold_raw_mc_mean.nii.gz -d ${nsub}_fs_new_anat_all -r ${nsub}_suma_surfaces_al_mean_bold_mc
                    cd $SUMASURFACE
                    echo $PWD
                    export SurfVol_ss=${nsub}_fs_new_anat_all_SurfVol_ss_al2exp+tlrc
                    export SurfVol=${nsub}_fs_new_anat_all_SurfVol_al2exp+tlrc
                
                    # Data visualization (merged hemispheres, resolution = 128)
                    afni -niml & suma -spec mh_ico128_al.spec -sv ${SurfVol_ss}
                    # afni -niml & suma -spec bh_ico128_al.spec -sv ${SurfVol_ss}
                else
                    echo "Visualize prepared SUMA surfaces"
                    cd $SUMASURFACE
                    echo $PWD
                    export SurfVol_ss=${nsub}_fs_new_anat_all_SurfVol_ss_al2exp+tlrc
                    export SurfVol=${nsub}_fs_new_anat_all_SurfVol_al2exp+tlrc
                     
                    afni -niml & suma -spec mh_ico128_al.spec -sv ${SurfVol_ss}
                    # afni -niml & suma -spec bh_ico128_al.spec -sv ${SurfVol_ss}
            fi
    fi
 
done
