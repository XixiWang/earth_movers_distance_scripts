#! /bin/bash
echo "Bash script to generate surface distance for haxby dataset"

export PATH=${PATH}:~/Dropbox/LAB_Folder_Macbook/MATLAB/Code/EMD_Scripts

# Surface settings
export resolution=64
export hemi=m
export Outtype=1D
export mapfunc=ave

for nsub in subj1
do
    export SUBJECTS_DIR=~/Dropbox/LAB_Folder_Macbook/MATLAB/Data/haxby2001
    source ~/.bash_profile

    echo $SUBJECTS_DIR
    cd $SUBJECTS_DIR
    # ls -l
    
    export Data_DIR=$SUBJECTS_DIR/$nsub
    export Surf_DIR=$SUBJECTS_DIR/${nsub}_suma_surfaces_al_mean_bold_mc
    export Coords_DIR=$SUBJECTS_DIR/${nsub}_SurfCoords

    # Generate VT mask
    if [ ! -e "${Data_DIR}/mask4_vt+orig.BRIK" ]; then
        echo "Generate VT mask in AFNI format"
        3dcopy ${Data_DIR}/mask4_vt.nii.gz ${Data_DIR}/mask4_vt+orig
    else
        echo "VT mask already exists"
    fi

    # Map volumetric data within the VT mask to the surface
    export gridparent='bold_raw_mc_mean'
    export OutName=${nsub}_${gridparent}_res${resolution}_${hemi}h_${mapfunc}
    export SurfVol=${nsub}_fs_new_anat_all_SurfVol_ss_al2exp+tlrc    
    if [ ! -e "$Coords_DIR/${OutName}.1D" ]; then
        echo "Project volume to surface"
        cd $Data_DIR 
        3dVol2Surf  -spec $Surf_DIR/${hemi}h_ico${resolution}_al.spec \
                    -surf_A $Surf_DIR/ico${resolution}_${hemi}h.smoothwm_al.asc \
                    -surf_B $Surf_DIR/ico${resolution}_${hemi}h.pial_al.asc \
                    -sv $Surf_DIR/$SurfVol \
                    -grid_parent $Data_DIR/${gridparent}.nii \
                    -map_func ${mapfunc} \
                    -cmask '-a mask4_vt+orig[0] -expr ispositive(a-0)' \
                    -no_headers \
                    -out_1D $Coords_DIR/${OutName}.1D
    else
        echo "Projection already finished"
    fi

    # Extract nodes list from the output file
    # if [ ! -e "$Coords_DIR/${OutName}_Nodeslist.1D" ]; then
        # echo "Extract nodes list"
        # 1dcat -sel '[0]' $Coords_DIR/${OutName}.1D > $Coords_DIR/${OutName}_Nodeslist.1D
        # 1dcat -sel '[2,3,4]' $Coords_DIR/${OutName}_Coordslist.1D
    # fi

    # Extract unique nodes list using MATLAB function
    export NodesListName=${nsub}_Nodelist_complete_unique_res${resolution}.1D
    if [ ! -e "$Coords_DIR/$NodesListName" ]; then
        cd ~/Dropbox/LAB_Folder_Macbook/MATLAB/Code/EMD_Scripts
        matlab -nodesktop -nosplash -nodisplay -r "GenerateNodesList_uni('$Coords_DIR','$OutName','$resolution','$nsub'); quit"
    else
        echo "Unique nodes list already exist"
    fi 

    # Extract complete nodes list using MATLAB function
    # Here sort the nodes list according to the global index of each voxel
    export NodesListName_com=${nsub}_Nodelist_complete_res${resolution}_sorted.1D
    if [ ! -e "$Coords_DIR/$NodesListName_com" ]; then
        cd ~/Dropbox/LAB_Folder_Macbook/MATLAB/Code/EMD_Scripts
        matlab -nodesktop -nosplash -nodisplay -r "GenerateNodesList_complete_sort('$Coords_DIR','$OutName','$resolution','$nsub'); quit"
    else
        echo "Complete nodes list already exist"
    fi 

    # Euclidian distance calulation on intermediate surface
    export DistName_Euc=${nsub}_NodesDist_Euc_${resolution}_intermediate_al.asc.1D
    if [ ! -e "$Coords_DIR/$DistName_Euc" ]; then
        echo "Calculate Euclidian distance"
        SurfDist    -i_fs $Surf_DIR/ico${resolution}_${hemi}h.intermediate_al.asc \
                    -input $Coords_DIR/$NodesListName \
                    -Euclidian > $Coords_DIR/$DistName_Euc
    else
        echo "Euclidian distance (low resolution) already calculated"
    fi


    # Surface distance calculation on intermediate surface 
    export DistName_surf=${nsub}_NodesDist_surf_${resolution}_intermediate_al.asc.1D
    if [ ! -e "$Coords_DIR/$DistName_surf" ]; then
        echo "Calculate surface mesh based distance"
        SurfDist    -i_fs $Surf_DIR/ico${resolution}_${hemi}h.intermediate_al.asc \
                    -input $Coords_DIR/$NodesListName > $Coords_DIR/$DistName_surf
    else
        echo "Surface mesh based distance (low resolution) already calculated"
    fi

    # Distance calculation for all nodes
    export DistName_Euc_com=${nsub}_NodesDist_com_Euc_${resolution}_intermediate_al_sorted.asc.1D
    if [ ! -e "$Coords_DIR/$DistName_Euc_com" ]; then
        echo "Calculate Euclidian distance"
        SurfDist    -i_fs $Surf_DIR/ico${resolution}_${hemi}h.intermediate_al.asc \
                    -input $Coords_DIR/$NodesListName_com \
                    -Euclidian > $Coords_DIR/$DistName_Euc_com
    else
        echo "Euclidian distance (low resolution, all nodes) already calculated"
    fi

    # Distance calculation for all nodes
    export DistName_surf_com=${nsub}_NodesDist_com_surf_${resolution}_intermediate_al_sorted.asc.1D
    if [ ! -e "$Coords_DIR/$DistName_surf_com" ]; then
        echo "Calculate surface mesh based distance"
        SurfDist    -i_fs $Surf_DIR/ico${resolution}_${hemi}h.intermediate_al.asc \
                    -input $Coords_DIR/$NodesListName_com > $Coords_DIR/$DistName_surf_com
    else
        echo "Surface mesh based distance (low resolution, all nodes) already calculated"
    fi
    
    
    echo "${nsub} is done" 
    df -h | mail -s "$nsub is done" xixi.wang577@gmail.com

    # check the exist status
    echo $?
done

