% TestEMD calculation
% Questions: distance matrix is asymmetric?
%%
clear;

addpath('~/Dropbox/LAB_Folder_Macbook/MATLAB/Code/FastEMD_PeleWerman/');
addpath('~/Dropbox/LAB_Folder_Macbook/MATLAB/Utilities/');

labels = {'bottle','cat','chair','face','house','scissor','scrapix','shoe'};
% Calculation type: 'actmaps' or 'tmaps'
CalcType = 'actmaps';
disp(CalcType);
% Calculate fast EMD (histograms don't have to be normalized)
extra_mass_penalty = -1;
% For Rubner's EMD, extra_mass_penalty = 0

ncond = 8;
all_conds_pairs = nchoosek(1:ncond,2);

nbins = 100;
AllEMD = zeros(5,size(all_conds_pairs,1));
for sub = 1:5
    disp(['Analyzing subject' num2str(sub)]);
    subj_dir = ['~/Dropbox/LAB_Folder_Macbook/MATLAB/Data/haxby2001/subj' num2str(sub)];
    cd(subj_dir);
    
    BinFileName = ['Subj' num2str(sub) '_AllBinDist_' num2str(nbins)];
    load(fullfile(subj_dir,BinFileName),'All_binsdist');
    
    histFileName = ['subj' num2str(sub) '_histdata'];
    load(fullfile(subj_dir,histFileName),'All_histinfo');
    
    EMDMtrx = zeros(1,size(all_conds_pairs,1));
    for condpairs = 1:size(all_conds_pairs,1)
        GDvec = All_binsdist{1,condpairs};
        GDMtrx = zeros(nbins,nbins);

        a = 1;
        for ctri = 1:nbins
            for ctrj = 1:nbins
                GDMtrx(ctri,ctrj) = GDvec(a);
                a = a + 1;
            end
        end

        condAidx = all_conds_pairs(condpairs,1);
        condBidx = all_conds_pairs(condpairs,2);
        
        % Histograms have nbins = 100 bins
        hist1_val = eval(['All_histinfo.nbins' num2str(nbins) '.cond' ...
                          num2str(condAidx)  '.hcond_vals']);
        hist1_norm_pdf = eval(['All_histinfo.nbins' num2str(nbins) '.cond' ...
                          num2str(condAidx) '.hcond_norm_pdf_vals']);
        hist1_norm_prob = eval(['All_histinfo.nbins' num2str(nbins) '.cond' ...
                          num2str(condAidx) '.hcond_norm_prob_vals']);
        
        hist2_val = eval(['All_histinfo.nbins' num2str(nbins) '.cond' ...
                          num2str(condBidx)  '.hcond_vals']);
        hist2_norm_pdf = eval(['All_histinfo.nbins' num2str(nbins) '.cond' ...
                          num2str(condBidx) '.hcond_norm_pdf_vals']);
        hist2_norm_prob = eval(['All_histinfo.nbins' num2str(nbins) '.cond' ...
                          num2str(condBidx) '.hcond_norm_prob_vals']);
                      
        % EMD calculation
        EMDMtrx(1,condpairs) = emd_hat_gd_metric_mex(hist1_norm_prob',hist2_norm_prob',GDMtrx);              
    end
    AllEMD(sub,:) = EMDMtrx;  
end

%% RSA toolbox
rsapath = '~/Dropbox/LAB_Folder_Macbook/MATLAB/Toolboxes/rsatoolbox/';
addpath(genpath(rsapath));

Sub_dir = '~/Dropbox/LAB_Folder_Macbook/MATLAB/Data/haxby2001/';

% Configure settings
userOptions.rootPath = fullfile(Sub_dir,'RSAToolBox_test');
userOptions.analysisName = 'TEST123';
userOptions.saveFiguresPDF = 1;
userOptions.conditionLabels = labels;
% userOptions.colourScheme = gray;

% Configure variables
userOptions.distance = 'Correlation'; % Default similarity measurement
% Calculate the dissimilarity between RDMs - rank correlation
userOptions.distanceMeasure = 'Spearman';

%% 
EMD_ltv_vecs = zeros(1,size(AllEMD,2),size(AllEMD,1));
for ctr = 1:size(AllEMD,1)
    EMD_ltv_vecs(1,:,ctr) = AllEMD(ctr,:);
end
EMD_based_Rdm = squareRDMs(EMD_ltv_vecs);
AvgSubEMDRDM = mean(EMD_based_Rdm,3);
showRDMs(EMD_based_Rdm);

EMD_based_vec_rank = zeros(1,size(AllEMD,2),size(AllEMD,1));
% Output rank transformed matrices and calculate the averaged EMD-RDM
for ctr = 1:size(AllEMD,1)
    EMD_based_vec_rank(1,:,ctr) = scale01(rankTransform_equalsStayEqual(EMD_ltv_vecs(1,:,ctr)));
end
EMD_based_Rdm_rank = squareRDMs(EMD_based_vec_rank);
AvgSubEMDRDM_rank = mean(EMD_based_Rdm_rank,3);
showRDMs(EMD_based_vec_rank,501);

%% 
modelRDMs = cat(3,EMD_based_Rdm,AvgSubEMDRDM);
RDMs_Names = {'EMD1','EMD2','EMD3','EMD4','EMD5','AveEMD'};
modelRDMs = wrapAndNameRDMs(modelRDMs,RDMs_Names);

showRDMs(modelRDMs,502);
modelRDMs_cell = cell(1,numel(modelRDMs));
for idx = 1:numel(modelRDMs)
    modelRDMs_cell{idx} = modelRDMs(idx);
end

FigNum = 503;
for ctr = 1:6
    VarName = RDMs_Names{ctr};
    MDSConditions(modelRDMs_cell{ctr},userOptions,struct('titleString','EMDMDS','figureNumber',FigNum)); 
    %     'fileName',[VarName '_MDS'],'figureNumber',FigNum));
    FigNum = FigNum + 1;
end

%% Decoding
close all;
[DecodingResults,MeanSubCorr] = AcrossSubsDecoding(AllEMD);