#! /bin/bash

export PATH=${PATH}:~/Dropbox/LAB_Folder_Macbook/MATLAB/Code/EMD_Scripts/Test_top_ten_activated_voxels

# Surface settings
export resolution=64
export hemi=m
export Outtype=1D
export mapfunc=ave

for nsub in subj1 subj2 subj3 subj4 subj5
do
    export SUBJECTS_DIR=~/Dropbox/LAB_Folder_Macbook/MATLAB/Data/haxby2001
    source ~/.bash_profile

    export Data_DIR=$SUBJECTS_DIR/$nsub
    export Surf_DIR=$SUBJECTS_DIR/${nsub}_suma_surfaces_al_mean_bold_mc
    export Coords_DIR=$Data_DIR/actmaps_top10

    # Convert to 1D format 
    cd $Coords_DIR
    # mv ${nsub}_nodes_condAB.txt ${nsub}_nodes_condAB.1D
    export NodesListName=${nsub}_nodes_condAB.1D

    # Euclidian distance calulation on intermediate surface
    export DistName_Euc=${nsub}_top_ten_Dist_Euc_${resolution}_intermediate_al.asc.1D
    if [ ! -e "$Coords_DIR/$DistName_Euc" ]; then
        echo "Calculate Euclidian distance"
        SurfDist    -i_fs $Surf_DIR/ico${resolution}_${hemi}h.intermediate_al.asc \
                    -input $Coords_DIR/$NodesListName \
                    -Euclidian > $Coords_DIR/$DistName_Euc
    else
        echo "Euclidian distance (low resolution) already calculated"
    fi

    # Surface distance calculation on intermediate surface 
    export DistName_surf=${nsub}_top_ten_Dist_surf_${resolution}_intermediate_al.asc.1D
    if [ ! -e "$Coords_DIR/$DistName_surf" ]; then
        echo "Calculate surface mesh based distance"
        SurfDist    -i_fs $Surf_DIR/ico${resolution}_${hemi}h.intermediate_al.asc \
                    -input $Coords_DIR/$NodesListName > $Coords_DIR/$DistName_surf
    else
        echo "Surface mesh based distance (low resolution) already calculated"
    fi


done

