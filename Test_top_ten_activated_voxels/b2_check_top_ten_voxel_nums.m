clear;
clc;

addpath('~/Dropbox/LAB_Folder_Macbook/MATLAB/Code/FastEMD_PeleWerman/');
addpath('~/Dropbox/LAB_Folder_Macbook/MATLAB/Utilities/');
labels = {'bottle','cat','chair','face','house','scissors','scrambledpix','shoe'};

% %% Extract top ten activation values
% for sub = 1:5
%     disp(['Analyzing subject' num2str(sub)]);
%     subj_dir = ['~/Dropbox/LAB_Folder_Macbook/MATLAB/Data/haxby2001/subj' num2str(sub)];
%
%     addpath(subj_dir);
%     cd(fullfile(subj_dir,'actmaps_top10'));
%
%     vt_act_top_ten_data = cell(1,8);
%
%     for nlabel = 1:8
%         actfilename = ['subj' num2str(sub) '_vol2surf_actmaps_mh_res64_' labels{1,nlabel} '.1D'];
%         actfile = importdata(actfilename);
%
%         % Extract actmaps and delete zero values
%         temp_actdata = actfile.data;
%         temp_actdata(temp_actdata(:,7) == 0,:) = [];
%
%         % Extract unique values
%         [tempa,tempb,tempc] = unique(temp_actdata(:,3:5),'rows','stable');
%         uni_val = zeros(length(tempb),1);
%         for ctr = 1:length(tempb)
%             uni_val(ctr,1) = mean(temp_actdata(tempc == ctr,7),1);
%         end
%         % Extract unique coords
%         uni_coords = temp_actdata(tempb,1:5);
%         uni_data = [uni_coords uni_val];
%
%         % Sort selected values
%         [tempB,tempI] = sort(uni_data(:,5),'descend');
%         % Select top ten percent
%         num_voxels = size(tempa,1);
%         top_ten_num_voxels = round(num_voxels * 0.1);
%         top_ten_idx = tempI(1:top_ten_num_voxels);
%
%         top_ten_data = uni_data(top_ten_idx,:);
%
%         vt_act_top_ten_data{1,nlabel} = top_ten_data;
%         % fprintf('Subject %d, %s - top ten percent: %d voxels selected \n',...
%         %    sub, labels{1,nlabel}, top_ten_num_voxels);
%
%         FileName = ['subj' num2str(sub) '_vt_top10_actmaps.mat'];
%         % save(FileName,'vt_act_top_ten_data');
%     end
% end

%% Calculate surface based distance
clear;
clc;

condlabel = nchoosek(1:8,2);
for sub = 2:5
    disp(['Analyzing subject' num2str(sub)]);
    subj_dir = ['~/Dropbox/LAB_Folder_Macbook/MATLAB/Data/haxby2001/subj' num2str(sub)];
    
    addpath(subj_dir);
    cd(fullfile(subj_dir,'actmaps_top10'));
    
    FileName = ['subj' num2str(sub) '_vt_top10_actmaps.mat'];
    load(FileName);
    
    node1 = vt_act_top_ten_data{1,1};node1 = node1(:,1);
    node2 = vt_act_top_ten_data{1,2};node2 = node2(:,1);
    node3 = vt_act_top_ten_data{1,3};node3 = node3(:,1);
    node4 = vt_act_top_ten_data{1,4};node4 = node4(:,1);
    node5 = vt_act_top_ten_data{1,5};node5 = node5(:,1);
    node6 = vt_act_top_ten_data{1,6};node6 = node6(:,1);
    node7 = vt_act_top_ten_data{1,7};node7 = node7(:,1);
    node8 = vt_act_top_ten_data{1,8};node8 = node8(:,1);
    
    % Node1-Node8 all equal here
    [tempX,tempY] = meshgrid(node1(:),node2(:));
    tempX = tempX(:);
    tempY = tempY(:);
    nodelist_condAB = [tempX tempY];
    
    txtfileName = ['subj' num2str(sub) '_nodes_condAB.txt'];
    % dlmwrite(txtfileName,nodelist_condAB,'delimiter','\t');
    
    % In case Node1 != Node2 ....
    %     for ncond = 1:28
    %         nodelist1 = vt_act_top_ten_data{1,condlabel(ncond,1)};
    %         nodelist2 = vt_act_top_ten_data{1,condlabel(ncond,2)};
    %         nodelist1 = nodelist1(:,1);
    %         nodelist2 = nodelist2(:,1);
    %
    %         [tempX,tempY] = meshgrid(nodelist1(:),nodelist2(:));
    %         tempX = tempX(:);
    %         tempY = tempY(:);
    %         nodelist_condAB = [tempX tempY];
    %
    %         fileID = fopen(['subj' num2str(sub) '_nodes_cond' num2str(condlabel(ncond,1)) ...
    %             num2str(condlabel(ncond,2)) '.txt'],'wt');
    %         fprintf(fileID,'%7d %7d\n',nodelist_condAB);
    %     end
     
end