% Script to select the top 10% activited values and used them for emd
% calculation

%% Check previous scripts
% Step 1 - Preprocessing_Extraction
%
% Step 2 - Examine results
%
% Step 3 - Project VT onto cortical surfaces & calculate surface based
% distance
%
% Step 4 - Generate ground distance matrices: [subj_1_GDMatrices.mat]
%%
clear;
clc;

addpath('~/Dropbox/LAB_Folder_Macbook/MATLAB/Code/FastEMD_PeleWerman/');
addpath('~/Dropbox/LAB_Folder_Macbook/MATLAB/Utilities/');

for sub = 1
    disp(['Analyzing subject' num2str(sub)]);
    subj_dir = ['~/Dropbox/LAB_Folder_Macbook/MATLAB/Data/haxby2001/subj' num2str(sub)];
    subj_surfdir = ['~/Dropbox/LAB_Folder_Macbook/MATLAB/Data/haxby2001/subj' num2str(sub) '_SurfCoords'];
    
    addpath(subj_dir);
    addpath(subj_surfdir);
    cd(subj_dir);
    if ~exist('actmaps_top10','dir')
        mkdir('actmaps_top10');
    end
    
    disp('Load ground distance matrices');
    GDFile = ['Subj' num2str(sub) '_GDMatrices.mat'];
    load(GDFile);
    
    % Load activation maps
    disp('Load activity maps (generated based on individual VT masks)')
    ActFile = ['subj' num2str(sub) '_actmaps_VT_mc_ss.mat'];
    load(ActFile);
    Subj_data = double(eval(['subj' num2str(sub) '_data']))';
    
    % Generate activity map's vector
    Subj_coords = double(eval(['subj' num2str(sub) '_coords']));
    disp([num2str(size(Subj_coords,1)) ' voxels are selected from the VT mask (PyMVPA)'])
    disp([num2str(size(SingleNodeInfo,1)) ' voxels are used for cortical surface distance calculation (AFNI)'])
    
    imgsize = [40,64,64];
    Subj_coords = [Subj_coords zeros(size(Subj_coords,1),1)];
    for ctr = 1:size(Subj_coords,1)
        Subj_coords(ctr,4) = sub2ind(imgsize,Subj_coords(ctr,1), ...
            Subj_coords(ctr,2) + 1, ...
            Subj_coords(ctr,3) + 1);
    end
    Subj_coords = [Subj_coords(:,4) Subj_coords(:,1:3)];
    
    PyMVPA_coords_val = [Subj_coords Subj_data];
    % Find corresponding values based on coordinates
    Vals = zeros(size(SingleNodeInfo,1),8);
    for ctr = 1:size(SingleNodeInfo,1)
        Vals(ctr,:) = PyMVPA_coords_val(PyMVPA_coords_val(:,1) == SingleNodeInfo(ctr,2),5:12);
    end
    Afni_coors_val = [SingleNodeInfo Vals];
    Afni_coors_val_HDR = {'Local idx','index','Coords','Act maps'};
    
    % Data for all voxels
    top_ten_nums = round(size(Afni_coors_val,1) * 0.1);
    top_ten_data = cell(1,8);
    
    for ncond = 1:8
        ind_cond = Afni_coors_val(:,5+ncond);
        [tempsorted_cond,tempidx] = sort(ind_cond,'descend');
        tempidx_sorted = sort(tempidx(1:top_ten_nums),'ascend');
        ind_cond_top10 = Afni_coors_val(tempidx_sorted, [1:5 5+ncond]);
        
        top_ten_data{1,ncond} = ind_cond_top10;
        
        clear ind_cond ind_cond_top10
    end
    
    FileName = ['subj' num2str(sub) '_afni_top10_actmaps.mat'];
    % save(fullfile('actmaps_top10',FileName),'top_ten_data');
end
disp('---------------------------------------------');

%% Test 1: calculate the emd between selected voxels
clear;
clc;
ncondlist = nchoosek(1:8,2);

for sub = 1:5
    disp(['Analyzing subject' num2str(sub)]);
    subj_dir = ['~/Dropbox/LAB_Folder_Macbook/MATLAB/Data/haxby2001/subj' num2str(sub)];
    subj_surfdir = ['~/Dropbox/LAB_Folder_Macbook/MATLAB/Data/haxby2001/subj' num2str(sub) '_SurfCoords'];
    
    addpath(subj_dir);
    addpath(subj_surfdir);
    cd(subj_dir);
    
    FileName = ['subj' num2str(sub) '_afni_top10_actmaps.mat'];
    load(fullfile('actmaps_top10',FileName));
    
    GDMatrxName = ['Subj' num2str(sub) '_GDMatrices.mat'];
    load(fullfile(subj_surfdir,GDMatrxName),'SingleNodeInfo',...
        'SingleNodeInfo_HDR','Euc_Surf_Col_ave2','GD_Euc','GD_Surf');
    
    top_ten_GD = cell(3,28);
   
    nselvox = size(top_ten_data{1,1},1);
    fprintf('Top 10 percent activated: %d voxels \n',nselvox)
    
    for ncond = 1:size(ncondlist,1)
        % Loop through all condition pairs
        fprintf('Compare cond %d and cond %d \n',ncondlist(ncond,1),ncondlist(ncond,2))
        condA_all = top_ten_data{1,ncondlist(ncond,1)};
        condB_all = top_ten_data{1,ncondlist(ncond,2)};
        condA_idx = condA_all(:,1);
        condB_idx = condB_all(:,1);
        
        condAB_idx = zeros(nselvox^2,2);
        a = 1;
        for ctrA = 1:nselvox
            for ctrB = 1:nselvox
                condAB_idx(a,1) = condA_idx(ctrA);
                condAB_idx(a,2) = condB_idx(ctrB);
                a = a + 1;
            end
        end
        
        % Extract ground distance values
        tempGD_Euc_vec = zeros(nselvox^2,1);
        tempGD_Surf_vec = zeros(nselvox^2,1);
        for ctrz = 1:nselvox^2
            if condAB_idx(ctrz,1) == condAB_idx(ctrz,2)
                tempGD_Euc_vec(ctrz) = 0;
                tempGD_Surf_vec(ctrz) = 0;
            elseif condAB_idx(ctrz,1) < condAB_idx(ctrz,2)
                tempGD_Euc_vec(ctrz) = GD_Euc(condAB_idx(ctrz,1),condAB_idx(ctrz,2));
                tempGD_Surf_vec(ctrz) = GD_Surf(condAB_idx(ctrz,1),condAB_idx(ctrz,2));
            elseif condAB_idx(ctrz,1) > condAB_idx(ctrz,2)
                tempGD_Euc_vec(ctrz) = GD_Euc(condAB_idx(ctrz,2),condAB_idx(ctrz,1));
                tempGD_Surf_vec(ctrz) = GD_Surf(condAB_idx(ctrz,2),condAB_idx(ctrz,1));
            end
        end
        condAB_idx(:,3) = tempGD_Euc_vec;
        condAB_idx(:,4) = tempGD_Surf_vec;
        
        top_ten_GD{1,ncond} = condAB_idx;
        top_ten_GD{2,ncond} = (reshape(tempGD_Euc_vec,nselvox,nselvox))';
        top_ten_GD{3,ncond} = (reshape(tempGD_Surf_vec,nselvox,nselvox))';
    end
    
    FileName = ['subj' num2str(sub) '_afni_top10_GD.mat'];
    save(fullfile('actmaps_top10',FileName),'top_ten_GD');
end

%% Test 2: project each condition's nifti file onto surfaces and then check the number of voxels/nodes