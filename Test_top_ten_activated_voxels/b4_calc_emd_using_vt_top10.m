% Calculate emd using vt_top10 voxels
clear 
clc;

addpath('~/Dropbox/LAB_Folder_Macbook/MATLAB/Code/FastEMD_PeleWerman/');
addpath('~/Dropbox/LAB_Folder_Macbook/MATLAB/Utilities/');
rsapath = '~/Dropbox/LAB_Folder_Macbook/MATLAB/Toolboxes/rsatoolbox/';
addpath(genpath(rsapath));

%% emd calculation
ncondlist = nchoosek(1:8,2);
top_ten_emd_all_subs = zeros(5,28);
top_ten_emd_all_subs_rank = zeros(5,28);
top_ten_emd_all_subs_partial = zeros(5,28);
top_ten_emd_all_subs_partial_rank = zeros(5,28);

% Select 'surf' or 'Euc'
gd_criterion = 'Euc';

for sub = 1:5
    disp(['Analyzing subject' num2str(sub)]);
    subj_dir = ['~/Dropbox/LAB_Folder_Macbook/MATLAB/Data/haxby2001/subj' num2str(sub)];

    addpath(subj_dir);
    cd(fullfile(subj_dir,'actmaps_top10'));
    
    actfile = ['subj' num2str(sub) '_vt_top10_actmaps.mat'];
    load(actfile);
    
    GDfile = ['subj' num2str(sub) '_top_ten_Dist_' gd_criterion '_64_intermediate_al.asc.1D'];
    GDfile = importdata(GDfile);
    
    top_ten_emd = zeros(1,28);
    top_ten_emd_partial = zeros(1,28);

    for ncond = 1:28
        condA_all = vt_act_top_ten_data{1,ncondlist(ncond,1)};
        condB_all = vt_act_top_ten_data{1,ncondlist(ncond,2)};
        
        condA_val = condA_all(:,6);
        condB_val = condB_all(:,6);
        if strcmp(gd_criterion,'Euc')
            GDAB_vec = GDfile.data(:,3);
            GDAB = reshape(GDAB_vec,length(condA_val),length(condA_val))';
        elseif strcmp(gd_criterion,'surf')
            GDAB_vec = GDfile.data(:,3);
            % Remove -1 values: assign max values to -1 
            % Question: assign 0 to certain values - No information
            % transfer?
            GDAB_vec(GDAB_vec == -1) = max(GDAB_vec(:)) + 5;
            GDAB = reshape(GDAB_vec,length(condA_val),length(condA_val))';
        end
        
        top_ten_emd(1,ncond) = emd_hat_gd_metric_mex(condA_val,condB_val,GDAB,-1);
        top_ten_emd_partial(1,ncond) = emd_hat_gd_metric_mex(condA_val,condB_val,GDAB,0);

    end
    
    % Rank transform the emd matrix
    top_ten_emd_rank = rankTransform_equalsStayEqual(top_ten_emd);
    top_ten_emd_partial_rank = rankTransform_equalsStayEqual(top_ten_emd_partial);

    top_ten_emd_all_subs(sub,:) = top_ten_emd;
    top_ten_emd_all_subs_rank(sub,:) = top_ten_emd_rank;
    top_ten_emd_all_subs_partial(sub,:) = top_ten_emd_partial;
    top_ten_emd_all_subs_partial_rank(sub,:) = top_ten_emd_partial_rank;

    FileName = ['subj' num2str(sub) '_emd_top10_' gd_criterion '.mat'];
    % save(FileName,'top_ten_emd','top_ten_emd_rank');
end

