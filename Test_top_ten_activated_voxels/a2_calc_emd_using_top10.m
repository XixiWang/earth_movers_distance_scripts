% Calculate emd using top 10% activated voxels
clear;
clc;

addpath('~/Dropbox/LAB_Folder_Macbook/MATLAB/Code/FastEMD_PeleWerman/');
addpath('~/Dropbox/LAB_Folder_Macbook/MATLAB/Utilities/');
rsapath = '~/Dropbox/LAB_Folder_Macbook/MATLAB/Toolboxes/rsatoolbox/';
addpath(genpath(rsapath));

%% emd calculation
ncondlist = nchoosek(1:8,2);
top_ten_emd_all_subs = zeros(5,28);
top_ten_emd_all_subs_rank = zeros(5,28);
top_ten_emd_all_subs_partial = zeros(5,28);
top_ten_emd_all_subs_partial_rank = zeros(5,28);

% Select 'surf' or 'euc'
gd_criterion = 'surf';

for sub = 1:5
    disp(['Analyzing subject' num2str(sub)]);
    subj_dir = ['~/Dropbox/LAB_Folder_Macbook/MATLAB/Data/haxby2001/subj' num2str(sub)];

    addpath(subj_dir);
    cd(fullfile(subj_dir,'actmaps_top10'));
    
    actfile = ['subj' num2str(sub) '_afni_top10_actmaps.mat'];
    GDfile = ['subj' num2str(sub) '_afni_top10_GD.mat'];
    load(actfile);
    load(GDfile);
    
    top_ten_emd = zeros(1,28);
    top_ten_emd_partial = zeros(1,28);

    for ncond = 1:28
        condA_all = top_ten_data{1,ncondlist(ncond,1)};
        condB_all = top_ten_data{1,ncondlist(ncond,2)};
        
        condA_val = condA_all(:,6);
        condB_val = condB_all(:,6);
        if strcmp(gd_criterion,'euc')
            GDAB = top_ten_GD{2,ncond};
        elseif strcmp(gd_criterion,'surf')
            GDAB = top_ten_GD{3,ncond};
            % Remove -1 values
            GDAB(GDAB == -1) = max(GDAB(:)) + 5;
        end
        
        top_ten_emd(1,ncond) = emd_hat_gd_metric_mex(condA_val,condB_val,GDAB,-1);
        top_ten_emd_partial(1,ncond) = emd_hat_gd_metric_mex(condA_val,condB_val,GDAB,0);

    end
    
    % Rank transform the emd matrix
    top_ten_emd_rank = rankTransform_equalsStayEqual(top_ten_emd);
    top_ten_emd_partial_rank = rankTransform_equalsStayEqual(top_ten_emd_partial);

    top_ten_emd_all_subs(sub,:) = top_ten_emd;
    top_ten_emd_all_subs_rank(sub,:) = top_ten_emd_rank;
    top_ten_emd_all_subs_partial(sub,:) = top_ten_emd_partial;
    top_ten_emd_all_subs_partial_rank(sub,:) = top_ten_emd_partial_rank;

    FileName = ['subj' num2str(sub) '_emd_top10_' gd_criterion '.mat'];
    % save(FileName,'top_ten_emd','top_ten_emd_rank');
end

%% Decoding using emd
labels = {'bottle','cat','chair','face','house','scissors','scrambledpix','shoe'};
figure;imagesc(squareform(top_ten_emd_all_subs_partial_rank(1,:)));colorbar;
set(gca,'XTickLabel',labels)
set(gca,'YTickLabel',labels)

%% Project onto mds
close all;
opts = statset('MaxIter',100000,'Display','off');

for nsub = 1:5
    figure(nsub);
    [EMD_MDS_cri_coords,~,disparities] = mdscale(top_ten_emd_all_subs_partial_rank(nsub,:),2,'criterion','stress','Options',opts);
    plot(EMD_MDS_cri_coords(:,1),EMD_MDS_cri_coords(:,2),'.','MarkerSize',2);
    text(EMD_MDS_cri_coords(:,1),EMD_MDS_cri_coords(:,2),labels,'Color','r', ...
        'FontSize',12,'FontWeight','bold');
end

%% Conclusion
% MDS didn't work when using Euc based ground distance
% 
% For surface based gd, the top_ten_emd_all_subs_partial_rank & stress
% worked best