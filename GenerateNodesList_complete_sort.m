function GenerateNodesList_complete_sort(var1, var2,var3,var4)
setenv('Coords_DIR',var1);
setenv('FileName', var2);
setenv('resolution',var3);
setenv('nsub',var4);

!echo $Coords_DIR
!echo $FileName
!echo $resolution
!echo $nsub

disp('Running MATLAB function!');

Coords_DIR = getenv('Coords_DIR');
FileName = getenv('FileName');
resolution = getenv('resolution');
nsub = getenv('nsub');

load(fullfile(Coords_DIR,strcat(FileName,'.1D')));
cd(Coords_DIR)
% save(fullfile(Coords_DIR,'NodesInfo.mat'))

NodesInfo = eval(FileName);

% Sort all the information according to Voxel's index
Voxs_idx = NodesInfo(:,2);
[~,I] = sort(Voxs_idx);
Nodes_Info_sort = NodesInfo(I,:);

% Number of node pairs
num_Nodes = size(Nodes_Info_sort,1);
disp([num2str(num_Nodes) ' nodes are selected'])

% Generate node pairs list
a = 1;
Nodeslist_com = zeros(num_Nodes * (num_Nodes - 1) / 2, 8);
for ctri = 1:num_Nodes
    for ctrj = 1:num_Nodes
        if ctri < ctrj
            Nodeslist_com(a,1) = Nodes_Info_sort(ctri,1);
            Nodeslist_com(a,2:4) = Nodes_Info_sort(ctri,3:5);
            Nodeslist_com(a,5) = Nodes_Info_sort(ctrj,1);
            Nodeslist_com(a,6:8) = Nodes_Info_sort(ctrj,3:5);
            a = a + 1;
        end
    end
end

% codegen ~/Desktop/Scripts/NodesList_for -o NodesList_for_mex -args {zeros(num_Nodes,7)}
% Nodeslist_com = NodesList_for_mex(NodesInfo);

% Write ascii file
FileName = [nsub '_Nodelist_complete_res' resolution '_sorted'];
save([FileName '.mat'],'Nodeslist_com');

Nodeslist_idx = [Nodeslist_com(:,1) Nodeslist_com(:,5)];
dlmwrite([FileName '.1D'], Nodeslist_idx, ' ');

end