__author__ = 'xixiwang'

from mvpa2.suite import *
from mvpa2.misc.io.base import SampleAttributes
from mvpa2.datasets.mri import fmri_dataset
import scipy
import scipy.stats
import os
import numpy as np

# Change the location of the root directory, for your local machine:
pymvpa_datadbroot = '/Users/XixiWang/Dropbox/LAB_Folder_Macbook/MATLAB/Data/haxby2001'

# Exclude subject 6
num_subjs = 1
num_categories = 8
num_squareform_vals = num_categories * (num_categories-1) / 2
all_subjs_squareform_sims = scipy.zeros((num_subjs,num_squareform_vals),float)

for subj_num in range(num_subjs):
    subj_string = 'subj' + str(subj_num + 1)
    # set path for each subject
    subjpath = os.path.join(pymvpa_datadbroot, subj_string)
    # Read PyMVPA sample attribute definitions from text files
    attrs = SampleAttributes(os.path.join(subjpath, 'labels.txt'),header=True)

    all_chunk = np.unique(attrs.chunks)
    all_labels = np.unique(attrs.labels)

    print " ----------------- About to import the dataset for " + subj_string
    # Create a dataset from an fMRI timeseries image (samples,targets,chunks,mask)
    my_dataset = fmri_dataset(samples=os.path.join(subjpath,'bold_raw_mc.nii'), targets=attrs.labels,chunks=attrs.chunks,mask=os.path.join(subjpath, 'mask4_vt.nii.gz'))

    # Extract coordinates of VOIs
    print " ----------------- Extract coordinates of VOI"
    Coords = my_dataset.fa.voxel_indices

    # Perform detrending
    detrender = PolyDetrendMapper(polyord=1, chunks_attr='chunks')
    detrended_dataset = my_dataset.get_mapped(detrender)

    # Data normalization: scales all voxels into the same range and removes the mean
    zscore(detrended_dataset,param_est=('targets', ['rest']))

    print " ----------------- Export data without rest state"
    detrended_dataset_without_rest_periods = detrended_dataset[detrended_dataset.sa.targets != 'rest']
    detrended_dataset_without_rest_periods_values = detrended_dataset_without_rest_periods.samples

    print " ----------------- Split odd and even runs"
    rnames = {0: 'even', 1: 'odd'}
    detrended_dataset_without_rest_periods.sa['runtype'] = [rnames[c % 2] for c in detrended_dataset_without_rest_periods.sa.chunks]

    print " ----------------- Calculate mean values"
    averager = mean_group_sample(['targets', 'runtype'])
    detrended_dataset_without_rest_periods_mean = detrended_dataset_without_rest_periods.get_mapped(averager)
    Coords2 = detrended_dataset_without_rest_periods_mean.fa.voxel_indices
    print detrended_dataset_without_rest_periods.sa.chunks # Even runs -- then odd runs
    # print detrended_dataset_without_rest_periods.sa.targets

    print " ----------------- Project back to NIFTI"
    fds = detrended_dataset_without_rest_periods_mean
    revtest = np.arange(100, 100 + fds.nfeatures)
    rmapped = fds.a.mapper.reverse1(revtest)
    nimg = map2nifti(fds, revtest)
    nimg.to_filename('mytest.nii.gz')

    print " ----------------- File I/O"
    mat_dict = {'data_oddevensplit': detrended_dataset_without_rest_periods_mean.samples, 'coords': Coords}
    FileName = os.path.join(subjpath,subj_string + '_OddEvenSplit.mat')
    scipy.io.savemat(FileName, mat_dict)