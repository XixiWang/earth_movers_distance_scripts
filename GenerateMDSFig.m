function MDSfig = GenerateMDSFig(DissimVec)
% Function to generate MDS plot
%
% DissimVec: NORMALIZED input dissimilarity vector,
%

addpath('~/Dropbox/LAB_Folder_Macbook/MATLAB/Utilities/export_fig/');

DisCorrMtrx = squareform(DissimVec);
YCorr = mdscale(DisCorrMtrx,2);
YCorr2 = mdscale(DisCorrMtrx,2,'criterion','sammon','Options',opts);
YCorr3 = mdscale(DisCorrMtrx,2,'criterion','metricsstress','Options',opts);
distancecorr1 = pdist(YCorr);
distancecorr2 = pdist(YCorr2);
distancecorr3 = pdist(YCorr3);

%
scrsz = get(0,'ScreenSize');
% figure('Position',[1 scrsz(4) scrsz(3)*0.8 scrsz(4)*0.8])
figure('Position',[1 scrsz(4) scrsz(3)/2 scrsz(4)/2])

subplot(2,3,2);
plot(DissimVec,distance1,'bo',...
    DissimVec,distance2,'r+',...
    DissimVec,distance3,'go',...
    [0 max(DissimVec)],[0 max(distance1)],'k--');
xlabel('Dissimilarities');ylabel('Distances');
legend({'Stress', 'Sammon Mapping', 'Squared Stress','1:1 Line'}, 'Location','NorthWest');
title('Shepard plot');

labels = {'bottle' 'cat' 'chair' 'face' 'house' 'scissors' 'scrambledpix' 'shoe'};
subplot(2,3,3);
plot(Y1(:,1),Y1(:,2),'.','MarkerSize',2);
text(Y1(:,1),Y1(:,2),labels,'Color','r');
title('Neural similarity space based on EMD in 2D MDS');

subplot(2,3,4);
Corr_Allmtrx = squareform(Corr_All(ctr,:)); v = ones(1,8); temp = diag(v);
Corr_Allmtrx = Corr_Allmtrx + temp;
imagesc(Corr_Allmtrx);colormap(hot);colorbar;caxis([0 1]);title('Correlation matrix');

subplot(2,3,5)
plot(DisCorrvec,distancecorr1,'bo',...
    DisCorrvec,distancecorr2,'r+',...
    DisCorrvec,distancecorr3,'go',...
    [0 max(DisCorrvec)],[0 max(distancecorr1)],'k--');
xlabel('Dissimilarities (1 - correlation)');ylabel('Distances');
legend({'Stress', 'Sammon Mapping', 'Squared Stress','1:1 Line'}, 'Location','NorthWest');
title('Shepard plot');

subplot(2,3,6);
plot(YCorr(:,1),YCorr(:,2),'.','MarkerSize',2);
text(YCorr(:,1),YCorr(:,2),labels,'Color','r')
title('Neural similarity space based on correlation in 2D MDS')

%     eval(['export_fig subject' num2str(ctr) '_FS.png -m2.5'])
close all
end