from mvpa2.suite import *
from mvpa2.misc.io.base import SampleAttributes
from mvpa2.datasets.mri import fmri_dataset
from mvpa2.misc.surfing import queryengine
import os
import scipy

datadbroot = '/axelUsers/xwang91/Documents/MATLAB/Data/haxby2001'

sub_tot = 1
cond_tot = 1

for nsub in range(sub_tot):
    sub_name = 'subj' + str(nsub + 1)
    print "Analyzing subject " + str(nsub + 1)

    func_path = os.path.join(datadbroot, sub_name)
    vtmask_ds = fmri_dataset(os.path.join(func_path, 'mask4_vt.nii.gz'))

    surf_path = os.path.join(datadbroot, sub_name + '_suma_surfaces_al_mean_bold_mc')
    actmaps_path = os.path.join(datadbroot, sub_name + '_suma_surfaces_al_mean_bold_mc')
    # print "Surface path: " + str(surf_path)
    # print "Activation maps path: " + str(actmaps_path)

    xyz_all_cond = {}

    for ncond in range(cond_tot):
        cond_name = 'cond' + str(ncond + 1)
        actmaps_name = sub_name + '_actmaps_' + cond_name + '_VT_mc_ss.nii'
        actmaps_ds = fmri_dataset(os.path.join(actmaps_path, actmaps_name))
        # print actmaps_ds.a.imghdr

        # instantiate volgeom instance from fmri dataset
        vg = volgeom.from_any(actmaps_ds)
        # Convert voxel indices in dataset to 3D coordinates
        xyz = vg.ijk2xyz(actmaps_ds.fa.voxel_indices)
        # print xyz
        xyz_all_cond[cond_name] = xyz

        ########################################################################
        # Volume selection based on searchlight analysis
        # Here I wanted to see whether I can set the searchlight radius as 1 and
        # extract corresponding voxel from the functional data
        # (Reference module: mvpa2.misc.surfing)
        ########################################################################

        # Searchlight radius
        radius = 1
        # Define hemisphere
        # Merged hemisphere
        hemi = 'm'
        # Surface resolution
        res = 64

        white_surf = os.path.join(surf_path, "ico%d_%sh.smoothwm_al.asc" % (res, hemi))
        pial_surf = os.path.join(surf_path, "ico%d_%sh.pial_al.asc" % (res, hemi))

        # print white_surf
        # print pial_surf
        print "Hemisphere: " + hemi + "; resolution: " + str(res)
        # Generate voxel searching que
        # Refer to help(disc_surface_queryengine) if necessary
        ########################################################################
        ############ Searchlight analysis test #################################
        ########################################################################
        # qe1 = disc_surface_queryengine(radius, actmaps_ds, white_surf, pial_surf, vtmask_ds)
        # print qe1

        # Here try using the test BOLD data for each condition
        func_test_path = os.path.join('/axelUsers/xwang91/Documents/Software_pyMVPA/tutorial_data_3/data/sub001/BOLD/task001_run001',
                                      'bold.nii.gz')
        print func_test_path
        func_test_ds = fmri_dataset(func_test_path)
        qe2 = disc_surface_queryengine(radius, func_test_ds, white_surf, pial_surf, volume_mask = vtmask_ds)
        print "Output of searchlight analysis wrappers"
        print qe2
        # print qe2.VolumeMaskDictionary



    # print xyz_all_cond

    # File I/O
    FileName = sub_name + 'xyz_all_cond' + '_VT_mc_ss' + '.mat'
    # scipy.io.savemat(os.path.join(datadbroot, FileName),{'xyz_all_cond':xyz_all_cond})


