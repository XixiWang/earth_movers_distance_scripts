#! /bin/bash

# To project functional data under different conditions onto cortical surface

export DBRoot=~/Dropbox/LAB_Folder_Macbook/MATLAB/Data/haxby2001
cd $DBRoot

# labels: bottle cat chair face house scissors scrambledpix shoe

export sub_tot=2
export cond_tot=3

# Subject 1
# for nsub in subj1
# do 
#     echo "Analyzing $nsub"
#     export SUBPATH=$DBRoot/$nsub
#     export SURFACEPATH=${DBRoot}/${nsub}_fs_new_anat_all
#     export SUMASURFACE=${DBRoot}/${nsub}_suma_surfaces_al_mean_bold_mc
#     export $SUMASURFACE
#     cd $SUMASURFACE
    
#     export SurfVol_ss=${nsub}_fs_new_anat_all_SurfVol_ss_al2exp+tlrc
#     export SurfVol=${nsub}_fs_new_anat_all_SurfVol_al2exp+tlrc
#     export Specfile=bh_ico128_al.spec

#     # Display surfaces
#     # What we are interested in are: face, house, scissors
    
#     suma -niml -npb 11 -spec $Specfile -sv $SurfVol_ss & 
#     afni -niml -npb 11 & 
#     # Second port
#     suma -niml -npb 12 -spec $Specfile -sv $SurfVol_ss & 
#     afni -niml -npb 12 & 
#     # Third port 
#     suma -niml -npb 13 -spec $Specfile -sv $SurfVol_ss & 
#     afni -niml -npb 13 &     
# done


# Subject 3
for nsub in subj3
do 
    echo "Analyzing $nsub"
    export SUBPATH=$DBRoot/$nsub
    export SURFACEPATH=${DBRoot}/${nsub}_fs_new_anat_all
    export SUMASURFACE=${DBRoot}/${nsub}_suma_surfaces_al_mean_bold_mc
    export $SUMASURFACE
    cd $SUMASURFACE
    
    export SurfVol_ss=${nsub}_fs_new_anat_all_SurfVol_ss_al2exp+tlrc
    export SurfVol=${nsub}_fs_new_anat_all_SurfVol_al2exp+tlrc
    export Specfile=bh_ico128_al.spec

    # Display surfaces
    # What we are interested in are: face, house, scissors
    
    suma -niml -npb 31 -spec $Specfile -sv $SurfVol_ss & 
    afni -niml -npb 31 -com 'SET_PBAR_ALL +99 1.0 Color_Pos' -com 'SET_THRESHOLD 0' & 
    # Second port
    suma -niml -npb 32 -spec $Specfile -sv $SurfVol_ss & 
    afni -niml -npb 32 & 
    # Third port 
    suma -niml -npb 33 -spec $Specfile -sv $SurfVol_ss & 
    afni -niml -npb 33 &  
done


