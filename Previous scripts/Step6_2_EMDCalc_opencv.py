import cv
import scipy
import scipy.io
import os
import numpy as np

subj = 1
cond = 8
ctr = cond * (cond - 1) /2

dbroot = '/axelUsers/xwang91/Documents/MATLAB/Data/haxby2001'

for nsub in range(subj):
    sub_str = 'subj' + str(nsub + 1)
    sub_dir = os.path.join(dbroot, sub_str)

    print "Analyzing subject" + str(nsub + 1)
    
    FileName = 'EMD_pre_' + sub_str + '_mc_ss_VT.mat'
    # Load data for EMD calculation
    mat_dict = scipy.io.loadmat(os.path.join(sub_dir,FileName))
    # Check variable names
    var = scipy.io.whosmat(os.path.join(sub_dir,FileName))
    print var
   
    print "Load ground distance matrices"
    GD_Euc_com = np.array(mat_dict['GD_Euc_com'])
    GD_Surf_com = np.array(mat_dict['GD_Surf_com'])
    GD_Surf_com_thr = np.array(mat_dict['GD_Surf_com_thr'])
    voxel_num = GD_Euc_com.shape[1]
    print str(voxel_num) + " voxels are slected"

    Afni_coors_val = np.array(mat_dict['Afni_coors_val'])
    # Extract different activity maps
    for ctri in range(1):
        for ctrj in range(2):
            if ctri < ctrj:
                condA = Afni_coors_val[:, 5 + ctri]
                condB = Afni_coors_val[:, 5 + ctrj]

                # Calculate signatures
                sigA = cv.fromarray(condA, allowND=True)
                sigB = cv.fromarray(condB, allowND=True)
                
                sigA1 = cv.CreateMat(voxel_num, 1, cv.CV_32FC1)
                sigB1 = cv.CreateMat(voxel_num, 1, cv.CV_32FC1)
                sigA1 = np.array(sigA1)
                sigB1 = np.array(sigB1)
                sigA1[:,0] = condA
                sigB1[:,0] = condB
                
                # Modify ground distance matrix
                print GD_Euc_com.flags.contiguous
                GD_Euc_com_cont = GD_Euc_com.copy()
                print GD_Euc_com_cont.flags.contiguous
                GD_Euc_com_cont = cv.fromarray(GD_Euc_com_cont)

                # GD_Euc_com = cv.fromarray(GD_Euc_com)
                # EMD_Euc = cv.CalcEMD2(sigA, sigB, cv.CV_DIST_USER, cost_matrix=GD_Euc_com)
                # print EMD_Euc

                EMD_Euc2 = cv.CalcEMD2(cv.fromarray(sigA1), cv.fromarray(sigB1), distance_type=cv.CV_DIST_USER,
                        cost_matrix=GD_Euc_com_cont)

