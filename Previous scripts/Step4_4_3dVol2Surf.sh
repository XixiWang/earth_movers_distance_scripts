#! bin/bash

export SUBJECTS_DIR=~/Desktop/Testdata
source ~/.bash_profile

export SUMA_Al_Surfaces=${SUBJECTS_DIR}/subj1_suma_surfaces_al_mean_bold_mc
cd ${SUMA_Al_Surfaces}

export SurfVol=subj1_fs_new_anat_all_SurfVol_ss_al2exp+tlrc
export resolution=128
export hemi=m

export OutName=Subj1_bold_mc_res${resolution}.${hemi}h.niml.dset
if [ ! -f $OutName ]; then
    3dVol2Surf  -spec ${hemi}h_ico${resolution}_al.spec \
                -surf_A ico${resolution}_${hemi}h.smoothwm_al.asc \
                -surf_B ico${resolution}_${hemi}h.pial_al.asc \
                -sv ${SurfVol} \
                -grid_parent subj1_actmaps_cond1_VT_mc_ss.nii \
                -map_func seg_vals \
                -out_1D $OutName

fi


