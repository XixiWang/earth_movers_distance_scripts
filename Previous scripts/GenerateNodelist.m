function GenerateNodelist
Coords_DIR = input('Input Coords_DIR:', 's');
FileName = input('Input FileName:', 's');

load(fullfile(Coords_DIR,strcat(FileName,'.1D')));
% save(fullfile(Coords_DIR,'NodesInfo.mat'),FileName)

NodesInfo = eval(FileName);
Nodes_idx = NodesInfo(:,1);
Voxel_coords = NodesInfo(:,3:5);

% Select unique voxel coords
[~,uni_idx,~] = unique(Voxel_coords,'rows','stable');
Nodes_idx_uni = Nodes_idx(uni_idx);

% Number of node pairs
num_Nodes_uni = length(Nodes_idx_uni)

% Generate node pairs list
a = 1;
for ctri = 1:num_Nodes_uni
    for ctrj = 1:num_Nodes_uni
        if ctri < ctrj
            Nodeslist(a,1) = Nodes_idx_uni(ctri);
            Nodeslist(a,2) = Nodes_idx_uni(ctrj);
            a = a + 1;
        end
    end
end

% Write ascii file
dlmwrite('nodelist_complete.1D', Nodeslist, ' ');
end