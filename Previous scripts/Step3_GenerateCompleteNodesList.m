% Generate node list
% Each voxel can be projected to multiple nodes
% Right now I'm selecting the very first nodes for each voxel on the
% white matter surface (smoothwm)
clear all;
close all;

load('Subj1_bold_raw_mc_mean_res128_mh_ave.mat');
Subj1_Nodes_info = [Subj1_Surf_info(:,1),Subj1_Surf_info(:,3:5)];
Subj1_Nodes_idx = Subj1_Surf_info(:,1);
Subj1_Voxel_coords = Subj1_Surf_info(:,3:5);
% Select unique voxel coordinates
[Subj1_Voxel_coords_uni,uni_idx,~] = unique(Subj1_Voxel_coords,'rows','stable');
Subj1_Nodes_idx_uni = Subj1_Nodes_idx(uni_idx);

num_Nodes_uni = length(Subj1_Nodes_idx_uni)

% Generate complete nodes list
a = 1;
for ctri = 1:num_Nodes_uni
    for ctrj = 1:num_Nodes_uni
        if ctri < ctrj
            Nodeslist(a,1) = Subj1_Nodes_idx_uni(ctri);
            Nodeslist(a,2) = Subj1_Nodes_idx_uni(ctrj);
            a = a + 1;
        end
    end
end

save('~/Desktop/Testdata/SurfCoords/Subj1_bold_raw_mc_mean_res128_nodelist.mat')
% Write ascii file
dlmwrite('nodelist_complete.1D',Nodeslist,' ')
