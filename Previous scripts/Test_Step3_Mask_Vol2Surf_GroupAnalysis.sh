#! /bin/bash
echo "Bash script to generate surface distance for haxby dataset"

# Surface settings
export resolution=128
export lowres=64
export hemi=m
export Outtype=1D
export mapfunc=ave

for nsub in subj2 
do
    export SUBJECTS_DIR=~/Documents/MATLAB/Data/haxby2001
    source ~/.bash_profile

    echo $SUBJECTS_DIR
    cd $SUBJECTS_DIR
    # ls -l
    
    export Data_DIR=$SUBJECTS_DIR/$nsub
    export Surf_DIR=$SUBJECTS_DIR/${nsub}_suma_surfaces_al_mean_bold_mc
    export Coords_DIR=$SUBJECTS_DIR/${nsub}_SurfCoords

    # Generate VT mask
    if [ ! -e "${Data_DIR}/mask4_vt+orig.BRIK" ]; then
        echo "Generate VT mask in AFNI format"
        3dcopy ${Data_DIR}/mask4_vt.nii.gz ${Data_DIR}/mask4_vt+orig
    else
        echo "VT mask already exists"
    fi

    # Map volumetric data within the VT mask to the surface
    export gridparent='bold_raw_mc_mean'
    export OutName=${nsub}_${gridparent}_res${resolution}_${hemi}h_${mapfunc}.1D
    export SurfVol=${nsub}_fs_new_anat_all_SurfVol_ss_al2exp+tlrc    
    if [ ! -e "$Coords_DIR/$OutName" ]; then
        echo "Project volume to surface"
        cd $Data_DIR 
        3dVol2Surf  -spec $Surf_DIR/${hemi}h_ico${resolution}_al.spec \
                    -surf_A $Surf_DIR/ico${resolution}_${hemi}h.smoothwm_al.asc \
                    -surf_B $Surf_DIR/ico${resolution}_${hemi}h.pial_al.asc \
                    -sv $Surf_DIR/$SurfVol \
                    -grid_parent $Data_DIR/${gridparent}.nii.gz \
                    -map_func ${mapfunc} \
                    -cmask '-a mask4_vt+orig[0] -expr ispositive(a-0)' \
                    -no_headers \
                    -out_1D $Coords_DIR/$OutName 
    else
        echo "Projection already finished"
    fi

    # Find unique nodes pairs using MATLAB
    # Call MATLAB function to generate unique nodes list
    # if [ ! -e "$Coords_DIR/nodelist_complete.1D" ]; then
    #    matlab -nosplash -nodisplay -nojvm -r "try, run /axelUsers/xwang91/Desktop/Scripts/GenerateNodelist; end; quit"        
    # fi

    # Load asc file to generate the ground distance

    # Distance calculation on white matter surface
    export DistName_surf=$nsub_NodesDist_surf_${resolution}_smoothwm_al.asc.1D
    if [ ! -e "$Coords_DIR/$DistName_surf" ]; then
        echo "Calculate surface mesh based distance"
        SurfDist    -i_fs $Surf_DIR/ico${resolution}_${hemi}h.smoothwm_al.asc \
                    -input $Coords_DIR/nodelist_complete.1D > $Coords_DIR/$DistName_surf
    else
        echo "Surface mesh based distance already calculated"
    fi

    export DistName_Euc=$nsub_NodesDist_Euc_${resolution}_smoothwm_al.asc.1D
    if [ ! -e "$Coords_DIR/$DistName_Euc" ]; then
        echo "Calculate Euclidian distance"
        SurfDist    -i_fs $Surf_DIR/ico${resolution}_${hemi}h.smoothwm_al.asc \
                    -input $Coords_DIR/nodelist_complete.1D \
                    -Euclidian > $Coords_DIR/$DistName_Euc
    else
        echo "Euclidian distance already calculated"
    fi

done
