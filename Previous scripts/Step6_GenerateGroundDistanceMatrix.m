% Perform EMD analysis based on surface distance
% Calculate original Reubner EMD
% Generate new Surface distance/Euclidian distance matrices
clear;

addpath('~/Dropbox/LAB_Folder_Macbook/MATLAB/Code/FastEMD_PeleWerman/');
addpath('~/Dropbox/LAB_Folder_Macbook/MATLAB/Utilities/');

% Calculation type: 'actmaps' or 'tmaps'
CalcType = 'actmaps';
disp(CalcType);

% Calculate fast EMD (histograms don't have to be normalized)
extra_mass_penalty = -1;
% For Rubner's EMD, extra_mass_penalty = 0

for sub = 1:5
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Generate ground distance matrices
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    disp(['Analyzing subject' num2str(sub)]);
    subj_dir = ['~/Dropbox/LAB_Folder_Macbook/MATLAB/Data/haxby2001/subj' num2str(sub)];
    subj_surfdir = ['~/Dropbox/LAB_Folder_Macbook/MATLAB/Data/haxby2001/subj' num2str(sub) '_SurfCoords'];
    
    addpath(subj_dir);
    addpath(subj_surfdir);
    cd(subj_dir);
    
    disp('Load ground distance matrices');
    GDFile = ['Subj' num2str(sub) '_GDMatrices.mat'];
    load(GDFile);
    
    if strcmp(CalcType,'actmaps')
        disp('Load activity maps (generated based on individual VT masks)')
        ActFile = ['subj' num2str(sub) '_actmaps_VT_mc_ss.mat'];
        load(ActFile);
        Subj_data = double(eval(['subj' num2str(sub) '_data']))';
    elseif strcmp(CalcType,'tmaps')
        disp('Load t maps (generated based on individual VT masks)')
        tmapsFile = ['subj' num2str(sub) '_tmaps_VT_mc_ss.mat'];
        load(tmapsFile);
        Subj_data = double(eval(['subj' num2str(sub) '_tmaps']))';
    end
    
    % Generate activity map's vector
    Subj_coords = double(eval(['subj' num2str(sub) '_coords']));
    disp([num2str(size(Subj_coords,1)) ' voxels are selected from the VT mask (PyMVPA)'])
    disp([num2str(size(SingleNodeInfo,1)) ' voxels are used for cortical surface distance calculation (AFNI)'])
    
    % Subscripts to indices
    imgsize = [40,64,64];
    Subj_coords = [Subj_coords zeros(size(Subj_coords,1),1)];
    for ctr = 1:size(Subj_coords,1)
        Subj_coords(ctr,4) = sub2ind(imgsize,Subj_coords(ctr,1), ...
            Subj_coords(ctr,2) + 1, ...
            Subj_coords(ctr,3) + 1);
    end
    Subj_coords = [Subj_coords(:,4) Subj_coords(:,1:3)];
    
    % Sort according to each voxel's index and see whether it's same to
    % what afni generated
    % [~,IX] = sort(Subj_coords(:,1));
    % Subj_coords_sorted = Subj_coords(IX,:);
    PyMVPA_coords_val = [Subj_coords Subj_data];
    
    % Find corresponding values based on coordinates
    Vals = zeros(size(SingleNodeInfo,1),8);
    for ctr = 1:size(SingleNodeInfo,1)
        Vals(ctr,:) = PyMVPA_coords_val(PyMVPA_coords_val(:,1) == SingleNodeInfo(ctr,2),5:12);
    end
    Afni_coors_val = [SingleNodeInfo Vals];
    
    if strcmp(CalcType,'actmaps')
        Afni_coors_val_HDR = {'Local idx','index','Coords','Act maps'};
    elseif strcmp(CalcType,'tmaps')
        Afni_coors_val_HDR = {'Local idx','index','Coords','tmaps'};
    end
    
    % Generate symmetric matrices
    GD_Euc_com = GD_Euc + GD_Euc';
    GD_Surf_com = GD_Surf + GD_Surf';
    
    % Test 1: assign a fixed value to -1 and then threshold the ground
    % distance matrix
    temp = GD_Surf_com(:);
    temp(temp == -1) = [];
    tempmax = max(temp);
    GD_Surf_com(GD_Surf_com == -1) = tempmax * 10;
    GD_Surf_com_thr_max = min(GD_Surf_com,tempmax);
    
    % Test 2: assign different values based on Euclidian distance matrix
    % It has been noticed that two clusters could be generated after
    % replacing -1 with different values
    % ---------------------------------------------------------
    factor = 2;
    % ---------------------------------------------------------
    temp1 = GD_Surf;
    idx = find(temp1 == -1);
    updated_val = GD_Euc(idx) * factor;
    temp1(idx) = updated_val;
    GD_Surf_com_newval = temp1 + temp1';
    eval(['GD_Surf_com_fac' num2str(factor) ' = GD_Surf_com_newval;'])
    
    % Test 3: assign a fixed value to -a and then threshold the ground
    % distance matrix (here the threshold value is set much lower)
    %
    temp2 = GD_Surf(:);
    temp2(temp2 == -1) = [];
    temp2(temp2 == 0) = [];
    % figure;hist(temp2,100);
    % ---------------------------------------------------------
    per = 50;
    % ---------------------------------------------------------
    thr = quantile(temp2,per/100);
    disp(thr);
    temp2_mtrx = GD_Surf_com;
    temp2_mtrx(temp2_mtrx == -1) = thr * 2;
    temp2_mtrx_thr = min(temp2_mtrx,thr);
    eval(['GD_Surf_com_thr_per' num2str(per) ' = temp2_mtrx_thr;'])
    
    % Save ground truth distance matrix and corresponding data
    if strcmp(CalcType,'actmaps')
        EMD_PrefileName = ['EMD_pre_subj' num2str(sub) '_mc_ss_VT.mat'];
%         save(fullfile(subj_dir,EMD_PrefileName),'Afni_coors_val','Afni_coors_val_HDR',...
%             'GD_Euc_com','GD_Surf_com','GD_Surf_com_thr_max', ...
%             ['GD_Surf_com_fac' num2str(factor)], ['GD_Surf_com_thr_per' num2str(per)]);
    elseif strcmp(CalcType,'tmaps')
        EMD_PrefileName = ['EMD_pre_subj' num2str(sub) '_mc_ss_VT_tmaps.mat'];
%         save(fullfile(subj_dir,EMD_PrefileName),'Afni_coors_val','Afni_coors_val_HDR',...
%             'GD_Euc_com','GD_Surf_com','GD_Surf_com_thr_max', ...
%             ['GD_Surf_com_fac' num2str(factor)], ['GD_Surf_com_thr_per' num2str(per)]);
        
    end
    
    disp(['Ground distance matrices for subject ' num2str(sub) ' is done']);
    
end
