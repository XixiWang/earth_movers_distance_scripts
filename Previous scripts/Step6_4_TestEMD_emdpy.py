# Use python package emdpy to calculate EMD 
# Right now the emd_dist is not equal to what Oele's MATLAB wrapper's results

import emd
import os 
import numpy as np
import scipy.io

data_dir = ('/axelUsers/xwang91/Dropbox/LAB_Folder_Macbook/MATLAB/Code/EMD_Scripts')
File_name = 'TestCondAB.mat'

# Load data file
data = scipy.io.loadmat(os.path.join(data_dir,File_name))
# data contains condA & condB 
condA = data['condA']
condB = data['condB']
GD_mtrx = data['GD_Euc_com']

pyemd_dist = emd.emd(condA, condB, X_weights=None, Y_weights=None, distance='precomputed',D=GD_mtrx)
print pyemd_dist
