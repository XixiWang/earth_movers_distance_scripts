from mvpa2.datasets.mri import fmri_dataset
from mvpa2.misc.surfing import queryengine
import scipy
import os

datadbroot = '/axelUsers/xwang91/Documents/MATLAB/Data/haxby2001'

sub_tot = 1
cond_tot = 1

for nsub in range(sub_tot):
    sub_name = 'subj' + str(nsub + 1)
    print "Analyzing subject " + str(nsub + 1)

    func_path = os.path.join(datadbroot, sub_name)
    actmaps_path = os.path.join(datadbroot, sub_name + '_suma_surfaces_al_mean_bold_mc')
    func_test_path = '/axelUsers/xwang91/Documents/Software_pyMVPA/tutorial_data_3/data/sub001/BOLD/task001_run001'
    surf_path = os.path.join(datadbroot, sub_name + '_suma_surfaces_al_mean_bold_mc')

    vtmask_ds = fmri_dataset(os.path.join(func_path, 'mask4_vt.nii.gz'))
    actmaps_ds = fmri_dataset(os.path.join(actmaps_path, sub_name + '_actmaps_cond1_VT_mc_ss.nii'))
    func_test_ds = fmri_dataset(os.path.join(func_test_path, 'bold.nii.gz'))

    # Define searchlight parameters
    # Searchlight radius
    radius = 50
    # Define hemisphere
    # Merged hemisphere
    hemi = 'm'
    # Surface resolution
    res = 64
    # Surface files
    white_surf = os.path.join(surf_path, "ico%d_%sh.smoothwm_al.asc" % (res, hemi))
    pial_surf = os.path.join(surf_path, "ico%d_%sh.pial_al.asc" % (res, hemi))
    source_surf = os.path.join(surf_path, "ico%d_%sh.intermediate_al.asc" % (res, hemi))

    print "Hemisphere: " + hemi + "; resolution: " + str(res)

    # Voxel selection wrapper for multiple center nodes on the surface
    # Right now all voxels have been selected
    qe = queryengine.disc_surface_queryengine(radius, func_test_ds, white_surf,
             pial_surf, source_surf)
    print qe.voxsel

    # Surface vertices query


