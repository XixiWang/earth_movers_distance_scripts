% Perform EMD analysis based on surface distance
% Calculate original Reubner EMD
% Generate new Surface distance/Euclidian distance matrices
clear;

addpath('~/Dropbox/LAB_Folder_Macbook/MATLAB/Code/FastEMD_PeleWerman/');
addpath('~/Dropbox/LAB_Folder_Macbook/MATLAB/Utilities/');

% Calculation type: 'actmaps' or 'tmaps'
CalcType = 'actmaps';
disp(CalcType);

% Calculate fast EMD (histograms don't have to be normalized)
extra_mass_penalty = -1;
% For Rubner's EMD, extra_mass_penalty = 0

for sub = 1:5
    disp(['Analyzing subject' num2str(sub)]);
    subj_dir = ['~/Dropbox/LAB_Folder_Macbook/MATLAB/Data/haxby2001/subj' num2str(sub)];
    cd(subj_dir);
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % EMD calculation
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    if strcmp(CalcType,'actmaps')
        EMD_PrefileName = ['EMD_pre_subj' num2str(sub) '_mc_ss_VT.mat'];
    elseif strcmp(CalcType,'tmaps')
        EMD_PrefileName = ['EMD_pre_subj' num2str(sub) '_mc_ss_VT_tmaps.mat'];
    end
    load(EMD_PrefileName);
    
    ncond = 8;
    % Matrices initialization
    EMD_Euc_com = zeros(ncond * (ncond - 1)/2,1);
    EMD_Euc_com_Rub = EMD_Euc_com;
    EMD_Surf_com_thr_max = EMD_Euc_com;
    EMD_Surf_com_thr_max_Rub = EMD_Euc_com;
    EMD_Surf_com_fac2 = EMD_Euc_com;
    EMD_Surf_com_fac2_Rub = EMD_Euc_com;
    EMD_Surf_com_thr_per50 = EMD_Euc_com;
    EMD_Surf_com_thr_per50_Rub = EMD_Euc_com;
    CorrMtrx = EMD_Euc_com;
    
    
    % Matrices based on scaled activity maps / tmaps
    EMD_Euc_com_scaled = zeros(ncond * (ncond - 1)/2,1);
    EMD_Euc_com_Rub_scaled = EMD_Euc_com;
    EMD_Surf_com_thr_max_scaled = EMD_Euc_com;
    EMD_Surf_com_thr_max_Rub_scaled = EMD_Euc_com;
    EMD_Surf_com_fac2_scaled = EMD_Euc_com;
    EMD_Surf_com_fac2_Rub_scaled = EMD_Euc_com;
    EMD_Surf_com_thr_per50_scaled = EMD_Euc_com;
    EMD_Surf_com_thr_per50_Rub_scaled = EMD_Euc_com;
    CorrMtrx_scaled = EMD_Euc_com;
    
    a = 1;
    for ctri = 1:ncond
        for ctrj = 1:ncond
            if ctri < ctrj
                condA = Afni_coors_val(:,5 + ctri);
                condB = Afni_coors_val(:,5 + ctrj);
                % Convert original activity maps range to [0,255]
                condA_scaled = 255 * mat2gray(condA);
                condB_scaled = 255 * mat2gray(condB);
                
                % 1. Correlation
                % 4. Scaled activity maps
                CorrMtrx(a,1) = corr(condA,condB);
                CorrMtrx_scaled(a,1) = corr(condA_scaled,condB_scaled);
                
                % 2. Original Activity maps - Euclidian distance
                EMD_Euc_com(a,1) = emd_hat_gd_metric_mex(condA,condB,GD_Euc_com,-1);
                EMD_Euc_com_Rub(a,1) = emd_hat_gd_metric_mex(condA,condB,GD_Euc_com,0);
                
                EMD_Euc_com_scaled(a,1) = emd_hat_gd_metric_mex(condA_scaled,condB_scaled,GD_Euc_com,-1);
                EMD_Euc_com_Rub_scaled(a,1) = emd_hat_gd_metric_mex(condA_scaled,condB_scaled,GD_Euc_com,0);
                
                % 3. Original Activity maps - surface distance
                % Calculate the EMD distance based on surface * 10 --
                % thresholded matirx
                EMD_Surf_com_thr_max(a,1) = emd_hat_gd_metric_mex(condA,condB,GD_Surf_com_thr_max,-1);
                EMD_Surf_com_thr_max_Rub(a,1) = emd_hat_gd_metric_mex(condA,condB,GD_Surf_com_thr_max,0);
                
                EMD_Surf_com_thr_max_scaled(a,1) = emd_hat_gd_metric_mex(condA_scaled,condB_scaled,GD_Surf_com_thr_max,-1);
                EMD_Surf_com_thr_max_Rub_scaled(a,1) = emd_hat_gd_metric_mex(condA_scaled,condB_scaled,GD_Surf_com_thr_max,0);
                
                % Calculate EMD based on GD_Surf_fac* matrix
                EMD_Surf_com_fac2(a,1) = emd_hat_gd_metric_mex(condA,condB,GD_Surf_com_fac2,-1);
                EMD_Surf_com_fac2_Rub(a,1) = emd_hat_gd_metric_mex(condA,condB,GD_Surf_com_fac2,0);
                
                EMD_Surf_com_fac2_scaled(a,1) = emd_hat_gd_metric_mex(condA_scaled,condB_scaled,GD_Surf_com_fac2,-1);
                EMD_Surf_com_fac2_Rub_scaled(a,1) = emd_hat_gd_metric_mex(condA_scaled,condB_scaled,GD_Surf_com_fac2,0);
                
                % Calculate EMD based on GD_Surf_thr_per* matrix
                EMD_Surf_com_thr_per50(a,1) = emd_hat_gd_metric_mex(condA,condB,GD_Surf_com_thr_per50,-1);
                EMD_Surf_com_thr_per50_Rub(a,1) = emd_hat_gd_metric_mex(condA,condB,GD_Surf_com_thr_per50,0);
                
                EMD_Surf_com_thr_per50_scaled(a,1) = emd_hat_gd_metric_mex(condA_scaled,condB_scaled,GD_Surf_com_thr_per50,-1);
                EMD_Surf_com_thr_per50_Rub_scaled(a,1) = emd_hat_gd_metric_mex(condA_scaled,condB_scaled,GD_Surf_com_thr_per50,0);
                      
                a = a + 1;
            end
        end
    end
    
    
    if strcmp(CalcType,'actmaps')
        FileName = ['EMD_subj' num2str(sub) '_mc_ss_VT.mat'];
        EucFile = who('EMD_Euc*');
        SurfFile = who('EMD_Surf*');     
        save(fullfile(subj_dir,FileName),'CorrMtrx','CorrMtrx_scaled');
        for ctr = 1:length(EucFile)
            save(fullfile(subj_dir,FileName),EucFile{ctr},'-append');
        end
        for ctr = 1:length(SurfFile)
            save(fullfile(subj_dir,FileName),SurfFile{ctr},'-append');
        end               
    elseif strcmp(CalcType,'tmaps')
        FileName = ['EMD_subj' num2str(sub) '_mc_ss_VT_tmaps.mat'];        
        EucFile = who('EMD_Euc*');
        SurfFile = who('EMD_Surf*');     
        save(fullfile(subj_dir,FileName),'CorrMtrx');
        for ctr = 1:length(EucFile)
            save(fullfile(subj_dir,FileName),EucFile{ctr},'-append');
        end
        for ctr = 1:length(SurfFile)
            save(fullfile(subj_dir,FileName),SurfFile{ctr},'-append');
        end
    end
    
    disp(['EMD calculation for subject ' num2str(sub) ' is done']);
    
end
