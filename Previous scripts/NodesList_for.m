function Nodeslist_com = NodesList_for(NodesInfo) %#codegen
assert(isa(NodesInfo,'double'));

num_Nodes = size(NodesInfo,1);

a = 1;
Nodeslist_com = zeros(num_Nodes * (num_Nodes - 1) / 2, 2);

for ctri = 1:num_Nodes
    for ctrj = 1:num_Nodes
        if ctri < ctrj
            Nodeslist_com(a,1) = NodesInfo(ctri,1);
            Nodeslist_com(a,2) = NodesInfo(ctrj,1);
            a = a + 1;
        end
    end
end
end