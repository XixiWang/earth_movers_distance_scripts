#! bin/bash

export SUBJECTS_DIR=~/Desktop/Testdata
source ~/.bash_profile
cd $SUBJECTS_DIR
# ls -al
export Surf_DIR=$SUBJECTS_DIR/subj1_suma_surfaces_al_mean_bold_mc
export Coords_DIR=$SUBJECTS_DIR/SurfCoords

# Convert to AFNI format
if [ ! -f 'mask4_vt+orig.BRIK' ]; then
    3dcopy mask4_vt.nii mask4_vt+orig
fi

# Generate mask
# if [ ! -f 'mask4_vt_3dcalc.BRIK' ]; then
#    3dcalc  -a 'mask4_vt+orig[0]'       \
#            -expr 'ispositive(a-0)'     \
#            -prefix mask4_vt_3dcalc
# fi

# Resolution
export resolution=128
# Hemisphere: l(eft), r(ight), m(erged)
export hemi=m
# Output file type: niml, 1D
export Outtype=1D
# echo "Output file type is ${Outtype}"
export SurfVol=$Surf_DIR/subj1_fs_new_anat_all_SurfVol_ss_al2exp+tlrc
echo $SurfVol

# Map data within the VT mask
export mapfunc=max
export gridparent='bold_raw_mc_mean'
# export gridparent='subj1_actmaps_cond3_VT_mc_ss'
export OutName=$Coords_DIR/Subj1_${gridparent}.res${resolution}_${hemi}h.${mapfunc}.1D
if [ ! -f $OutName ]; then
    3dVol2Surf  -spec $Surf_DIR/${hemi}h_ico${resolution}_al.spec \
                -surf_A $Surf_DIR/ico${resolution}_${hemi}h.smoothwm_al.asc \
                -surf_B $Surf_DIR/ico${resolution}_${hemi}h.pial_al.asc \
                -sv ${SurfVol} \
                -grid_parent $SUBJECTS_DIR/${gridparent}.nii \
                -map_func ${mapfunc} \
                -cmask '-a mask4_vt+orig[0] -expr ispositive(a-0)' \
                -out_1D $OutName 
fi

# Calculate node distance based on the surface coordinates
echo "Calculate surface distance between each two nodes"

# Using spec file for merged head
# export OutName=$Coords_DIR/NodesDist.spec.1D
# if [ ! -f $OutName ]; then
#    SurfDist -spec $Surf_DIR/bh_ico128_al.spec \
#             -input $Coords_DIR/nodelist.1D > $OutName
# fi
# cat $OutName
#          
# Using surface volume
# export OutName2=$Coords_DIR/NodesDist.surfvol.1D
# if [ ! -f $OutName2 ]; then
#    SurfDist -sv ${SurfVol}. \
#             -input $Coords_DIR/nodelist.1D > $OutName2
# fi
# cat $OutName2

# Using asc file
export OutName3=$Coords_DIR/NodesDist.asc.1D
if [ ! -f $OutName3 ]; then
    SurfDist -i_fs $Surf_DIR/ico${resolution}_${hemi}h.smoothwm_al.asc \
             -input $Coords_DIR/nodelist.1D > $OutName3
fi
cat $OutName3

# Use complete nodes informatio
# Calculate surface mesh based distance
export OutName=$Coords_DIR/NodesDist_complete.asc.1D
if [ ! -f $OutName ]; then
    SurfDist -i_fs $Surf_DIR/ico${resolution}_${hemi}h.smoothwm_al.asc \
             -input $Coords_DIR/nodelist_complete.1D > $OutName
fi

# Calculate Euclidian distance
export OutName=$Coords_DIR/NodesDist_complete_Euc.asc.1D
if [ ! -f $OutName ]; then
    SurfDist -i_fs $Surf_DIR/ico${resolution}_${hemi}h.smoothwm_al.asc \
             -input $Coords_DIR/nodelist_complete.1D \
             -Euclidian > $OutName
fi

