% Perform EMD analysis based on surface distance
clear;
% clc;

addpath('~/Documents/MATLAB/Code/FastEMD_PeleWerman/');
% EMD parameters
const_fac = 1;
extra_mass_penalty = -1;
FType = 2;

% Calculation type: 'actmaps' or 'tmaps'
CalcType = 'tmaps';
disp(CalcType);

for sub = 1:5
    disp(['Analyzing subject' num2str(sub)]);
    subj_dir = ['~/Documents/MATLAB/Data/haxby2001/subj' num2str(sub)];
    subj_surfdir = ['~/Documents/MATLAB/Data/haxby2001/subj' num2str(sub) '_SurfCoords'];
    
    addpath(subj_dir);
    addpath(subj_surfdir);
    cd(subj_dir);
    
    disp('Load ground distance matrices');
    GDFile = ['Subj' num2str(sub) '_GDMatrices.mat'];
    load(GDFile);
    
    if strcmp(CalcType,'actmaps')
        disp('Load activity maps (generated based on individual VT masks)')
        ActFile = ['subj' num2str(sub) '_actmaps_VT_mc_ss.mat'];
        load(ActFile);
        Subj_data = double(eval(['subj' num2str(sub) '_data']))';
    elseif strcmp(CalcType,'tmaps')
        disp('Load t maps (generated based on individual VT masks)')
        tmapsFile = ['subj' num2str(sub) '_tmaps_VT_mc_ss.mat'];
        load(tmapsFile);
        Subj_data = double(eval(['subj' num2str(sub) '_tmaps']))';
    end
    
    % Generate activity map's vector
    Subj_coords = double(eval(['subj' num2str(sub) '_coords']));
    disp([num2str(size(Subj_coords,1)) ' voxels are selected from the VT mask (PyMVPA)'])
    disp([num2str(size(SingleNodeInfo,1)) ' voxels are used for cortical surface distance calculation (AFNI)'])
    
    % Subscripts to indices
    imgsize = [40,64,64];
    Subj_coords = [Subj_coords zeros(size(Subj_coords,1),1)];
    for ctr = 1:size(Subj_coords,1)
        Subj_coords(ctr,4) = sub2ind(imgsize,Subj_coords(ctr,1), ...
            Subj_coords(ctr,2) + 1, ...
            Subj_coords(ctr,3) + 1);
    end
    Subj_coords = [Subj_coords(:,4) Subj_coords(:,1:3)];
    
    % Sort according to each voxel's index and see whether it's same to
    % what afni generated
    % [~,IX] = sort(Subj_coords(:,1));
    % Subj_coords_sorted = Subj_coords(IX,:);
    PyMVPA_coords_val = [Subj_coords Subj_data];
    
    % Find corresponding values based on coordinates
    Vals = zeros(size(SingleNodeInfo,1),8);
    for ctr = 1:size(SingleNodeInfo,1)
        Vals(ctr,:) = PyMVPA_coords_val(PyMVPA_coords_val(:,1) == SingleNodeInfo(ctr,2),5:12);
    end
    Afni_coors_val = [SingleNodeInfo Vals];
    
    if strcmp(CalcType,'actmaps')
        Afni_coors_val_HDR = {'Local idx','index','Coords','Act maps'};
    elseif strcmp(CalcType,'tmaps')
        Afni_coors_val_HDR = {'Local idx','index','Coords','tmaps'};
    end
    
    % Generate symmetric matrices
    GD_Euc_com = GD_Euc + GD_Euc';
    GD_Surf_com = GD_Surf + GD_Surf';
    % figure;imagesc(GD_Euc);
    % figure;imagesc(GD_Surf);
    
    % For surface based distance, -1 values exist
    % How to deal with these values?
    % Threshold the GD matrix
    temp = GD_Surf_com(:);
    temp(temp == -1) = [];
    tempmax = max(temp);
    GD_Surf_com(GD_Surf_com == -1) = tempmax * 10;
    GD_Surf_com_thr = min(GD_Surf_com,tempmax);

    ncond = 8;
    EMD_Euc2 = zeros(ncond * (ncond - 1)/2,1);
    EMD_Surf = zeros(ncond * (ncond - 1)/2,1);
    CorrMtrx = zeros(ncond * (ncond - 1)/2,1);
    
    % Save ground truth distance matrix and corresponding data
    if strcmp(CalcType,'actmaps')
        EMD_PrefileName = ['EMD_pre_subj' num2str(sub) '_mc_ss_VT.mat'];
        if exist(fullfile(subj_dir,EMD_PrefileName),'file') ~= 2
            save(fullfile(subj_dir,EMD_PrefileName),'Afni_coors_val','Afni_coors_val_HDR',...
                'GD_Euc_com','GD_Surf_com','GD_Surf_com_thr');
        end
    elseif strcmp(CalcType,'tmaps')
        EMD_PrefileName = ['EMD_pre_subj' num2str(sub) '_mc_ss_VT_tmaps.mat'];
        if exist(fullfile(subj_dir,EMD_PrefileName),'file') ~= 2
            save(fullfile(subj_dir,EMD_PrefileName),'Afni_coors_val','Afni_coors_val_HDR',...
                'GD_Euc_com','GD_Surf_com','GD_Surf_com_thr');
        end
    end
    
    % EMD calculation
    a = 1;
    for ctri = 1:ncond
        for ctrj = 1:ncond
            if ctri < ctrj
                condA = Afni_coors_val(:,5 + ctri);
                condB = Afni_coors_val(:,5 + ctrj);
                dist_Euc2 = emd_hat_gd_metric_mex(condA,condB,GD_Euc_com,extra_mass_penalty);
                dist_Surf = emd_hat_gd_metric_mex(condA,condB,GD_Surf_com_thr,extra_mass_penalty);
                EMD_Euc2(a,1) = dist_Euc2;
                EMD_Surf(a,1) = dist_Surf;
                CorrMtrx(a,1) = corr(condA,condB);
                a = a + 1;
            end
        end
    end
    
    if strcmp(CalcType,'actmaps')
        FileName = ['EMD_subj' num2str(sub) '_mc_ss_VT.mat'];
        if exist(fullfile(subj_dir,FileName),'file') == 2
            disp('EMD matrix already calculated');
        else
            save(fullfile(subj_dir,FileName),'EMD_Euc2','EMD_Surf','CorrMtrx');
        end
    elseif strcmp(CalcType,'tmaps')
        FileName = ['EMD_subj' num2str(sub) '_mc_ss_VT_tmaps.mat'];
        if exist(fullfile(subj_dir,FileName),'file') == 2
            disp('EMD matrix already calculated');
        else
            save(fullfile(subj_dir,FileName),'EMD_Euc2','EMD_Surf','CorrMtrx');
        end
    end
    disp(['Subject ' num2str(sub) ' is done']);
    
end