#! bin/bash

export SUBJECTS_DIR=~/Desktop/Testdata
source ~/.bash_profile

export SUMA_Al_Surfaces=${SUBJECTS_DIR}/subj1_suma_surfaces_al_mean_bold_mc
cd ${SUMA_Al_Surfaces}
echo "Aligned surfaces path is ${SUMA_Al_Surfaces}"
export SurfVol=subj1_fs_new_anat_all_SurfVol_ss_al2exp+tlrc

###################################################################
# 1. 
# AFNI command: 3dVol2Surf
# Map data from a volume domain to a surface domain
###################################################################
# Resolution
export resolution=128
# Hemisphere: l(eft), r(ight), m(erged)
export hemi=m
# Output file type: niml, 1D
export Outtype=1D
echo "Output file type is ${Outtype}"

if [ ${Outtype} == 1D ]; then
        export OutName=Subj1_act_con1_vals_res${resolution}_${hemi}h.1D
        echo $OutName 
        if [ ! -f $OutName ]; then
            3dVol2Surf  -spec ${hemi}h_ico${resolution}_al.spec \
                        -surf_A ico${resolution}_${hemi}h.smoothwm_al.asc \
                        -surf_B ico${resolution}_${hemi}h.pial_al.asc \
                        -sv ${SurfVol} \
                        -grid_parent subj1_actmaps_cond1_VT_mc_ss.nii \
                        -map_func seg_vals \
                        -out_1D $OutName
        fi

    elif [ ${Outtype} == niml ]; then
        export OutName=Subj1_act_con1_vals_res${resolution}_${hemi}h.niml.dset
        echo $OutName
        if [ ! -f $OutName ]; then
            3dVol2Surf -spec ${hemi}h_ico${resolution}_al.spec \
                -surf_A ico${resolution}_${hemi}h.smoothwm_al.asc \
                -surf_B ico${resolution}_${hemi}h.pial_al.asc \
                -sv ${SurfVol} \
                -grid_parent subj1_actmaps_cond1_VT_mc_ss.nii \
                -map_func seg_vals \
                -out_niml $OutName
        fi
fi

###################################################################
# 2. 
# Map VOI (VT mask) to a surface domain
###################################################################
# cd ~/Desktop/Testdata
# fslmaths bold_raw_mc_mean.nii -mul mask4_vt.nii bold_raw_mc_mean_VT.nii
export OutName=Subj1_vt_mask_res${resolution}_${hemi}h.1D
echo $OutName
if [ ! -f $OutName ]; then
    3dVol2Surf -spec ${hemi}h_ico${resolution}_al.spec \
        -surf_A ico${resolution}_${hemi}h.smoothwm_al.asc \
        -surf_B ico${resolution}_${hemi}h.pial_al.asc \
        -sv ${SurfVol} \
        -map_func seg_vals \
        -out_1D $OutName \
        -grid_parent bold_raw_mc_mean_VT.nii.gz   
fi


###################################################################
# 3. Use flattened patch
# Need to convert the flatmap first?
###################################################################
export OutName=Subj1_flat_res${resolution}_lh.1D
echo $OutName
if [ ! -f $OutName ]; then
    3dVol2Surf  -spec lh_ico${resolution}_al.spec \
        -surf_A lh.full.patch.flat_test1.out \
        -sv ${SurfVol} \
        -grid_parent bold_raw_mc_mean_VT.nii.gz \
        -map_func mask \
        -out_1D ${OutName}
fi
# tksurfer subj1_fs_new_anat_all lh pial


###################################################################
# 4. Convert surface coordinates directly
###################################################################
export OutName=ico${resolution}_${hemi}h.smoothwm_al.1D
echo $OutName

if [ ! -f ${OutName}.coord ]; then
    ConvertSurface \
        -i_fs ico${resolution}_${hemi}h.smoothwm_al.asc \
        -o_1D ico${resolution}_${hemi}h.smoothwm_al.1D.coord \
        ico${resolution}_${hemi}h.smoothwm_al.1D.topo
fi

###################################################################
# 7. Generate mask and then project functional volume to the surface 
###################################################################
export Maskname=ico${resolution}_${hemi}h_mask.asc
echo ${Maskname}
# Convert to AFNI format
3dcopy ~/Desktop/Testdata/mask4_vt.nii ~/Desktop/Testdata/mask4_vt+orig
if [ ! -f ${OutName} ]; then
    3dmaskdump \
        -mask mask4_vt+orig \
        -o ${Maskname} \
        mask4_vt+orig
fi

export OutName=ico${resolution}_${hemi}h_VTmask.1D
echo $OutName
if [ ! -f $OutName ]; then
    3dVol2surf  -spec ${hemi}h_ico${resolution}_al.spec \
        -surf_A ico${resolution}_${hemi}h.smoothwm_al.asc \
        -surf_B ico${resolution}_${hemi}h.pial_al.asc \
        -sv ${SurfVol} \
        -grid_parent bold_raw_mc_mean.nii \
        -cmask mask4_vt+orig \
        -map_func mask \
        -out_1D ${OutName}
fi


###################################################################
# 5. Use pymvpa method
###################################################################
function PyMVPA_surf {
python - <<END
from mvpa2.suite import *
import os
import scipy

datadbroot = '/axelUsers/xwang91/Desktop/Testdata'
func_data_path = os.path.join(datadbroot, 'bold_raw_mc_mean_VT.nii.gz')
func_data = fmri_dataset(func_data_path)

vg = volgeom.from_any(func_data)
xyz = vg.ijk2xyz(func_data.fa.voxel_indices)

print xyz

FileName = 'bold_raw_mc_mean_VT_coords.mat'
print FileName
scipy.io.savemat(os.path.join(datadbroot, FileName),{'xyzcoords':xyz})

END
}

# Call Python function
# PyMVPA_surf

###################################################################
# 6. Use pymvpa method
###################################################################
function PyMVPA_surf2 {
python - <<END
from mvpa2.suite import *
import os
import scipy
import numpy as np

datadbroot = '/axelUsers/xwang91/Desktop/Testdata'
func_data_path = os.path.join(datadbroot, 'bold_raw_mc_mean.nii')
mask_vol_path = os.path.join(datadbroot, 'mask4_vt.nii')
func_data = fmri_dataset(func_data_path)
mask_data = fmri_dataset(mask_vol_path)
mask_bool = np.array(mask_data,dtype='bool').T

vg = volgeom.from_any(func_data)
print vg
ijk_idx = func_data.fa.voxel_indices
xyz_spa = vg.ijk2xyz(ijk_idx)

print ijk_idx
print xyz_spa

FileName = 'bold_raw_mc_mean_coords.mat'
print FileName
scipy.io.savemat(os.path.join(datadbroot, FileName),{'xyzcoords':xyz_spa, 'ijkcoords':ijk_idx, 'mask_data':mask_data.samples})

END
}

# PyMVPA_surf2
# python from mvpa2.suite import * help(volgeom)


# Methods 1 and 2 generate same results: for merged hemisphere, 327683 
# nodes were generated. For left hemisphere, 163841 nodes were generated.
# For method 5, 163840 nodes were generated.
# Here each voxel is treated as a node? See whether 3dVol2Surf can be applied to ROIs/VOIs only

# Possible commands: SurfaceMetrics Surf2VolCoord SurfMeasures
# When using AFNI & SUMA, it shows XYZ coordinates for each node (RAS), do they stand for surface coordinates? (Negative values exist)

# --------
# My guess is: first apply VT masks, then extract nodes with values, then calculate edges between different nodes
