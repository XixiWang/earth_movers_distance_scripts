#! /bin/bash

# Surface settings
export resolution=64
export hemi=m
export Outtype=1D
export mapfunc=ave

for nsub in subj1
do
    export SUBJECTS_DIR=~/Documents/MATLAB/Data/haxby2001
    source ~/.bash_profile
    
    export Data_DIR=$SUBJECTS_DIR/$nsub
    export Surf_DIR=$SUBJECTS_DIR/${nsub}_suma_surfaces_al_mean_bold_mc
    export Coords_DIR=$SUBJECTS_DIR/${nsub}_SurfCoords

    export NodesListName=${nsub}_Nodelist_complete_unique_res${resolution}.1D
    
    # Euclidian distance 
    SurfDist    -i_fs $Surf_DIR/ico${resolution}_${hemi}h.intermediate_al.asc \
                -input $Coords_DIR/$NodesListName \
                -Euclidian \
                -node_path_do node_path > $Coords_DIR/${nsub}_checkEuc.1D
    
    echo 'Then visualize the distance: --------- '
    cd $Surf_DIR
    suma -niml & 
    DriveSuma -com show_surf -label ico -i_fs $Surf_DIR/ico${resolution}_${hemi}h.intermediate_al.asc \
              -com viewer_cont -load_do $Coords_DIR/node_path.1D.do

    # Surface based distance 
    
    
done
