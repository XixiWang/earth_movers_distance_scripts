% Generate new surface distance matrices
clear 

addpath('~/Dropbox/LAB_Folder_Macbook/MATLAB/Code/FastEMD_PeleWerman/');
addpath('~/Dropbox/LAB_Folder_Macbook/MATLAB/Utilities/');

CalcType = 'actmaps';
disp(CalcType);

extra_mass_penalty = -1;

for sub = 1:5
    disp(['Analyzing subject' num2str(sub)]);
    subj_dir = ['~/Dropbox/LAB_Folder_Macbook/MATLAB/Data/haxby2001/subj' num2str(sub)];
    subj_surfdir = ['~/Dropbox/LAB_Folder_Macbook/MATLAB/Data/haxby2001/subj' num2str(sub) '_SurfCoords'];
    
    addpath(subj_dir);
    addpath(subj_surfdir);
    cd(subj_dir);
    
    disp('Load raw ground distance matrices');
    GDFile = ['Subj' num2str(sub) '_GDMatrices.mat'];
    load(GDFile);
    
    if strcmp(CalcType,'actmaps')
        disp('Load activity maps (generated based on individual VT masks)')
        ActFile = ['subj' num2str(sub) '_actmaps_VT_mc_ss.mat'];
        load(ActFile);
        Subj_data = double(eval(['subj' num2str(sub) '_data']))';
    elseif strcmp(CalcType,'tmaps')
        disp('Load t maps (generated based on individual VT masks)')
        tmapsFile = ['subj' num2str(sub) '_tmaps_VT_mc_ss.mat'];
        load(tmapsFile);
        Subj_data = double(eval(['subj' num2str(sub) '_tmaps']))';        
    end
    
    % Generate activity maps' vector
    Subj_coords = double(eval(['subj' num2str(sub) '_coords']));
    disp([num2str(size(Subj_coords,1)) ' voxels are selected from the VT mask (PyMVPA)'])
    disp([num2str(size(SingleNodeInfo,1)) ' voxels are used for cortical surface distance calculation (AFNI)'])
    
    % Subscripts to indices
    imgsize = [40,64,64];
    Subj_coords = [Subj_coords zeros(size(Subj_coords,1),1)];
    for ctr = 1:size(Subj_coords,1)
        Subj_coords(ctr,4) = sub2ind(imgsize,Subj_coords(ctr,1), ...
            Subj_coords(ctr,2) + 1, ...
            Subj_coords(ctr,3) + 1);
    end
    Subj_coords = [Subj_coords(:,4) Subj_coords(:,1:3)];
    
    % Sort according to each voxel's index and see whether it's same to
    % what afni generated
    % [~,IX] = sort(Subj_coords(:,1));
    % Subj_coords_sorted = Subj_coords(IX,:);
    PyMVPA_coords_val = [Subj_coords Subj_data];
    
    % Find corresponding values based on coordinates
    Vals = zeros(size(SingleNodeInfo,1),8);
    for ctr = 1:size(SingleNodeInfo,1)
        Vals(ctr,:) = PyMVPA_coords_val(PyMVPA_coords_val(:,1) == SingleNodeInfo(ctr,2),5:12);
    end
    Afni_coors_val = [SingleNodeInfo Vals];
    
    if strcmp(CalcType,'actmaps')
        Afni_coors_val_HDR = {'Local idx','index','Coords','Act maps'};
    elseif strcmp(CalcType,'tmaps')
        Afni_coors_val_HDR = {'Local idx','index','Coords','tmaps'};
    end
    
    % Generate symmetric matrices
    GD_Euc_com = GD_Euc + GD_Euc';
    GD_Surf_com = GD_Surf + GD_Surf';
    
    % To deal with -1 values: -1 values indicate that for two nodes in the
    % cortical surface, they tend to be relatively faraway from each other
    % Assume there will be no information transfer for certain nodes?
    temp = GD_Surf_com;
    tempmax = max(temp(:));
    GD_Surf_com(GD_Surf_com == -1) = tempmax * 100;
    % Perform thresholding 
    GD_Surf_com = min(GD_Surf_com,tempmax);
    
    
end