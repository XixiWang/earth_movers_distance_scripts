from mvpa.suite import *
import os
from matplotlib.pyplot import figure, show
from mvpa.misc.io.base import SampleAttributes
from mvpa.datasets.mri import fmri_dataset
import scipy
import scipy.stats
import scipy.spatial  # For the squareform function
import numpy as np
#import mvpa.base.hdf5 # For writing the results in a Matlab-readable format

pymvpa_datadbroot = '/Users/XixiWang/Dropbox/LAB_Folder_Macbook/MATLAB/Data/haxby2001/'
HarvardOxford_path = os.path.join(pymvpa_datadbroot,'HarvardOxford_masks')
labels = ['bottle','cat','chair','face','house','scissors','scrambledpix','shoe','rest']

# Analyze first five subjects
num_subjs = 5
num_categories = 8
num_HarvardOxford_rois = 48
num_squareform_vals = num_categories * (num_categories-1) / 2
all_subjs_squareform_sims = scipy.zeros((num_subjs,num_HarvardOxford_rois,num_squareform_vals),float)

for subj_num in range(num_subjs):
    subj_string = 'subj' + str(subj_num + 1) 
    subjpath = os.path.join(pymvpa_datadbroot, subj_string)
    
    # Use motion corrected bold files 
    bold_nii_file = os.path.join(subjpath, 'bold_raw_mc.nii.gz')
    attrs = SampleAttributes(os.path.join(subjpath, 'labels.txt'),header=True)

    for roi_num in range(num_HarvardOxford_rois):
        print "About to import the dataset for " + subj_string + " ROI " + str(roi_num+1)
        HarvardOxford_roi_name = 'HarvardOxford_haxby_normed_' + str(roi_num+1) + '.nii.gz'
        
        # Load different Harvard-Oxford ROI masks
        my_dataset = mvpa.datasets.mri.fmri_dataset( samples=bold_nii_file,
        targets=attrs.labels,chunks=attrs.chunks,mask=os.path.join(HarvardOxford_path,HarvardOxford_roi_name))

        detrender = PolyDetrendMapper(polyord=1, chunks_attr='chunks')
        detrended_dataset = my_dataset.get_mapped(detrender)
        zscore(detrended_dataset,param_est=('targets', ['rest']))
        
        # Now remove the rest periods
        detrended_dataset_without_rest_periods = detrended_dataset[detrended_dataset.sa.targets != 'rest']
        averager = mean_group_sample(['targets'])
        averaged_dataset = detrended_dataset_without_rest_periods.get_mapped(averager)
        averaged_fMRI_values = averaged_dataset.samples
        
        # Now make the correlation matrix
        sim_matrix = scipy.corrcoef(averaged_fMRI_values)
        # It seems that the squareform command is very picky about matrix symmetry,
        # so that even rounding errors make it give an error.
        # So, we need to turn off that checking. 
        # Also, we are using similarities instead of 1-corr,
        # so the diagonal is made of ones, not zeros.
        this_squareform_vec = scipy.spatial.distance.squareform(sim_matrix,checks=False)
        all_subjs_squareform_sims[subj_num,roi_num,:] = this_squareform_vec

        this_subj_data_roi = scipy.zeros(num_categories,dtype=np.object)

        # Extract different conditions
        for target_num in range(num_categories):
            this_target = labels[target_num]
            target_data = detrended_dataset[detrended_dataset.sa.targets == this_target]
            this_subj_data_roi[target_num] = target_data.samples
            print this_target + str(target_data.shape)

        # Save data for each ROI 
        data_to_save = {}
        data_to_save[subj_string + '_ROI_' + str(roi_num) + '_data'] = this_subj_data_roi
        data_to_save[subj_string + '_ROI_' + str(roi_num) + '_coords'] = detrended_dataset.fa.voxel_indices

# scipy.io.savemat('normed_haxby2001_HarvOxf_sim_matrices.mat',{'all_subjs_squareform_sims':all_subjs_squareform_sims})


