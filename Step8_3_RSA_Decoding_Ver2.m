% Perform decoding based on rank-transformed RDMs
clear 
close all

Sub_dir = '~/Dropbox/LAB_Folder_Macbook/MATLAB/Data/haxby2001/';
addpath('~/Dropbox/LAB_Folder_Macbook/MATLAB/Utilities/');
rsapath = '~/Dropbox/LAB_Folder_Macbook/MATLAB/Toolboxes/rsatoolbox/';
addpath(genpath(rsapath));

labels = {'bottle','cat','chair','face','house','scissors','scrambledpix','shoe'};
% CalcType: 'tmaps' or 'actmaps'
CalcType = 'actmaps';
% EMDType: 'EMDVec' or 'EMDVec_norm'
EMDType = 'EMDVec';

% Configure settings
userOptions.rootPath = fullfile(Sub_dir,'RSAToolBox_test');
userOptions.analysisName = 'Corr_EMD_test';
userOptions.saveFiguresPDF = 1;
userOptions.conditionLabels = labels;
% userOptions.colourScheme = gray;

% Configure variables
userOptions.distance = 'Correlation'; % Default similarity measurement
% Calculate the dissimilarity between RDMs - rank correlation
userOptions.distanceMeasure = 'Spearman';

% Load variables
MatName = 'mc_ss_VT_new3';
tempFile = fullfile(Sub_dir,'subj1',['EMD_subj1_' MatName '.mat']);
% tempfile = '~/Dropbox/LAB_Folder_Macbook/MATLAB/Data/haxby2001/subj1/EMD_subj1_mc_ss_VT_new.mat';
AllVarNames = who('-file',tempFile);

% % Rank transformation and then decoding
% for ctr = 1:numel(AllVarNames)
%     disp(AllVarNames{ctr});
%     tempRDM = GenerateGroupMtrx(MatName, CalcType,AllVarNames{ctr});
%     
%     EMD_based_vec_rank = zeros(1,28,5);
%     for ctri = 1:5
%         EMD_based_vec_rank(1,:,ctri) = scale01(rankTransform_equalsStayEqual(tempRDM(ctri,:)));
%     end
%     % Perform decoding
%     
%     EMD_based_mtrx_rank = squeeze(EMD_based_vec_rank)';
%     [DecodingVals, MeanCorr] = AcrossSubsDecoding(EMD_based_mtrx_rank);
%     disp('---------------------------')
% end

% Rank transformation and then decoding
% for ctr = 1:numel(AllVarNames)
for ctr = 55
%     
    disp(AllVarNames{ctr});
    tempRDM = GenerateGroupMtrx(MatName, CalcType,AllVarNames{ctr});
    
    EMD_based_vec_rank = zeros(1,28,5);
    EMD_based_vec_rank_scaled = zeros(1,28,5);

    for ctri = 1:5
        EMD_based_vec_rank(1,:,ctri) = rankTransform_equalsStayEqual(tempRDM(ctri,:));
        EMD_based_vec_rank_scaled(1,:,ctri) = scale01(rankTransform_equalsStayEqual(tempRDM(ctri,:)));
    end
    
    fprintf('EMD_based_vec_rank is equal to EMD_based_vec_rank_scaled? %i \n',...
        isequal(EMD_based_vec_rank, EMD_based_vec_rank_scaled));
    
    % Perform decoding
    EMD_based_mtrx_rank = squeeze(EMD_based_vec_rank)';
    [DecodingVals, MeanCorr] = AcrossSubsDecoding(EMD_based_mtrx_rank);
    disp('---------------------------')
end