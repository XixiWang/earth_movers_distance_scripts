function GenerateNodesList_complete(var1, var2,var3,var4)
setenv('Coords_DIR',var1);
setenv('FileName', var2);
setenv('resolution',var3);
setenv('nsub',var4);

!echo $Coords_DIR
!echo $FileName
!echo $resolution
!echo $nsub

disp('Running MATLAB function!');

Coords_DIR = getenv('Coords_DIR');
FileName = getenv('FileName');
resolution = getenv('resolution');
nsub = getenv('nsub');

load(fullfile(Coords_DIR,strcat(FileName,'.1D')));
cd(Coords_DIR)
% save(fullfile(Coords_DIR,'NodesInfo.mat'))

NodesInfo = eval(FileName);
Nodes_idx = NodesInfo(:,1);

% Number of node pairs
num_Nodes = length(Nodes_idx);
disp([num2str(num_Nodes) ' nodes are selected'])

% Generate node pairs list
a = 1;
Nodeslist_com = zeros(num_Nodes * (num_Nodes - 1) / 2, 2);
for ctri = 1:num_Nodes
    for ctrj = 1:num_Nodes
        if ctri < ctrj
            Nodeslist_com(a,1) = Nodes_idx(ctri);
            Nodeslist_com(a,2) = Nodes_idx(ctrj);
            a = a + 1;
        end
    end
end

% codegen ~/Desktop/Scripts/NodesList_for -o NodesList_for_mex -args {zeros(num_Nodes,7)}
% Nodeslist_com = NodesList_for_mex(NodesInfo);

% Write ascii file
FileName = [nsub '_Nodelist_complete_res' resolution '.1D'];
dlmwrite(FileName, Nodeslist_com, ' ');

end