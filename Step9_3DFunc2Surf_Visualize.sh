#! /bin/bash

# To project functional data under different conditions onto cortical surface
# 3D functional data -- 2D surfaces 

# Labels
declare -a label=("bottle" "cat" "chair" "face" "house" "scissors" "scrambledpix" "shoe")
labellength=${#label[@]}
echo $labellength
# Hemisphere
export hemi=m

# Subject(s)
for nsub in subj1 subj2 subj3 subj4 subj5
do
    echo "Project functional data onto the surface and mix them in suma"
    echo "Analyzing $nsub"

    export DBRoot=~/Dropbox/LAB_Folder_Macbook/MATLAB/Data/haxby2001
    export SUBPATH=$DBRoot/$nsub
    export Surf_DIR=$DBRoot/${nsub}_suma_surfaces_al_mean_bold_mc
    export SurfVol_ss=${nsub}_fs_new_anat_all_SurfVol_ss_al2exp+tlrc
    cd $Surf_DIR
    
    for (( ctr = 1; ctr < ${labellength} + 1; ctr ++ ));
    do 
        export sti=${label[${ctr} - 1]}
        echo "Analyzing stimuli ${sti}"
        export OutName=${nsub}_vol2surf_actmaps_${hemi}h_res64_${sti}.niml.dset
        
        # Project functional data onto surfaces
        if [ ! -e "~/Dropbox/${OutName}" ]; then
            3dVol2Surf -spec $Surf_DIR/${hemi}h_ico64_al.spec \
                -surf_A $Surf_DIR/ico64_${hemi}h.smoothwm_al.asc \
                -surf_B $Surf_DIR/ico64_${hemi}h.pial_al.asc \
                -sv $Surf_DIR/${SurfVol_ss} \
                -grid_parent $Surf_DIR/${nsub}_actmaps_cond${ctr}_VT_mc_ss.nii \
                -map_func ave \
                -out_niml $Surf_DIR/$OutName
        else
            echo "Functional data for stimuli ${sti} have already been projected"
        fi
    done

    # Output 1D files to check the number of voxels for each stimuli
    for (( ctr = 1; ctr < ${labellength} + 1; ctr ++ ));
    do 
        export sti=${label[${ctr} - 1]}
        echo "Analyzing stimuli ${sti}"
        export OutName=${nsub}_vol2surf_actmaps_${hemi}h_res64_${sti}.niml.dset
        
        # Project functional data onto surfaces
        if [ ! -e "~/Dropbox/${OutName}" ]; then
            3dVol2Surf -spec $Surf_DIR/${hemi}h_ico64_al.spec \
                -surf_A $Surf_DIR/ico64_${hemi}h.smoothwm_al.asc \
                -surf_B $Surf_DIR/ico64_${hemi}h.pial_al.asc \
                -sv $Surf_DIR/${SurfVol_ss} \
                -grid_parent $Surf_DIR/${nsub}_actmaps_cond${ctr}_VT_mc_ss.nii \
                -map_func ave \
                -out_niml $Surf_DIR/$OutName
        else
            echo "Functional data for stimuli ${sti} have already been projected"
        fi
    done      
done





