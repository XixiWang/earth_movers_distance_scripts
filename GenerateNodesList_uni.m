function GenerateNodesList_uni(var1, var2,var3,var4)
setenv('Coords_DIR',var1);
setenv('FileName', var2);
setenv('resolution',var3);
setenv('nsub',var4);

!echo $Coords_DIR
!echo $FileName
!echo $resolution
!echo $nsub

disp('Running MATLAB function!');

Coords_DIR = getenv('Coords_DIR');
FileName = getenv('FileName');
resolution = getenv('resolution');
nsub = getenv('nsub');

load(fullfile(Coords_DIR,strcat(FileName,'.1D')));
cd(Coords_DIR)
% save(fullfile(Coords_DIR,'NodesInfo.mat'))

NodesInfo = eval(FileName);
Nodes_idx = NodesInfo(:,1);
Voxel_coords = NodesInfo(:,3:5);

% Select unique voxel coords
[~,uni_idx,~] = unique(Voxel_coords,'rows','stable');
Nodes_idx_uni = Nodes_idx(uni_idx);

% Number of node pairs
num_Nodes_uni = length(Nodes_idx_uni);
disp([num2str(num_Nodes_uni) ' nodes are selected'])

% Generate node pairs list
a = 1;
Nodeslist = zeros(num_Nodes_uni * (num_Nodes_uni - 1) / 2, 2);
for ctri = 1:num_Nodes_uni
    for ctrj = 1:num_Nodes_uni
        if ctri < ctrj
            Nodeslist(a,1) = Nodes_idx_uni(ctri);
            Nodeslist(a,2) = Nodes_idx_uni(ctrj);
            a = a + 1;
        end
    end
end

% Write ascii file
FileName = [nsub '_Nodelist_complete_unique_res' resolution '.1D'];
dlmwrite(FileName, Nodeslist, ' ');

end