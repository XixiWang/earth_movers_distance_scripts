%%
% Decoding based on correlation
clear
close all

addpath('~/Dropbox/LAB_Folder_Macbook/MATLAB/Utilities/');

CalcType = 'actmaps';
disp(CalcType);

Script_dir = '~/Dropbox/LAB_Folder_Macbook/MATLAB/Code/EMD_Scripts';
Sub_dir = '~/Dropbox/LAB_Folder_Macbook/MATLAB/Data/haxby2001/';
% ---------------------------------------------------------
MatName = 'mc_ss_VT_new3';
% ---------------------------------------------------------
tempFile = fullfile(Sub_dir,'subj1',['EMD_subj1_' MatName '.mat']);
% tempFile = '~/Dropbox/LAB_Folder_Macbook/MATLAB/Data/haxby2001/subj1/EMD_subj1_mc_ss_VT.mat';

AllVarNames = who('-file',tempFile);
AllVarNames_num = length(AllVarNames);
Decoding_Acc_values = zeros(length(AllVarNames),1);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 1. Perform decoding using EMD matrices directly
% EMD is NOT similarity matrix so the decoding results may not be good!
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
for ctr = 1:length(AllVarNames)
    disp(AllVarNames{ctr});
    % Combine individual results
    GroupMtrx = GenerateGroupMtrx(MatName, CalcType,AllVarNames{ctr});
    % Perform decoding and display results
    [Decoding_Results,mean_subj_prop_correct] = AcrossSubsDecoding(GroupMtrx);
    Decoding_Acc_values(ctr,1) = mean_subj_prop_correct;
    % Save decoding results
    clear GroupMtrx mean_subj_prop_correct;
end
% save(fullfile(Script_dir,'Decoding_Acc.mat'),'Decoding_Acc_values');

%% 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 2. Perform decoding using normalized EMD 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
Decoding_Acc_values_ind_norm = zeros(length(AllVarNames),1);
for ctr = 1:length(AllVarNames)
    disp(AllVarNames{ctr});
    % Combine individual results
    GroupMtrx = GenerateGroupMtrx(MatName, CalcType,AllVarNames{ctr});
    % Normalize each subject's EMD values
    GroupMtrx_norm = zeros(size(GroupMtrx));
    for ctrj = 1:size(GroupMtrx,1)
        GroupMtrx_norm(ctrj,:) = mat2gray(GroupMtrx(ctrj,:));
    end
    
    % Perform decoding and display results
    [Decoding_Results,mean_subj_prop_correct] = AcrossSubsDecoding(GroupMtrx_norm);
    Decoding_Acc_values_ind_norm(ctr,1) = mean_subj_prop_correct;
    % Save decoding results
    clear GroupMtrx GroupMtrx_norm mean_subj_prop_correct;
end
% save(fullfile(Script_dir,'Decoding_Acc_ind_norm.mat'),'Decoding_Acc_values_ind_norm');

%% 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 3. Perform decoding using 1 - normalized EMD 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
Decoding_Acc_values_ind_norm_sim = zeros(length(AllVarNames),1);
for ctr = 1:length(AllVarNames)
    disp(AllVarNames{ctr});
    % Combine individual results
    GroupMtrx = GenerateGroupMtrx(MatName, CalcType,AllVarNames{ctr});
    % Normalize each subject's EMD values
    GroupMtrx_norm_sim = zeros(size(GroupMtrx));
    for ctrj = 1:size(GroupMtrx,1)
        GroupMtrx_norm_sim(ctrj,:) = 1 - mat2gray(GroupMtrx(ctrj,:));
    end
    
    % Perform decoding and display results
    [Decoding_Results,mean_subj_prop_correct] = AcrossSubsDecoding(GroupMtrx_norm_sim);
    Decoding_Acc_values_ind_norm_sim(ctr,1) = mean_subj_prop_correct;
    % Save decoding results
    clear GroupMtrx GroupMtrx_norm_sim mean_subj_prop_correct;
end
% save(fullfile(Script_dir,'Decoding_Acc_ind_norm_sim.mat'),'Decoding_Acc_values_ind_norm_sim');


%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Convert EMD (dissimilarity) to similarity matrices and
% then perfrom decoding
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
CorrMtrx = GenerateGroupMtrx(CalcType,AllVarNames{1});
EMDMtrx = GenerateGroupMtrx(CalcType,AllVarNames{6});
EMDMtrx_mat = mat2gray(EMDMtrx);


% Decoding based on similarity matrices 
% Sim = 1 - Dis
% Decoding_Results11 = AcrossSubsDecoding(Sim_EMDMtrx);

% Dis = sqrt(1 - sim)
% Sim_EMDMtrx2 = 1 - EMDMtrx_mat.^2;
% Decoding_Results22 = AcrossSubsDecoding(Sim_EMDMtrx2);

% Convert EMD to sim for each sub
Sim_EMDMtrx3 = zeros(size(EMDMtrx_mat));
for ctr = 1:5
    temp = mat2gray(EMDMtrx(ctr,:));
    Sim_EMDMtrx3(ctr,:) = temp;
end
Decoding_Results33 = AcrossSubsDecoding(Sim_EMDMtrx3);

%% Dissimilarity to similarity conversion
disp(AllVarNames{9})
EMDMtrx = GenerateGroupMtrx(CalcType,AllVarNames{9});
EMDMtrx_mat = mat2gray(EMDMtrx);

% Convert matrix to [0,255]
EMDMtrx_mat_rescaled = EMDMtrx_mat * 255;

% Convert matrix to [0.1 0.95]
EMDMtrx_mat_rescaled_temp = EMDMtrx * 0.95;

CorrMtrx2 = GenerateGroupMtrx(CalcType,AllVarNames{1});

% Check value distributions
figure;subplot(1,2,1);hist(EMDMtrx(:),50);
subplot(1,2,2);hist(EMDMtrx_mat(:),50);
figure;hist(EMDMtrx_mat_rescaled(:),50);

% Find out maximum and minimum values
maxEMD = max(EMDMtrx_mat(:)); minEMD = min(EMDMtrx_mat(:));

% Convert dissimilarity values to similarity values 
% For different subjects, EMD value ranges differ 
% Linear shift 
Converted_sim = 1 - EMDMtrx_mat;
figure;hist(Converted_sim(:),50);