# Earth Mover's Distance scripts to analyze Haxby dataset #

### Script summary ###

* Scripts are generated to perform pre-processing, cortical surface extraction and EMD calculation of the fMRI dataset