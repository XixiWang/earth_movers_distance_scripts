clear;
close all;

addpath('~/Dropbox/LAB_Folder_Macbook/MATLAB/Code/FastEMD_PeleWerman/');
addpath('~/Dropbox/LAB_Folder_Macbook/MATLAB/Utilities/');
addpath('~/Dropbox/LAB_Folder_Macbook/MATLAB/Utilities/histnorm/');

labels = {'bottle','cat','chair','face','house','scissor','scrapix','shoe'};

% Calculation type: 'actmaps' or 'tmaps'
CalcType = 'actmaps';
disp(CalcType);

% Calculate fast EMD (histograms don't have to be normalized)
extra_mass_penalty = -1;
% For Rubner's EMD, extra_mass_penalty = 0

ncond = 8;
all_conds_pairs = nchoosek(1:ncond,2);

% for sub = 1:5
for sub = 1
    disp(['Analyzing subject' num2str(sub)]);
    subj_dir = ['~/Dropbox/LAB_Folder_Macbook/MATLAB/Data/haxby2001/subj' num2str(sub)];
    cd(subj_dir);
    
    if strcmp(CalcType,'actmaps')
        EMD_PrefileName = ['EMD_pre_subj' num2str(sub) '_mc_ss_VT2.mat'];
    elseif strcmp(CalcType,'tmaps')
        EMD_PrefileName = ['EMD_pre_subj' num2str(sub) '_mc_ss_VT_tmaps2.mat'];
    end
    load(EMD_PrefileName);
    
    GroundDistFiles = who('GD_Surf_com*');
    GDlength = size(GroundDistFiles,1);
    
    GroundDistFiles{GDlength + 1,1} = 'GD_Euc_com';

    GDMtrx = GD_Euc_com;
    % Matrix initialization
    EMDMtrx = zeros(ncond * (ncond - 1)/2,1);
    EMDMtrx_Rub = EMDMtrx;
    EMDMtrx_scaled = EMDMtrx;
    EMDMtrx_scaled_Rub = EMDMtrx;
    
    % Generate table header info
    % fileID = fopen(['subj' num2str(sub) '_Euc_com_TestResults.txt'],'at');
    % fprintf(fileID,'Condition1\tCondition2\tFast EMD\tRubner EMD\tScaled fEMD\tSRubner EMD\n');
    for condpairs = 1:size(all_conds_pairs,1)
        condA = Afni_coors_val(:,5 + all_conds_pairs(condpairs,1));
        condB = Afni_coors_val(:,5 + all_conds_pairs(condpairs,2));
        
        % Normalize values to [0,1]
        condA_mat = mat2gray(condA);
        condB_mat = mat2gray(condB);
        
        % ***************************************************************
        % Updated on 07/01/2015: Histogram equalization
        % Method 1: NEW histogram function has been used here
        % ***************************************************************
        nbins = 100;
        figure; title('Method 1 condA');
        hold on;
        hcondA = histogram(condA,nbins);
        % Normalize histogram using pdf
        hcondA_norm_pdf = histogram(condA,nbins,'Normalization','pdf');
        sig1_val_norm_pdf = hcondA_norm_pdf.Values; % Return histogram values 
        % sig1_ctr_norm_pdf = hcondA_norm_pdf.
        % Normalize histogram using probability distribution
        hcondA_norm_prob = histogram(condA,nbins,'Normalization','probability');
        sig1_val_norm_prob = hcondA_norm_prob.Values;
        
        figure; title('Method 1 condB');
        hold on;
        hcondB = histogram(condB,nbins);
        hcondB_norm_pdf = histogram(condB,nbins,'Normalization','pdf');
        sig2_val_norm_pdf = hcondB_norm_pdf.Values;
        hcondB_norm_prob = histogram(condB,nbins,'Normalization','probability');
        sig2_val_norm_prob = hcondB_norm_prob.Values;
        
        % close all;        
        % Check the total weight of histogram: 
        % S1 = sum(h1.values);
        % S2 = sum(h2.values); Sum of bar AREA is 1
        % S3 = sum(h3.values); Sum of bar HEIGHT is 1
        
        % ***************************************************************
        % Method 2: each voxel is treated as a histogram bin
        % ***************************************************************
        figure; title('Method 2 condA');
        hold on;
        nbins2 = length(condA);
        hcondA2 = histogram(condA,nbins2);
        hcondA2_vals = hcondA2.Values;
        hcondA2_binedges = hcondA2.BinEdges;
        
        hcondA2_norm_pdf = histogram(condA,nbins2,'Normalization','pdf');
        hcondA2_norm_pdf_vals = hcondA2_norm_pdf.Values;
        
        hcondA2_norm_prob = histogram(condA,nbins2,'Normalization','probability');
        hcondA2_norm_prob_vals = hcondA2_norm_prob.Values;
        
        figure; title('Method 2 condB');
        hold on;
        hcondB2 = histogram(condB,nbins2);
        hcondB2_vals = hcondB2.Values;
        hcondB2_binedges = hcondB2.BinEdges;
        
        hcondB2_norm_pdf = histogram(condB,nbins2,'Normalization','pdf');
        hcondB2_norm_pdf_vals = hcondB2_norm_pdf.Values;
        
        hcondB2_norm_prob = histogram(condB,nbins2,'Normalization','probability');
        hcondB2_norm_prob_vals = hcondB2_norm_prob.Values;
        
        close all;
        
        % Select which histograms are gonna be used
        histo_type = 'original';
        % EMD calculates distances between different bins:
        switch histo_type
            case 'original'
                sig1 = hcondA2_vals;
                sig2 = hcondB2_vals;
            case 'norm_pdf'
                sig1 = hcondA2_norm_pdf_vals;
                sig2 = hcondB2_norm_pdf_vals;
            case 'norm_prob'
                sig1 = hcondA2_norm_prob_vals;
                sig2 = hcondB2_norm_prob_vals;
        end
        
        bin_condA_idx = cell(size(sig1));
        bin_condB_idx = cell(size(sig2));   
        for ctr = 1:length(sig1)
            % For each bin find corresponding idx (each bin could either have single/multiple idx or null)
            tempidxA = find(hcondA2_binedges(ctr) <= condA & condA <= hcondA2_binedges(ctr + 1));
            tempidxB = find(hcondB2_binedges(ctr) <= condB & condB <= hcondB2_binedges(ctr + 1));
            
            if ~isempty(tempidxA) 
                bin_condA_idx{ctr} = tempidxA;
            end

            if ~isempty(tempidxB)
                bin_condB_idx{ctr} = tempidxB;
            end
        end
        
        % Bin to bin distance calculation - based on coordinates
        
        
        % ***************************************************************
        
        
        % Calculate EMD
        val1 = emd_hat_gd_metric_mex(condA,condB,GDMtrx,-1);
        val2 = emd_hat_gd_metric_mex(condA,condB,GDMtrx,0);
        val3 = emd_hat_gd_metric_mex(condA_mat,condB_mat,GDMtrx,-1);
        val4 = emd_hat_gd_metric_mex(condA_mat,condB_mat,GDMtrx,0);
        
        EMDMtrx(condpairs,1) = val1;
        EMDMtrx_Rub(condpairs,1) = val2;
        EMDMtrx_scaled(condpairs,1) = val3;
        EMDMtrx_scaled_Rub(condpairs,1) = val4;
        label1 = labels{all_conds_pairs(condpairs,1)};
        label2 = labels{all_conds_pairs(condpairs,2)};
        
        % fprintf(fileID,'%s\t%s\t%4.4f\t%4.4f\t%4.4f\t%4.4f\n', label1, label2, val1, val2, val3, val4);
    end
    % fclose(fileID);

end