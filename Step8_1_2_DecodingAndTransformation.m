% Decoding based on correlation/EMD
clear 
close all

addpath('~/Dropbox/LAB_Folder_Macbook/MATLAB/Utilities/');

CalcType = 'actmaps';
disp(CalcType)

Sub_dir = '~/Dropbox/LAB_Folder_Macbook/MATLAB/Data/haxby2001';
% ---------------------------------------------------------
MatName = 'mc_ss_VT_new3';
% ---------------------------------------------------------
tempFile = fullfile(Sub_dir,'subj1',['EMD_subj1_' MatName '.mat']);

AllVarNames = who('-file',tempFile);
AllVarNames_num = length(AllVarNames);

% Extract correlation matrix and EMD matrix for al[l subjects
CorrMtrx = GenerateGroupMtrx(MatName,CalcType,AllVarNames{1});
EMDMtrx = GenerateGroupMtrx(MatName,CalcType,AllVarNames{3});
EMDMtrx_mat = mat2gray(EMDMtrx);
EMDMtrx_mat_256 = EMDMtrx_mat .* 255;


% Perform decoding based on EMD matrices
% What needs to be done here: calcuate simialrity of distance matrices
Results1 = AcrossSubsDecoding(EMDMtrx_mat);

% Perform decoding based on EMD matrices
% For each EMD matrix, the range value differs
% So I'm trying to normalize each subject separately
EMDMtrx_mat_ind = zeros(5,28);
for nsub = 1:5
    EMDMtrx_mat_ind(nsub,:) = mat2gray(EMDMtrx(nsub,:));
end
Results2 = AcrossSubsDecoding(EMDMtrx_mat_ind);

Mtrx1 = Results1{4,1};
% Perform statistical test to see whether there is significant change
[h,p,ci,stats] = ttest2(Results1,Results2);

% Decoding performance is similar to what we had before
[h2,p2,ci2,stats2] = ttest2(Results1,Results2);
GroupMtrx3 = GenerateGroupMtrx(MatName,CalcType,AllVarNames{30});
Decoding_results3 = AcrossSubsDecoding(GroupMtrx3);

% Function to transfer value ranges
a1 = min(GroupMtrx3(:)); b1 = max(GroupMtrx3(:));
c1 = 255; d1 = 255;

GroupMtrx_shifted = c1 + (d1 - c1)/(b1 - a1) .* (GroupMtrx3 - a1);
% The value range changes, then check correlation value?
tempCorrVals = corr(GroupMtrx_shifted);
