% Test script to correlate surface/Euclidian distance with spatial
% coordinates
% Right now the surface distance calculation hasn't been finished.... So
% wait for subject 3 - 5 here
clear all;

resolution = '64';
surf = 'intermediate_al';
for sub = 1
    subj_dir = ['~/Documents/MATLAB/Data/haxby2001/subj' num2str(sub) '_SurfCoords'];
    cd(subj_dir);
    
    % Load Euclidian distance (unique nodes)
    EucDistFile = importdata(['subj' num2str(sub) '_NodesDist_Euc_' resolution '_' surf '.asc.1D']);
    EucDist = EucDistFile.data;

    % Load surface distance (unique nodes)
    SurfDistFile = importdata(['subj' num2str(sub) '_NodesDist_surf_' resolution '_' surf '.asc.1D']);
    SurfDist = SurfDistFile.data;

    % Load nodes indices
    AllNodesInfo = importdata(['subj' num2str(sub) '_bold_raw_mc_mean_res' resolution '_mh_ave.1D']);
    Nodes_Coords_list = [AllNodesInfo(:,1),AllNodesInfo(:,3:5)];
    
    num_nodes_uni = size(SurfDist,1);
    coords1 = zeros(num_nodes_uni,3);
    coords2 = zeros(num_nodes_uni,3);

    for ctr = 1:num_nodes_uni
        coords1(ctr,:) = AllNodesInfo(AllNodesInfo(:,1) == SurfDist(ctr,1), 3:5);
        coords2(ctr,:) = AllNodesInfo(AllNodesInfo(:,1) == SurfDist(ctr,2), 3:5);
    end
    
    HDR = {'Nodes1','Coords1','Nodes2','Coords2','SurfDist','EucDist'};
    DistData = [SurfDist(:,1),coords1,SurfDist(:,2),coords2,SurfDist(:,3),EucDist(:,3)];
    
    FileName = ['subj' num2str(sub) '_DistData_UniNodes_res' resolution '_' surf '.mat'];
    save(fullfile(subj_dir,FileName),'HDR','DistData')
end 