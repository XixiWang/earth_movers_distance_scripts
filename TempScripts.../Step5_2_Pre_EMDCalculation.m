clear all;

resolution = '64';
surf = 'intermediate_al';
for sub = 1
    subj_dir = ['~/Documents/MATLAB/Data/haxby2001/subj' num2str(sub)];
    subj_surf_coords_dir = ['~/Documents/MATLAB/Data/haxby2001/subj' num2str(sub) '_SurfCoords'];
    subj_suma_dir = ['~/Documents/MATLAB/Data/haxby2001/subj' num2str(sub) ...
        '_suma_surfaces_al_mean_bold_mc'];
    addpath(subj_dir);
    addpath(subj_surf_coords_dir);
    addpath(subj_suma_dir);
    
    SurfDistFile = ['subj' num2str(sub) '_DistData_AllNodes_ave_res' resolution '_' surf '.mat'];
    load(SurfDistFile);
    
    ActMapsFile = ['Subj' num2str(sub) '_actmaps_VT_mc_ss.mat'];
    load(ActMapsFile,['subj' num2str(sub) '_data'],['subj' num2str(sub) '_coords']);
    
    IndActMaps = eval(['subj' num2str(sub) '_data'])';
    IndActMapCoords = eval(['subj' num2str(sub) '_coords']);
    
    % Convert subscripts to linear idx (PyMVPA generated)
    % Original image size
    imgsiz = [40,64,64];
    
    ActMapLen = size(IndActMapCoords,1);
    ActMap_IdxList = zeros(ActMapLen,1);
    for ncond = 1:ActMapLen
        % Convert voxels in PyMVPA generated activity maps to 1D index
        % i, j + 1, k + 1
        ActMap_IdxList(ncond,1) = sub2ind(imgsiz,IndActMapCoords(ncond,1), ...
            IndActMapCoords(ncond,2) + 1, ...
            IndActMapCoords(ncond,3) + 1);
    end
    IndActInfo = [ActMap_IdxList double(IndActMapCoords) IndActMaps];

    % Convert subscripts to linear idx (AFNI generated)
    Voxel1Coords = DistData_ave(:,1:3);
    Voxel2Coords = DistData_ave(:,4:6);
    
%     num_univoxels = size(unique(Voxel1Coords,'rows'),1);
%     uni_voxels_coords = unique(Voxel1Coords,'rows');
    num_univoxels = size(unique(Voxel1Coords,'rows','stable'),1);
    uni_voxels_coords = unique(Voxel1Coords,'rows','stable');
    
    [a,b,c] = unique(Voxel1Coords,'rows','stable');
    
    uni_voxels_idx = zeros(num_univoxels,1);
    for ctr = 1:num_univoxels
        uni_voxels_idx(ctr,1) = sub2ind(imgsiz,uni_voxels_coords(ctr,1), ...
                                uni_voxels_coords(ctr,2) + 1, ...
                                uni_voxels_coords(ctr,3) + 1);
    end
    
    % Use linear idx to find corresponding value under each condition
    Act_All_Cond = cell(1,15);
    ActVec = zeros(num_univoxels,1);
    for ncond = 1:8
        disp(['Analyzing condition ' num2str(ncond)]);
        for ctr = 1:num_univoxels
            tempidx = find(IndActInfo(:,1) == uni_voxels_idx(ctr,1));
            ActVec(ctr,1) = IndActInfo(tempidx, 4 + ncond);
        end
        Act_All_Cond{1,ncond} = ActVec;
    end
    
    Act_All_Cond{1,9} = [uni_voxels_idx uni_voxels_coords]; 
    
    % Process all voxel pairs
    num_allvoxel_pairs = size(DistData_ave,1);
    temp1 = zeros(num_allvoxel_pairs,1);
    temp2 = zeros(num_allvoxel_pairs,1);
    for ctr = 1:num_allvoxel_pairs
        temp1(ctr,1) = sub2ind(imgsiz,Voxel1Coords(ctr,1), ...
            Voxel1Coords(ctr,2) + 1, ...
            Voxel1Coords(ctr,3) + 1);
        temp2(ctr,1) = sub2ind(imgsiz,Voxel2Coords(ctr,1), ...
            Voxel2Coords(ctr,2) + 1, ...
            Voxel2Coords(ctr,3) + 1);
    end
    SurfDistInfo = [temp1 temp2 DistData_ave(:,7:8)];

    % Delete self comparison voxel pairs
    SelfVoxPairs_Idx = find(temp1(:) == temp2(:) );
    SurfDistInfo_InterVox = SurfDistInfo;
    SurfDistInfo_InterVox(SelfVoxPairs_Idx,:) = [];
    
    % Construction of ground distance using surface based dist.
    GD_Surf = zeros(num_univoxels,num_univoxels);
    num_allvoxel_pairs_inter = size(SurfDistInfo_InterVox,1);
    rclist = zeros(num_allvoxel_pairs_inter,2);
    
    for ctr = 1:num_allvoxel_pairs_inter
        row = find(uni_voxels_idx == SurfDistInfo_InterVox(ctr,1));
        col = find(uni_voxels_idx == SurfDistInfo_InterVox(ctr,2));
        rclist(ctr,1) = row;
        rclist(ctr,2) = col;
        GD_Surf(row,col) = SurfDistInfo_InterVox(ctr,3);
    end
    
    % I found out that dist(ij) != dist(ji); sometimes the difference is
    % quite big...
    % Construction of ground distance using Euclidian dist.
    GD_Euc = zeros(num_univoxels,num_univoxels);
end