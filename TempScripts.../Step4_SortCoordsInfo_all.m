% Test script to correlate surface/Euclidean distance with spatial
% coordinates
% 124 are done
clear all

resolution = '64';
surf = 'intermediate_al';
for sub = 1
    subj_dir = ['~/Documents/MATLAB/Data/haxby2001/subj' num2str(sub) '_SurfCoords'];
    cd(subj_dir);
    
    FileName = ['subj' num2str(sub) '_DistData_AllNodes_res' resolution '_' surf '.mat'];
    if exist(FileName,'file') == 0
        % Load surface distance (all nodes)
        SurfDistFile = importdata(['subj' num2str(sub) '_NodesDist_com_surf_' resolution '_' surf '.asc.1D']);
        SurfDist = SurfDistFile.data;
        
        % Load Euclidian distance (all nodes)
        EucDistFile = importdata(['subj' num2str(sub) '_NodesDist_com_Euc_' resolution '_' surf '.asc.1D']);
        EucDist = EucDistFile.data;
        
        % Load nodes indices
        AllNodesInfo = importdata(['subj' num2str(sub) '_bold_raw_mc_mean_res' resolution '_mh_ave.1D']);
        Nodes_Coords_list = [AllNodesInfo(:,1),AllNodesInfo(:,3:5)];
        
        num_nodes = size(SurfDist,1);
        coords1 = zeros(num_nodes,3);
        coords2 = zeros(num_nodes,3);
        
        for ctr = 1:num_nodes
            coords1(ctr,:) = AllNodesInfo(AllNodesInfo(:,1) == SurfDist(ctr,1), 3:5);
            coords2(ctr,:) = AllNodesInfo(AllNodesInfo(:,1) == SurfDist(ctr,2), 3:5);
        end
        
        HDR = {'Nodes1','Coords1','Nodes2','Coords2','SurfDist','EucDist'};
        DistData = [SurfDist(:,1),coords1,SurfDist(:,2),coords2,SurfDist(:,3),EucDist(:,3)];
        
        save(fullfile(subj_dir,FileName),'HDR','DistData')
    else
        load(FileName);
    end
    
    % Extrac unique node pairs .............
    FileName2 = ['subj' num2str(sub) '_DistData_AllNodes_ave_res' resolution '_' surf '.mat'];
    if exist(FileName2,'file') == 0
        Node1toNode2 = [DistData(:,2:4),DistData(:,6:8)];
        SurfDist = DistData(:,9);
        % For the surface distance here, -1 values exist
        % So in order to process these values: 
        % SurfDistNew = SurfDist;
        % SurfDistNew(SurfDist == -1) = max(SurfDist) * 1.5;
        EucDist = DistData(:,10);
        
        [C,ia,ic] = unique(Node1toNode2,'rows','stable');
        num_uni_pairs = length(ia);
        
        Node1toNode2_coords = zeros(num_uni_pairs,6);
        Node1toNode2_Surf_values = zeros(num_uni_pairs,1);
        Node1toNode2_Euc_values = zeros(num_uni_pairs,1);
        
        for ctr = 1:num_uni_pairs
            % SurfDist(ic == ctr,:)
            temp = Node1toNode2(ic == ctr,:);
            Node1toNode2_coords(ctr,:) = temp(1,:);
            Node1toNode2_Surf_values(ctr,:) = mean(SurfDist(ic == ctr,:));
            Node1toNode2_Euc_values(ctr,:) = mean(EucDist(ic == ctr,:));
        end
        
        HDR2 = {'Coords1','Coords2','Ave_Surf_Dist','Ave_Euc_Dist'};
        DistData_ave = [Node1toNode2_coords,Node1toNode2_Surf_values,Node1toNode2_Euc_values];
        save(fullfile(subj_dir,FileName2),'HDR2','DistData_ave');
    else
        load(FileName2);
    end
    
    unix('df -h | /usr/bin/mail -s "MATLAB analysis is done!" xixi.wang577@gmail.com')
end


