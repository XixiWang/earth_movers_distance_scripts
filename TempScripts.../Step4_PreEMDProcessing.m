% Generate ground distance matrix and extract corresponding activity data
% under different conditions

% Step3: subject 2 is done

clear ;
% clc;

resolution = '64';
surf = 'intermediate_al';

sub = 1;
subj_dir = ['~/Documents/MATLAB/Data/haxby2001/subj' num2str(sub) '_SurfCoords'];
cd(subj_dir);

NodesInfoFile = ['subj' num2str(sub) '_Nodelist_complete_res' resolution '_sorted.mat'];
load(NodesInfoFile);

SingleNodeCoords = load(['subj' num2str(sub) '_bold_raw_mc_mean_res' resolution '_mh_ave.1D']);
Voxs_idx = SingleNodeCoords(:,2);
[~,I] = sort(Voxs_idx);
SingleNodeCoords_sort = SingleNodeCoords(I,:);
[tempa,tempb] = unique(SingleNodeCoords_sort(:,3:5),'rows','stable'); 
tempc = SingleNodeCoords_sort(tempb,2);
num_voxels = size(tempa,1);
disp([num2str(num_voxels) ' voxels are selected']);
% Generate single node information
SingleNodeInfo = [(1:num_voxels)',tempc,tempa];
SingleNodeInfo_HDR = {'Local index','Global voxel index','Voxel Coords'};

% Load Euclidian distance (all nodes)
EucDistFile = importdata(['subj' num2str(sub) '_NodesDist_com_Euc_' resolution '_' surf '_sorted.asc.1D']);
EucDist = EucDistFile.data;

% Load surface distance (all nodes)
% SurfDistFile = importdata(['subj' num2str(sub) '_NodesDist_com_surf_' resolution '_' surf '_sorted.asc.1D']);
% SurfDist = SurfDistFile.data;

DistData = [Nodeslist_com EucDist(:,3)]; % Add surface dist....
HDR = {'Node1Idx','VoxCoords','Node2Idx','VoxCoords','EucDist'};

temp = DistData;
% Extract unique voxel pairs
% Each voxel can be projected into multiple nodes
% First find intra voxel comparisions
intra_vox_idx = bsxfun(@eq,DistData(:,2:4),DistData(:,6:8));
intra_vox_idx = all(intra_vox_idx,2)';
intra_vox_idx = find(intra_vox_idx == 1);
disp([num2str(length(intra_vox_idx)) ' intra voxel comparisons found']);

% Delete intra voxel comparisons
DistData(intra_vox_idx,:) = [];

% Delete nodes indices
DistData(:,1) = []; 
DistData(:,4) = [];

% Combine same voxel pairs
% From voxel A
[a1,b1,c1] = unique(DistData(:,1:3),'rows','stable');
% To voxel B
[a2,b2,c2] = unique(DistData(:,4:6),'rows','stable');
% Correspond to single node information (SingleNodeInfo)
c2 = c2 + 1;
 
% Generate ground distance vector
Euc_vec = [c1 c2 DistData(:,7)];
[a3,b3,c3] = unique(Euc_vec(:,1:2),'rows','stable');

% Calculate averaged ground distance vector
Euc_vec_ave = zeros(size(a3,1),3);
for ctr = 1:size(a3,1)
    Euc_vec_ave(ctr,3) = mean(Euc_vec(c3 == ctr,3));
end
Euc_vec_ave(:,1:2) = a3;

% Construct ground distance matrix
GD_Euc = zeros(num_voxels,num_voxels);
for ctr = 1:size(a3,1)
    GD_Euc(Euc_vec_ave(ctr,1),Euc_vec_ave(ctr,2)) = Euc_vec_ave(ctr,3);
end

unix('df -h | /usr/bin/mail -s "Ground distance matrix is done" xixi.wang577@gmail.com')