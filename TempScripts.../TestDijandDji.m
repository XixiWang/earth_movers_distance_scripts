% Test script to find out the duplication problem and potential distance
% differences
clear;
resolution = '64';
surf = 'intermediate_al';

for sub = 1
    subj_dir = ['~/Documents/MATLAB/Data/haxby2001/subj' num2str(sub) '_SurfCoords'];
    cd(subj_dir);
    
    FileName = ['subj' num2str(sub) '_DistData_AllNodes_res' resolution '_' surf '.mat'];
    load(FileName);
    
    FileName2 = ['subj' num2str(sub) '_DistData_AllNodes_ave_res' resolution '_' surf '.mat'];
    load(FileName2);
end

% Sort DistData_ave
[a1,b1,c1] = unique(DistData_ave(:,1:3),'rows');
[a2,b2,c2] = unique(DistData_ave(:,4:6),'rows');