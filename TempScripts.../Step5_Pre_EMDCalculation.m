% Import previous calculated distance matrices and then perform EMD
% calculation
clear all;

resolution = '64';
surf = 'intermediate_al';
for sub = 1
    subj_dir = ['~/Documents/MATLAB/Data/haxby2001/subj' num2str(sub)];
    subj_surf_coords_dir = ['~/Documents/MATLAB/Data/haxby2001/subj' num2str(sub) '_SurfCoords'];
    subj_suma_dir = ['~/Documents/MATLAB/Data/haxby2001/subj' num2str(sub) ...
        '_suma_surfaces_al_mean_bold_mc'];
    addpath(subj_dir);
    addpath(subj_surf_coords_dir);
    addpath(subj_suma_dir);
    
    SurfDistFile = ['subj' num2str(sub) '_DistData_AllNodes_ave_res' resolution '_' surf '.mat'];
    load(SurfDistFile);
    
    ActMapsFile = ['Subj' num2str(sub) '_actmaps_VT_mc_ss.mat'];
    load(ActMapsFile,['subj' num2str(sub) '_data'],['subj' num2str(sub) '_coords']);
    
    IndActMaps = eval(['subj' num2str(sub) '_data'])';
    IndActMapCoords = eval(['subj' num2str(sub) '_coords']);
    
    % Convert subscripts to linear idx
    % Original image size
    imgsiz = [40,64,64];
    ActMapLen = size(IndActMapCoords,1);
    ActMap_IdxList = zeros(ActMapLen,1);
    for ncond = 1:ActMapLen
        % Convert voxels in PyMVPA generated activity maps to 1D index
        % i, j + 1, k + 1
        ActMap_IdxList(ncond,1) = sub2ind(imgsiz,IndActMapCoords(ncond,1), ...
            IndActMapCoords(ncond,2) + 1, ...
            IndActMapCoords(ncond,3) + 1);
    end
    IndActInfo = [ActMap_IdxList double(IndActMapCoords) IndActMaps];
    
    % Process the data generated in AFNI
    Node1Coords = DistData_ave(:,1:3);
    Node2Coords = DistData_ave(:,4:6);
    SurfDistMap_IdxList_length = size(Node1Coords,1);
    temp1 = zeros(SurfDistMap_IdxList_length,1);
    temp2 = zeros(SurfDistMap_IdxList_length,1);
    for ncond = 1:SurfDistMap_IdxList_length
        temp1(ncond,1) = sub2ind(imgsiz,Node1Coords(ncond,1), ...
            Node1Coords(ncond,2) + 1, ...
            Node1Coords(ncond,3) + 1);
        temp2(ncond,1) = sub2ind(imgsiz,Node2Coords(ncond,1), ...
            Node2Coords(ncond,2) + 1, ...
            Node2Coords(ncond,3) + 1);
    end
    SurfDistInfo = [temp1 temp2 DistData_ave(:,7:8)];
    
    % Generate activity vectors for all eight conditions
    Act_All_Cond = cell(1,9);
    
    for ncond = 1:8
        val1 = zeros(SurfDistMap_IdxList_length,1);
        val2 = val1;
        disp(['Analyzing conditional ' num2str(ncond)]);
        
        for ctri = 1:SurfDistMap_IdxList_length
            idx1 = find(IndActInfo(:,1) == SurfDistInfo(ctri,1));
            idx2 = find(IndActInfo(:,1) == SurfDistInfo(ctri,2));
            
            val1(ctri,1) = IndActInfo(idx1, 4 + ncond);
            val2(ctri,1) = IndActInfo(idx2, 4 + ncond);
        end
        
        val_tot = [val1 val2];
        Act_All_Cond{1,ncond} = val_tot;
    end
    
    NodesInfo_fin = [Node1Coords Node2Coords];
    Act_All_Cond{1,9} = NodesInfo_fin;
end

