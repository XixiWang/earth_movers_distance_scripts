clear;
close all;

addpath('~/Dropbox/LAB_Folder_Macbook/MATLAB/Code/FastEMD_PeleWerman/');
addpath('~/Dropbox/LAB_Folder_Macbook/MATLAB/Utilities/');
addpath('~/Dropbox/LAB_Folder_Macbook/MATLAB/Utilities/histnorm/');

labels = {'bottle','cat','chair','face','house','scissor','scrapix','shoe'};

% Calculation type: 'actmaps' or 'tmaps'
CalcType = 'actmaps';
disp(CalcType);

% Calculate fast EMD (histograms don't have to be normalized)
extra_mass_penalty = -1;
% For Rubner's EMD, extra_mass_penalty = 0

ncond = 8;
all_conds_pairs = nchoosek(1:ncond,2);

for sub = 1:5
    disp(['Analyzing subject' num2str(sub)]);
    subj_dir = ['~/Dropbox/LAB_Folder_Macbook/MATLAB/Data/haxby2001/subj' num2str(sub)];
    subj_surfdir = ['~/Dropbox/LAB_Folder_Macbook/MATLAB/Data/haxby2001/subj' num2str(sub) '_SurfCoords'];
    cd(subj_dir);
    
    if strcmp(CalcType,'actmaps')
        EMD_PrefileName = ['EMD_pre_subj' num2str(sub) '_mc_ss_VT2.mat'];
    elseif strcmp(CalcType,'tmaps')
        EMD_PrefileName = ['EMD_pre_subj' num2str(sub) '_mc_ss_VT_tmaps2.mat'];
    end
    load(EMD_PrefileName);
    
    GroundDistFiles = who('GD_Surf_com*');
    GDlength = size(GroundDistFiles,1);
    
    GroundDistFiles{GDlength + 1,1} = 'GD_Euc_com';
    
    % Generate table header info
    % fileID = fopen(['subj' num2str(sub) '_Euc_com_TestResults.txt'],'at');
    % fprintf(fileID,'Condition1\tCondition2\tFast EMD\tRubner EMD\tScaled fEMD\tSRubner EMD\n');
    
    % ***************************************************************
    % Updated on 07/01/2015: Histogram equalization
    % Method 1: NEW histogram function has been used here
    % ***************************************************************
    All_histinfo = struct;
    % binvals = [100 size(Afni_coors_val,1)];
    binvals = 100;
    for ctrbins = 1:length(binvals)
        nbins = binvals(ctrbins);
        disp(['nbins = ' num2str(nbins)]);
        
        % Loop through all condition pairs
        for ctrcond = 1:ncond
            disp(['Analyzing condition ' num2str(ctrcond) ': ' labels(ctrcond)]);
            currcond = Afni_coors_val(:,5 + ctrcond);
            
            % Histogram calculation and comparison
            % For histogram normalization: binedges are same, only values
            % differ
            figure; title(labels(ctrcond));
            hold on;
            hcond = histogram(currcond,nbins);
            hcond_vals = hcond.Values;
            hcond_binedges = hcond.BinEdges;
            
            hcond_norm_pdf = histogram(currcond,nbins,'Normalization','pdf');
            hcond_norm_pdf_vals = hcond_norm_pdf.Values;
            % hcond_norm_pdf_binedges = hcond_norm_pdf.BinEdges;
            
            hcond_norm_prob = histogram(currcond,nbins,'Normalization','probability');
            hcond_norm_prob_vals = hcond_norm_prob.Values;
            % hcond_norm_prob_binedges = hcond_norm_prob.BinEdges;
            
            bin_cond_idx = cell(1, size(Afni_coors_val,1));
            for ctrj = 1:length(hcond_vals)
                % For each bin find corresponding idx (each bin could either have single/multiple idx or null)
                tempidxA = find(hcond_binedges(ctrj) <= currcond & currcond <= hcond_binedges(ctrj + 1));
                if ~isempty(tempidxA)
                    bin_cond_idx{ctrj} = tempidxA;
                end
            end
            
            eval(['All_histinfo.nbins' num2str(nbins) '.cond' num2str(ctrcond) '.data = currcond;'])
            eval(['All_histinfo.nbins' num2str(nbins) '.cond' num2str(ctrcond) '.hcond_vals = hcond_vals;'])
            eval(['All_histinfo.nbins' num2str(nbins) '.cond' num2str(ctrcond) '.hcond_norm_pdf_vals = hcond_norm_pdf_vals;'])
            eval(['All_histinfo.nbins' num2str(nbins) '.cond' num2str(ctrcond) '.hcond_norm_prob_vals = hcond_norm_prob_vals;'])
            eval(['All_histinfo.nbins' num2str(nbins) '.cond' num2str(ctrcond) '.hcond_binedges = hcond_binedges;'])
            eval(['All_histinfo.nbins' num2str(nbins) '.cond' num2str(ctrcond) '.bin_cond_idx = bin_cond_idx;'])
            
            % Clear histogram objects
            close all;
        end
    end
    
    disp(['Subject ' num2str(sub) ' is done']);
    
    FileName = ['subj' num2str(sub) '_histdata.mat'];
    if ~exist(FileName,'file')
        save(fullfile(subj_dir,FileName),'All_histinfo');
    end
    
    % ***************************************************************
    % Bin to bin distance calculation
    % ***************************************************************
    SurfDataName = ['Subj' num2str(sub) '_GDMatrices.mat'];
    load(fullfile(subj_surfdir,SurfDataName),'Euc_Surf_Col_ave2');
    DistData = Euc_Surf_Col_ave2;
    for ctrbins = 1:length(binvals)
        % ***************************************************************
        % Loop through different number of bins
        % ***************************************************************
        nbins = binvals(ctrbins);
        disp(['number of bins is ' num2str(nbins)]);
        
        All_binsdist = cell(1,size(all_conds_pairs,1));
        for ctrk = 1:size(all_conds_pairs,1)
            tic;
            
            cond1_idx = all_conds_pairs(ctrk,1);
            cond2_idx = all_conds_pairs(ctrk,2);
            
            eval(['cond1_histidx = All_histinfo.nbins' num2str(nbins) '.cond' num2str(cond1_idx) '.bin_cond_idx;']);
            eval(['cond2_histidx = All_histinfo.nbins' num2str(nbins) '.cond' num2str(cond2_idx) '.bin_cond_idx;']);
            
            bintobin_dist_vec = zeros(length(hcond_vals)^2,1);
            [ctr1_mesh,ctr2_mesh] = meshgrid(1:length(hcond_vals),1:length(hcond_vals));
            ctr1_col = ctr1_mesh(:);
            ctr2_col = ctr2_mesh(:);
            for ctr = 1:length(ctr1_col)
                idxlist1 = cond1_histidx{ctr1_col(ctr)};
                idxlist2 = cond2_histidx{ctr2_col(ctr)};
                if isempty(idxlist1) || isempty(idxlist2) || isequal(idxlist1,idxlist2)
                    bintobin_dist_vec(ctr) = 0;
                else
                    comp_Euc_vals = zeros(length(idxlist1) * length(idxlist2),1);
                    bintobin_dist_vec(ctr) = CalcAveDist(DistData,idxlist1,idxlist2);
                end
            end
            
            All_binsdist{ctrk} = bintobin_dist_vec;
            t_test = toc;
            
            unix('df -h | /usr/bin/mail -s "Single condition pairs comparison is done" xixi.wang577@gmail.com');
        end
        
        unix('df -h | /usr/bin/mail -s "Single bin value is done" xixi.wang577@gmail.com');
        FileName2 = ['Subj' num2str(sub) '_AllBinDist_' num2str(nbins)];
        % save(fullfile(subj_dir,FileName2),'All_binsdist')
    end
        
    unix('df -h | /usr/bin/mail -s "Single subject is done" xixi.wang577@gmail.com');
end
