% Test script to run RSA toolbox based on group-level
% similarity/dissimilarity matrices
%
% Modifed based on Step8_2_MDSProjection.m

%%
clear
close all

Sub_dir = '~/Dropbox/LAB_Folder_Macbook/MATLAB/Data/haxby2001/';
addpath('~/Dropbox/LAB_Folder_Macbook/MATLAB/Utilities/');
addpath('~/Dropbox/LAB_Folder_Macbook/MATLAB/Utilities/export_fig/');

labels = {'bottle','cat','chair','face','house','scissors','scrambledpix','shoe'};
criterions = {'stress','sstress','metricstress','metricsstress','sammon','strain'};

MatName = 'mc_ss_VT_new3';
tempFile = fullfile(Sub_dir,'subj1',['EMD_subj1_' MatName '.mat']);
% tempfile = '~/Dropbox/LAB_Folder_Macbook/MATLAB/Data/haxby2001/subj1/EMD_subj1_mc_ss_VT_new.mat';
AllVarNames = who('-file',tempFile);

% CalcType: 'tmaps' or 'actmaps'
CalcType = 'actmaps';
% EMDType: 'EMDVec' or 'EMDVec_norm'
EMDType = 'EMDVec';

% EMD_MDS_criterion = 'metricstress';
% Corr_MDS_criterion = 'sstress';

disp(CalcType);
disp(EMDType);
% disp(AllVarNames);

% Generate correlation matrix
CorrMtrx = GenerateGroupMtrx(MatName,CalcType,AllVarNames{1});
disp(['Correlation matrix: ' AllVarNames{1}]);

% Select a possible EMD matrix
varname = 36;
EMDMtrx = GenerateGroupMtrx(MatName,CalcType,AllVarNames{varname});
disp(['EMD matrix: ' AllVarNames{varname}])

%% RSA toolbox
rsapath = '~/Dropbox/LAB_Folder_Macbook/MATLAB/Toolboxes/rsatoolbox/';
addpath(genpath(rsapath));

mkdir(fullfile(Sub_dir,'RSAToolBox_test'));

% Configure settings
userOptions.rootPath = fullfile(Sub_dir,'RSAToolBox_test');
userOptions.analysisName = 'Corr_EMD_test';
userOptions.saveFiguresPDF = 1;
userOptions.conditionLabels = labels;
% userOptions.colourScheme = gray;

% Configure variables
userOptions.distance = 'Correlation'; % Default similarity measurement
% Calculate the dissimilarity between RDMs - rank correlation
userOptions.distanceMeasure = 'Spearman';

%% Process all CORRELATION based RDMs
% Display RDMs
FigNum = 1;
Corr_ltv_vecs = zeros(1,size(CorrMtrx,2),size(CorrMtrx,1));
for ctr = 1:size(CorrMtrx,1)
    Corr_ltv_vecs(1,:,ctr) = CorrMtrx(ctr,:);
end
% Convert similarity to dissimilarity
Corr_based_Rdm = squareRDMs(1 - Corr_ltv_vecs);
AvgSubCorrRDM = mean(Corr_based_Rdm,3);

% Rank transform of RDMs
showRDMs(Corr_based_Rdm,FigNum,1);
FigNum = FigNum + 1;
Fig1_name = 'Corr_based_RDMs_RankTrans';
% handleCurrentFigure(fullfile(Sub_dir,'RSAToolBox_test',Fig1_name),userOptions);

% No rank transform of RMDs
showRDMs(Corr_based_Rdm,FigNum,0);
FigNum = FigNum + 1;
Fig2_name = 'Corr_based_RMDs_noRankTrans';
% handleCurrentFigure(fullfile(Sub_dir,'RSAToolBox_test',Fig2_name),userOptions);

% Show all subjects + avg
showRDMs(concatRDMs_unwrapped(Corr_based_Rdm,AvgSubCorrRDM),FigNum);
FigNum = FigNum + 1;
Fig3_name = 'All_Subs_and_Avg';
% handleCurrentFigure(fullfile(Sub_dir,'RSAToolBox_test',Fig3_name),userOptions);


%% Process all EMD based RDMs
EMD_ltv_vecs = zeros(1,size(CorrMtrx,2),size(CorrMtrx,1));
for ctr = 1:size(EMDMtrx,1)
    EMD_ltv_vecs(1,:,ctr) = EMDMtrx(ctr,:);
end
EMD_based_Rdm = squareRDMs(EMD_ltv_vecs);

EMD_based_vec_rank = zeros(1,size(CorrMtrx,2),size(CorrMtrx,1));
% Output rank transformed matrices and calculate the averaged EMD-RDM
for ctr = 1:size(EMDMtrx,1)
    EMD_based_vec_rank(1,:,ctr) = scale01(rankTransform_equalsStayEqual(EMD_ltv_vecs(1,:,ctr)));
end
EMD_based_Rdm_rank = squareRDMs(EMD_based_vec_rank);
AvgSubEMDRDM_rank = mean(EMD_based_Rdm_rank,3);

% Examine output files
% showRDMs(EMD_based_Rdm,500,0);
% showRDMs(EMD_based_Rdm_rank,501,0);
% showRDMs(EMD_based_Rdm_rank,502,1);

showRDMs(concatRDMs_unwrapped(EMD_based_Rdm_rank,AvgSubEMDRDM_rank),FigNum);
FigNum = FigNum + 1;
Fig4_name = 'EMD_based_RDMs_RankTrans';
% handleCurrentFigure(fullfile(Sub_dir,'RSAToolBox_test',Fig4_name),userOptions);

%% Disp RDMs based on both correlation and EMD
% Correlation based MDS
% userOptions.criterion = metric stress
% -------------------------------------------------------------------------
% Here the EMD based rdm are not rank transformed 
modelRDMs = cat(3,Corr_based_Rdm,AvgSubCorrRDM,EMD_based_Rdm,AvgSubEMDRDM_rank);
% modelRDMs = cat(3,Corr_based_Rdm,AvgSubCorrRDM,EMD_based_Rdm_rank,AvgSubEMDRDM); % Updated 08/03
% -------------------------------------------------------------------------
RDMs_Names = {'Corr1','Corr2','Corr3','Corr4','Corr5','AveCorr',...
    'EMD1','EMD2','EMD3','EMD4','EMD5','AveEMD'};
modelRDMs = wrapAndNameRDMs(modelRDMs,RDMs_Names);

showRDMs(modelRDMs,FigNum);
FigNum = FigNum + 1;
Fig5_name = 'All_RDMs';
% handleCurrentFigure(fullfile(Sub_dir,'RSAToolBox_test',Fig5_name),userOptions);

% Place the RDMs in cells in order to pass them to compareRefRDM2candRDMs
modelRDMs_cell = cell(1,numel(modelRDMs));
for idx = 1:numel(modelRDMs)
    modelRDMs_cell{idx} = modelRDMs(idx);
end

% Avg correlation matrix MDS
MDSConditions(modelRDMs_cell{6},userOptions,struct('titleString','Avg Corr MDS', ...
    'fileName','Avg_Corr_MDS','figureNumber',FigNum));
FigNum = FigNum + 1;

% Avg correlation dendrogram
dendrogramConditions(modelRDMs_cell{6},userOptions, ...
    struct('titleString','Avg Corr Dendrogram','figureNumber', FigNum));
FigNum = FigNum + 1;

%% MDS - metric stress criterion
close all;
disp(['FigNum = ' num2str(FigNum)]);
for ctr = 1:6
    % EMD matrix MDS
    VarName = RDMs_Names{6 + ctr};
    MDSConditions(modelRDMs_cell{6 + ctr},userOptions,struct('titleString','EMD - MDS', ...
        'fileName',[VarName '_MDS'],'figureNumber',FigNum));
    FigNum = FigNum + 1;
    
    % EMD dendrogram
    % dendrogramConditions(modelRDMs_cell{6 + ctr},userOptions, ...
    %     struct('titleString',[VarName '_Dendrogram'],'figureNumber', FigNum));
    % FigNum = FigNum + 1;
end

%% CHECK MDS - different criterions
% Test metricsstress,sammon,strain //stress,sstress
% tempfignum = 500;
% usrOptions = userOptions;
% usrOptions.criterion = 'strain';
% for ctr = 1:6
%     VarName = RDMs_Names{6 + ctr};
%     MDSConditions(modelRDMs_cell{6 + ctr},usrOptions,struct('figureNumber',tempfignum));
%     tempfignum = tempfignum + 1;
% end
% After checking all criterions, it turned out that metricstress criterion
% is the best 

%% Dissimilarity of RDMs
% Compute Kendall rank correlation coefficient
userOptions.RDMcorrelationType = 'Kendall_taua';

pairwiseCorrelateRDMs(modelRDMs_cell,userOptions,...
    struct('figureNumber',FigNum,'fileName','RDM_CorrMtrx'));
FigNum = FigNum + 1;
MDSRDMs(modelRDMs_cell,userOptions,...
    struct('titleString','MDS of RDMs','figureNumber',FigNum,'fileName','2ndOrderMDSPlot'));
FigNum = FigNum + 1;

%% CHECK rank transformed EMD based RDMs
modelRDMs2 = cat(3,Corr_based_Rdm,AvgSubCorrRDM,EMD_based_Rdm_rank,AvgSubEMDRDM_rank);
modelRDMs2 = wrapAndNameRDMs(modelRDMs2,RDMs_Names);

modelRDMs_cell2 = cell(1,numel(modelRDMs2));
for idx = 1:numel(modelRDMs2)
    modelRDMs_cell2{idx} = modelRDMs2(idx);
end

pairwiseCorrelateRDMs(modelRDMs_cell2,userOptions,...
    struct('figureNumber',FigNum));
FigNum = FigNum + 1;
MDSRDMs(modelRDMs_cell2,userOptions,...
    struct('figureNumber',FigNum));
FigNum = FigNum + 1;
% After checking rank transformed EMD-RDMs and untransformed EMD-RDMs, they
% yielded same results

%% MDS of rank transformed EMD
modelRDMs3 = cat(3,EMD_based_Rdm_rank,AvgSubEMDRDM_rank);
modelRDMs3 = wrapAndNameRDMs(modelRDMs3,{'rEMD1','rEMD2','rEMD3','rEMD4','rEMD5','averEMD'});
modelRDMs_cell3 = cell(1,6);
for idx = 1:6
    modelRDMs_cell3{idx} = modelRDMs3(idx);
end
MDSRDMs(modelRDMs_cell3,userOptions,struct('figureNumber',FigNum));
FigNum = FigNum + 1;

%% MDS of original EMD
modelRDMs4 = EMD_based_Rdm;
modelRDMs4 = wrapAndNameRDMs(modelRDMs4,{'EMD1','EMD2','EMD3','EMD4','EMD5'});
modelRDMs_cell4 = cell(1,5);
for idx = 1:5
    modelRDMs_cell4{idx} = modelRDMs4(idx);
end
MDSRDMs(modelRDMs_cell4,userOptions,struct('figureNumber',FigNum));
FigNum = FigNum + 1;

%%% It turns out that (r)EMD3 and (r)EMD5 always differ from other RDMs

%% Use another rank transform function
EMD_based_vec_rank_new = zeros(1,size(CorrMtrx,2),size(CorrMtrx,1));
% Output rank transformed matrices and calculate the averaged EMD-RDM
for ctr = 1:size(EMDMtrx,1)
    %     EMD_based_vec_rank_new(1,:,ctr) = rankTransform(EMD_ltv_vecs(1,:,ctr));
    EMD_based_vec_rank_new(1,:,ctr) = scale01(rankTransform(EMD_ltv_vecs(1,:,ctr)));
end
EMD_based_Rdm_rank_new = squareRDMs(EMD_based_vec_rank_new);
AvgSubEMDRDM_rank_new = mean(EMD_based_Rdm_rank_new,3);

modelRDMs5 = cat(3,EMD_based_Rdm_rank_new,AvgSubEMDRDM_rank_new);
modelRDMs5 = wrapAndNameRDMs(modelRDMs5,{'rnEMD1','rnEMD2','rnEMD3','rnEMD4','rnEMD5','avernEMD'});
modelRDMs_cell5 = cell(1,6);
for idx = 1:6
    modelRDMs_cell5{idx} = modelRDMs5(idx);
end
MDSRDMs(modelRDMs_cell5,userOptions,struct('figureNumber',FigNum));
FigNum = FigNum + 1;

%% For decoding, use rank transformed EMD matrices
close all;
% Test decoding using correlation matrices
% [DecodingResults,MeanSubCorr] = AcrossSubsDecoding(1 - CorrMtrx);
[DecodingResults,MeanSubCorr] = AcrossSubsDecoding(CorrMtrx);
[DecodingResults_noZ,MeanSubCorr_noZ] = AcrossSubsDecoding_noZscore(CorrMtrx);

% Test decoding using rank transformed EMD matrices
tempMtrx = squeeze(EMD_based_vec_rank)';
% [DecodingResults2,MeanSubCorr2] = AcrossSubsDecoding(1 - tempMtrx);
[DecodingResults2,MeanSubCorr2] = AcrossSubsDecoding(tempMtrx);

%% CHECK: decoding without using z-scores
[DecodingResults3,MeanSubCorr3] = AcrossSubsDecoding_noZscore(tempMtrx);

% CHECK: for decoding, use the pre-calculated average matrix as template? 
% or anything related with leave one out decoding?!
[DecodingResults4,MeanSubCorr4] = AcrossSubsDecoding_noZscore_fixAve(tempMtrx);

%% Based on the EMD plots: try deleting sub3 & sub5
% Scratch idea......
tempMtrx1 = tempMtrx;
tempMtrx1(3,:) = [];
tempMtrx1(4,:) = [];
[DecodingResults5,MeanSubCorr5] = AcrossSubsDecoding(tempMtrx1);

%% CHECK: bin-to-bin EMD calculations
% Refer to other code


