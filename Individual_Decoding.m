% Perform individual subject's decoding
addpath('~/Dropbox/LAB_Folder_Macbook/MATLAB/Utilities/');

CalcType = 'tmaps';
disp(CalcType);

Script_dir = '~/Dropbox/LAB_Folder_Macbook/MATLAB/Utilities';
Sub_dir = '~/Dropbox/LAB_Folder_Macbook/MATLAB/Data/haxby2001/';

MatName = 'mc_ss_VT_new3';
tempFile = fullfile(Sub_dir,'subj1',['EMD_subj1_' MatName '.mat']);

AllVarName = who('-file',tempFile);
AllVarNames_num = length(AllVarName);

GroupMtrx_norm = zeros(5,28);
for ctr = 1:length(AllVarNames_num)
    
    GroupMtrx = GenerateGroupMtrx(MatName, CalcType, AllVarName{35});
    for ctrj = 1:size(GroupMtrx,1)
        GroupMtrx_norm(ctrj,:) = mat2gray(GroupMtrx);
    end
    
    [Decoding_Results,mean_subj_prop_correct] = AcrossSubsDecoding(GroupMtrx_norm);
    
    % Decoding results 
    Sub_ind_acc = Decoding_Results(GrayCodedMod_EbNo_M);
    
    
end

